from django.conf import settings

from django.contrib.auth.models import AbstractUser, Group
from django.db import models


class ILSUser(AbstractUser):

    class Meta:
        db_table = 'auth_user'

    action_notifications = models.BooleanField(
        default=True,
        help_text="Subscribe to incident action notifications"
    )
    sharing_notifications = models.BooleanField(
        default=True,
        help_text="Subscribe to incident sharing notifications"
    )
    investigation_notifications = models.BooleanField(
        default=True,
        help_text="Subscribe to investigation assignment notifications"
    )

    def can_investigate(self):
        return self.has_perm("incidents.change_investigation")

