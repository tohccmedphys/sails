from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from accounts.models import ILSUser
from django import forms


class ILSUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = ILSUser


class ILSUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = ILSUser

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            ILSUser.objects.get(username=username)
        except ILSUser.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


class ILSUserAdmin(UserAdmin):
    form = ILSUserChangeForm
    add_form = ILSUserCreationForm
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('action_notifications', 'investigation_notifications',)}),
    )


admin.site.register(ILSUser, ILSUserAdmin)
