
import logging
import ssl

from django.conf import settings
from django.contrib.auth.backends import BaseBackend
from accounts.models import ILSUser as User

if settings.USE_LDAP:
    import ldap3
    from ldap3.core.exceptions import LDAPException, LDAPBindError, LDAPSocketOpenError
    from ldap3 import ALL, Connection, Server, Tls

    # logging.basicConfig(filename='ldap.log', level=logging.DEBUG)
    # from ldap3.utils.log import set_library_log_detail_level, OFF, BASIC, NETWORK, EXTENDED
    # set_library_log_detail_level(EXTENDED)
    # from ldap3.utils.log import set_library_log_activation_level
    # set_library_log_activation_level(logging.INFO)


def auth_ldap(username=None, password=None, create=True):
    if not password or len(password) == 0:
        return None
    try:
        domain_username = '{}\\{}'.format(settings.LDAP_NT4_DOMAIN, username)

        if settings.LDAP_USE_SSL:
            #tls_config = Tls(
            #    validate=ssl.CERT_REQUIRED,
            #    local_private_key_file=settings.LDAP_LOCAL_KEY_FILE,
            #    local_certificate_file=settings.LDAP_LOCAL_CERT_FILE,
            #    #ca_certs_file=settings.LDAP_CERT_FILE,
            #    version=settings.LDAP_TLS_VERSION,
            #    ciphers='ALL'
            #)
            server = Server(settings.LDAP_AUTH_URL, use_ssl=True)
            conn = Connection(server, user=domain_username, password=password, auto_bind=ldap3.AUTO_BIND_NO_TLS)
        else:
            conn = Connection(settings.LDAP_AUTH_URL, user=domain_username, password=password, auto_bind=ldap3.AUTO_BIND_NO_TLS)

        if conn.result.get("description", '') == 'success':
            print("Success")
            if create:
                return get_or_create_ldap_user(username, conn)
            else:
                return User.objects.filter(username=username).first()
        return None

    except LDAPBindError:
        print("Unable to bind {}".format(username))
        return None


def get_or_create_ldap_user(username, conn):

    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:

        conn.search(
            settings.LDAP_AUTH_SEARCH_BASE,
            '({}={})'.format(settings.LDAP_USER_ID_FIELD, username),
            attributes=settings.LDAP_SEARCH_FIELDS
        )

        result = conn.entries[0]
        user = User(username=username, first_name=result.givenName, last_name=result.sn, email=result.mail)

        user.is_staff = False
        user.is_superuser = False
        user.set_unusable_password()
        user.save()

    return user


class ActiveDirectoryGroupMembershipSSLBackend(BaseBackend):

    def authenticate(self, request, username=None, password=None):
        if settings.USE_LDAP:
            return auth_ldap(username=username, password=password)
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

