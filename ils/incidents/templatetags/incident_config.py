from django import template
from django.template import Context
from django.template.loader import get_template

register = template.Library()

@register.simple_tag
def kochoice(id_, name,type_, active):
    if type_ == "tree":
        t = get_template("incidents/_kotree.html")
        c = {"name": name, "id": id_, "active": active}
    else:
        t = get_template("incidents/_kochoice.html")
        c = {"name": name, "id": id_, "active": active}
    return t.render(c)
