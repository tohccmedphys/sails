require(['jquery', 'lodash'], function($, _) {


    $(document).ready(function () {

        $("#action-table, #incident-table, #sharing-table").dataTable({
            iDisplayLength: 5,
            sDom: '<<ir>><<rt>><<p>>'
        });

        var stack = 0,
            bars = true,
            lines = false,
            steps = false;

        var options = {
            series: {
                stack: stack,
                lines: {
                    show: lines,
                    fill: true,
                    steps: steps
                },
                bars: {
                    show: bars,
                    barWidth: 0.8,
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0
            },
            yaxis: {
                autoscaleMargin: 0.08,
                minTickSize: 1,
                tickDecimals: 0
            },
            grid: {
                markings: [{xaxis: {from: -0.5, to: months_ago_new_year}}]
            },
            legend: {
                show: false
            }
        };

        var count_data = _.map(type_counts, function (i, l) {
            return {label: l, data: i};
        });

        var severity_data = _.map(severity_counts, function (i, l) {
            return {label: l, data: i};
        });

        var status_data = _.map(status_counts, function (i, l) {
            return {label: l, data: i};
        });


        function plotStats(toChartData, legend, chart) {
            /** This function is called by each chart/legend combo in the Incidents Counts section of the Statistics page.
             * it creates the intereactive graph/legends.
             *
             * Uses flot: https://github.com/flot/flot/blob/master/API.md
             *
             * @param legend: the div that contains the clickable legend.
             * @param chart: the div that contains the chart.
             * @param toChartData: the data set needed to be graphed */

            var i = 0;

            $.each(toChartData, function (key, val) {
                val.color = i;
                ++i;
                l = val.label;
                var theDiv = $('<div class="small-legend-select row checked"name="' + l + '" id="' + l + '">').appendTo(legend);
                var cbb = $('<div class="checkbox-div-border">').appendTo(theDiv);
                $('<div class="checkbox-div" id="' + l + '-checkbox">').appendTo(cbb);
                $('<label>', {
                    text: l
                    //'for': l
                }).appendTo(theDiv);

                theDiv.click(function () {
                    $(this).toggleClass("checked");
                    $(this).toggleClass("unchecked");
                    plotAccordingToChoices();
                    legend.find(".checked").change(plotAccordingToChoices);
                });

                theDiv.hover(
                    function () {
                        $(".checkbox-div-border", this).css("background-color", "#dd4814");
                        $("label", this).css("color", "#dd4814")
                    },
                    function () {
                        $(".checkbox-div-border", this).css("background-color", "#474747");
                        $("label", this).css("color", "#474747")
                    }
                );
            });

            function plotAccordingToChoices() {
                var data = [];

                legend.find(".checked").each(function () {
                    var key = this.id;

                    for (var i = 0; i < toChartData.length; i++) {
                        if (toChartData[i].label === key) {
                            data.push(toChartData[i]);
                            return true;
                        }
                    }
                });

                var series = $.plot(chart, data, options).getData();
                for (var i = 0; i < series.length; ++i) {
                    var label = series[i].label;
                    legend.find('#' + label + '-checkbox').css("background-color", series[i].color);
                }
            }

            var previousPoint = null;

            chart.bind("plothover", function (event, pos, item) {
                $("#x").text(pos.x.toFixed(2));
                $("#y").text(pos.y.toFixed(2));

                if (item) {
                    if (previousPoint != item.datapoint) {
                        previousPoint = item.datapoint;

                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(2),
                            y = item.datapoint[1].toFixed(2);

                        showTooltip(item.pageX, item.pageY, item.series.label + " $" + y);
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });

            //function showTooltip(x, y, contents) {
            //    $('<div id="tooltip">' + contents + '</div>').css({
            //        position: 'absolute',
            //        display: 'none',
            //        top: y + 5,
            //        left: x + 15,
            //        border: '1px solid #fdd',
            //        padding: '2px',
            //        backgroundColor: '#fee',
            //        opacity: 0.80
            //    }).appendTo("body").fadeIn(200);
            //}

            plotAccordingToChoices();
            legend.find(".checked").change(plotAccordingToChoices);

            $('.legendColorBox > div').each(function (i) {
                $(this).clone().prependTo(legend.find("li").eq(i));
            });

        }

        plotStats(count_data, $("#incident-legend"), $("#incident-chart"));
        plotStats(severity_data, $("#severity-legend"), $("#severity-chart"));
        plotStats(status_data, $("#status-legend"), $("#status-chart"));

        //$.plot("#incident-chart", count_data, _.extend({
        //    legend:{
        //        container: $("#incident-legend")
        //    }
        //},options));
        //
        //$.plot("#severity-chart", severity_data, _.extend({
        //    legend:{
        //        container: $("#severity-legend")
        //    }
        //},options));
        //
        //$.plot("#status-chart", status_data, _.extend({
        //    legend:{
        //        container: $("#status-legend")
        //    }
        //},options));


    });
});
