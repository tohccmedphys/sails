
function slugify(text) {
    return text.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
}

function ChoiceViewModel(url, pane) {
    var self = this;

    self.url = url;
    self.pane = pane;
    self.container = self.pane.find("ol");

    self.choices = ko.observableArray();
    self.itemToAdd = ko.observable("");

    self.dirty = ko.computed(function(){
        return _.any(_.invoke(self.choices(), "dirty"));
    });

    self.makeSortable = function(){
        self.container.sortable({
            group: self.pane.attr('id'),
            handle: 'span.move-handle',
            onDrop: function($item, container, _super){
                _super($item);
                self.reorder();
            }
        });
    };

    self.reorder = function(){
        var items = self.container.find("li");
        for (var idx =0; idx < items.length; idx++){
            var data = ko.dataFor(items[idx]);
            if (data.order !== idx){
                data.order = idx;
                data.dirty(true);
            }
        }
    };

    self.addItem = function(){
        var name = this.itemToAdd();
        if (name != "") {
            var toAdd = new AbstractChoice({
                name:name,
                slug:slugify(name),
                color:"#CCC",
                order:self.container.find("li").length,
                dirty:true,
                isNew:true,
                url:self.url
            });
            self.choices.push(toAdd);
            self.itemToAdd("");
            self.makeSortable();
            self.container.find(".colorpicker").colorpicker();

        }
    }.bind(self);

    self.save = function(){
        var dirty = _.filter(self.choices(), function(c){return c.dirty()});
        var count = dirty.length;
        var container = $(".tab-pane:visible .messages");
        _.invoke( dirty, "save", function(choice){
            count = count -1;
            if (count === 0){
                container.append('<div class="alert alert-success"> <strong>Success</strong> All choices updated successfully; <a class="close" data-dismiss="alert" href="#">&times;</a></div>');
            }
        },function(choice, resp){
            var msg = resp.slug[0].replace("this Slug","name "+choice.name);
            container.append('<div class="alert alert-error"> <strong>Failed</strong> Something went wrong. '+msg+' <a class="close" data-dismiss="alert" href="#">&times;</a></div>');
        });
    };

    ko.computed(function(){
        $.getJSON(self.url, function(data){
            self.choices(_.map(_.sortBy(data.results,"order"),function(e){return new AbstractChoice(e);}));

            self.makeSortable();
            self.reorder();

            if (self.dirty()){
                self.save();
            }

            self.container.find(".colorpicker").colorpicker();

        });
    });

}

function TreeViewModel(url, pane) {
    var self = this;

    self.url = url;
    self.pane = pane;
    self.container = self.pane.find("ol");

    self.items = ko.observableArray();

    //keep a flat record of all items to easily check
    //when collection is dirty etc
    self.flatItems = ko.observableArray();
    self.itemToAdd = ko.observable("");

    self.addItem = function(item){

        var name = this.itemToAdd();
        if (name != "") {
            var toAdd = new MPTT({
                name:name,
                lft:1,
                rght:2,
                level:0,
                tree_id:self.container.children('li').length+1,
                dirty:true,
                isNew:true,
                url:self.url,
                children:[]
            });
            self.flatItems.push(toAdd);
            self.items.push(toAdd);
            self.itemToAdd("");
            self.makeSortable();
        }

    };


    self.flatten = function(items,flat){
        flat = flat || [];
        _.each(items, function(item){
            flat.push(item);
            self.flatten(item.children, flat);
        });
        return flat;
    };



    self.dirty = ko.computed(function(){
        return _.any(_.invoke(self.flatItems(), "dirty"));
    });

    self.save = function(){

        var dirty = _.filter(self.flatten(self.items()), function(c){return c.dirty()});
        var count = dirty.length;
        var container = $(".tab-pane:visible .messages");
        var ptr = 0;
        var _save = function _save(cur_el){
            cur_el.save(
                function(choice){
                    ptr++;
                    if (ptr < count){
                        _save(dirty[ptr]);
                    }else{
                        container.append('<div class="alert alert-success"> <strong>Success</strong> All choices updated successfully; <a class="close" data-dismiss="alert" href="#">&times;</a></div>');
                    }
                },function(choice, resp){
                    var msg = resp.slug[0].replace("this Slug","name "+choice.name);
                    container.append('<div class="alert alert-error"> <strong>Failed</strong> Something went wrong. '+msg+' <a class="close" data-dismiss="alert" href="#">&times;</a></div>');
                }
            );
        };
        if (count > 0){
            _save(dirty[0]);
        }
    };

    self.listToTree = function(items, parentUrl){
        if (items.length == 0){
            return []
        }

        var tree = [];
        var el;
        for (var i=0; i < items.length; i++){
            el = items[i];
            el.children = _.filter(items, function(c){ return c.parent()===el.url.replace(Django.context.ABSOLUTE_ROOT,"")});
            _.each(el.children, function(c){c.parentItem = el});
            if (el.parent() === null){
                tree.push(el);
            }
        }
        return tree;
    };

    self.reorder = function(){

        var allRoots = self.container.children("li");

        _.each(allRoots, function(root){
            var rght = 1;

            var rebuild = function(n, lft){

                if (self.items().length === 0){ return; }

                var parent, data, parentData;

                n = $(n);

                if (n.get(0).tagName.toLowerCase() == "li"){
                    rght = lft +1;
                }

                _.each(n.children("ol,li"), function(c){
                    rght = rebuild(c, rght);
                })


                if (n.get(0).tagName.toLowerCase() == "li"){

                    parent = n.parentsUntil("li").parent();
                    parentData = ko.dataFor(parent.get(0));

                    data = ko.dataFor(n.get(0));
                    data.setAttr("lft",lft);
                    data.setAttr("rght",rght);
                    data.setAttr("parent",parentData ? parentData.url : null);
                    data.setAttr("level",n.parentsUntil(".root").filter("li").length);

                    var topParent = n.parents("li").last();
                    var searchFor = topParent.length > 0 ? topParent : n;
                    data.setAttr("treeID", self.container.children('li').index(searchFor)+1);

                    rght += 1;

                }

                return rght;
            }

            rebuild(root,1);
        });

    }

    self.makeSortable = function(){
        self.container.sortable({
            group: self.pane.attr('id'),
            handle: 'span.move-handle',
            onDrop: function($item, container, _super){

                _super($item);

                var data = ko.dataFor($item.get(0));

                //remove from current parent if necessary
                if (data.parent()){
                    data.parentItem.children.pop(data);
                    data.parentItem = null;
                    data.setAttr("parent",null);
                }

                //add to new parent  if necessary;
                var parents = $item.parents("li")
                if (parents.length  > 0){
                    var newParent = ko.dataFor($item.parents("li")[0]);
                    newParent.children.push(data);
                    data.parentItem = newParent;
                    data.setAttr("parent",newParent.url);
//self.items().pop(data);
                }

                self.reorder();
            }
        });
    };

    ko.computed(function(){
        $.getJSON(self.url, function(data){
            var results = _.map(data.results,function(d){return new MPTT(d);});
            self.flatItems(results);
            self.items(self.listToTree(results));
            self.makeSortable();

            self.reorder();
            if (self.dirty()){
                self.save();
            }
        });
    });
}

var choiceVMS = [
    //{
    //    name:"who",
    //    url: SiteConfig.forceScriptName + Django.url("whochoice-list", "json"),
    //    container:$("#who")
    //},
    //{
    //    name:"location",
    //    url: SiteConfig.forceScriptName +  SiteConfig.forceScriptName + Django.url("locationchoice-list", "json"),
    //    container:$("#location")
    //},
    //{
    //    name:"type",
    //    url: SiteConfig.forceScriptName + Django.url("incidenttype-list", "json"),
    //    container:$("#type")
    //},
    //{
    //    name:"severity",
    //    url: SiteConfig.forceScriptName + Django.url("incidentseverity-list", "json"),
    //    container:$("#severity")
    //},
    //{
    //    name:"priority",
    //    url: SiteConfig.forceScriptName + Django.url("actionpriority-list", "json"),
    //    container:$("#priority")
    //},
    //{
    //    name:"escalation",
    //    url: SiteConfig.forceScriptName + Django.url("actionescalation-list", "json"),
    //    container:$("#escalation")
    //},
    //{
    //    name:"standarddescription",
    //    url: SiteConfig.forceScriptName + Django.url("standarddescription-list", "json"),
    //    container:$("#standarddescription")
    //}
];

var treeVMS = [
    //{
    //    name:"domain",
    //    url: SiteConfig.forceScriptName + Django.url("domain-list", "json"),
    //    container:$("#domain")
    //},
    //{
    //    name:"cause",
    //    url: SiteConfig.forceScriptName + Django.url("cause-list", "json"),
    //    container:$("#cause")
    //}
];

var allVMs = {};

$(document).ready(function(){

    _.each(choiceVMS, function(options){
        var VM = new ChoiceViewModel(options.url.replace("undefined",""), options.container);
        ko.applyBindings(VM,options.container[0]);
        allVMs[options.name] = VM;
    });


    _.each(treeVMS, function(options){
        var VM = new TreeViewModel(options.url, options.container);
        ko.applyBindings(VM,options.container[0]);
        allVMs[options.name] = VM;
    });

    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
    }
    $("ol, ul ul, ul ol, ol ol, ol ul ").css("list-style","circle");


    // Change hash for page-reload
    $('.nav-tabs a').on('shown', function (e) {
        window.location.hash = e.target.hash;
    })
});

