"use strict";
$(document).ready(function(){



    $("table").on("click", "a.incident-flag", function(){
        var el = $(this);
        var icon = el.find("i");
        var id = el.data("incident");

        // note, I have no idea why Django.js is putting "triage" rather than "incident" in the url :(
        var url = SiteConfig.forceScriptName + Django.url("incident-detail", id, "json").replace("undefined","").replace("triage","incident");

        $.ajax(url, {
            method:"PATCH",
            data:{flag:!el.hasClass("flagged")},
            success: function(){
                el.toggleClass("flagged");
                icon.toggleClass("icon-flag icon-flag-alt");
            }
        });
    });

});

