"use strict";

function Investigation(data){
    var self = this;
    _.extend(self, data);
    self.originDomain = ko.observable(data.origin_domain);
    self.detectionDomain = ko.observable(data.detection_domain);
    self.investigator = ko.observable(data.investigator);
    self.complete = ko.observable(data.complete);
    self.init_load = true;

    ko.computed(function(){
        var data = {
            /*
            "investigator": self.investigator(),
            "origin_domain": self.originDomain(),
            "detection_domain": self.detectionDomain()
            /*"assigned": null,
            "assigned_by": null,
            "psls_id": null,
            "complete": false,
            "incident": "http://127.0.0.1:8000/incidents/api/incident/1/"
            */
        };

        if (self.init_load){
            self.init_load=false;
            return;
        }
        $.ajax(self.url, {
            method:"PATCH",
            data:data
        }
        );
    });
}

function Incident(data){
    var self = this;
    _.extend(self, data);
    self.description = ko.observable(data.description);
    self.psls_id = ko.observable(data.psls_id);
    self.patient_id = ko.observable(data.patient_id);
    if (!data.investigation){
        data.investigation = {
            origin_domain:null,
            detection_domain:null,
            investigator:null
        };

    }
    self.investigation = ko.observable(new Investigation(data.investigation));
    self.statusChoices = ["potential", "actual"];
    self.who = ko.observableArray(_.pluck(data.who,"url"));
    self.actual = ko.observable(data.actual);
    self.valid = ko.observable(data.valid);
    self.actualOrPotential = ko.computed(function(){
        var a = self.actual();
        return a.charAt(0).toUpperCase()+a.substr(1);
    });

    self.sync = function(callback){
        var data = {
            who:self.who(),
            actual:self.actual(),
            valid:self.valid(),
            description:self.description(),
            patient_id: self.patient_id()
        };
        $.ajax(self.url, {
            method:"PATCH",
            data:data,
            success:function(){callback(self)}
        });
    };
}

function IncidentAction(data){
    var self = this;
    _.extend(self, data);
    self.actionType = ko.observable(data.action_type.url);
    self.responsible = ko.observable(data.responsible);
}

function IncidentSharing(data){
    var self = this;
    _.extend(self, data);
    self.sharingType = ko.observable(data.sharing_type.url);
    self.responsible = ko.observable(data.responsible);
}

function AbstractChoice(data){

    var self = this;
    _.extend(self, data);
    self.url = self.url.replace(Django.context.ABSOLUTE_ROOT,"");
    self.dirty = ko.observable(data.dirty||false);
    self.color = ko.observable(data.color);
    self.color.subscribe(function(newColor){
        self.dirty(true);
    });

    self.isNew = data.isNew || (false);


    self.save = function(success_cb, error_cb){
        if (self.isNew){
            $.ajax(self.url,{
                method:"POST",
                data:self.getData(),
                success:function(data){
                    self.url = data.url.replace(Django.context.ABSOLUTE_ROOT,"");
                    self.dirty(false);
                    self.isNew = false;
                    success_cb(self);
                },
                error:function(xhr){
                    error_cb(self, JSON.parse(xhr.responseText));
                }
            });
        }else{
            $.ajax(self.url,{
                method:"PUT",
                data:self.getData(),
                success:function(){
                    self.dirty(false);
                    success_cb(self);
                },
                error:function(xhr){
                    error_cb(self, JSON.parse(xhr.responseText));
                }
            });
        }
    };
    self.getData = function(){
        return {
            name:self.name,
            slug:self.slug,
            color:self.color(),
            description:self.description,
            order:self.order
        };
    };

    self.setDirty = function(){
        self.dirty(true);
    };

}

function MPTT(data){

    var self = this;
    _.extend(self, data);

    self.lft = ko.observable(data.lft);
    self.rght = ko.observable(data.rght);
    self.name = ko.observable(data.name);
    self.description = ko.observable(data.description);
    self.parent = ko.observable(data.parent);
    self.treeID = ko.observable(data.tree_id);
    self.level = ko.observable(data.level);
    self.url = self.url.replace(Django.context.ABSOLUTE_ROOT,"");

    self.dirty = ko.observable(data.dirty || false);

    self.setAttr = function(attr, val){

        if (self[attr]() !== val){
            self[attr](val);
            self.dirty(true);
        }
    };

    self.setDirty = function(){
        self.dirty(true);
    };

    self.save = function(success_cb, error_cb){
        if (self.isNew){
            $.ajax(self.url,{
                method:"POST",
                data:self.getData(),
                success:function(data){
                    self.url = data.url.replace(Django.context.ABSOLUTE_ROOT,"");
                    self.dirty(false);
                    self.isNew = false;
                    _.each(self.children,function(c){
                        c.parent(self.url);
                    });

                    if (data.lft !== self.lft() && data.rght !==self.rght()){
                        throw "Left & Right tree data not stored correctly";
                    }
                    success_cb(self);
                },
                error:function(xhr){
                    error_cb(self, JSON.parse(xhr.responseText));
                }
            });
        }else{
            $.ajax(self.url,{
                method:"PUT",
                data:self.getData(),
                success:function(){
                    self.dirty(false);
                    success_cb(self);
                },
                error:function(xhr){
                    error_cb(self, JSON.parse(xhr.responseText));
                }
            });
        }
    };

    self.getData = function(){
        return {
            name:self.name(),
            description:self.description(),
            lft: self.lft(),
            rght:self.rght(),
            parent:self.parent(),
            tree_id:self.treeID(),
            level:self.level()
        };
    };
}
