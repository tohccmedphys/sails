
require(['jquery', 'flatpickr', 'listable'], function($) {

    $(document).ready(function () {
        $(".flatpickr").flatpickr({
            altInput: true,
            dateFormat: SiteConfig.FRONTEND_DATE_DATA_FORMAT,
            altFormat: SiteConfig.FRONTEND_DATE_DISPLAY_FORMAT,
            maxDate: new Date()
        });
        $("#user-table").dataTable({
            bPaginate: false,
            bFilter: false
        });
    });
});
