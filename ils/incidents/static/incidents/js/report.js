
require([
    'jquery',
    'lodash',
    'moment',
    'toastr',
    'flatpickr',
    'select2',
], function ($, _, moment, toastr) {

    let flatpickr_options = {
        dateFormat: SiteConfig.FRONTEND_DATE_DATA_FORMAT,
        altFormat: SiteConfig.FRONTEND_DATE_DISPLAY_FORMAT,
        altInput: true,
        allowInput: true
        // wrap: true
    };

    let toastr_actual_clinical_options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "0",
        "extendedTimeOut": "0",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        // "tapToDismiss": true
    };

    let toastr_options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "3000",
        "extendedTimeOut": "5000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        // "tapToDismiss": true
    };

    var all_popovers = $(".ils-popover");

    function set_patient_id_text() {
        var txt = "Patient id";
        if ($("#id_incident_type").find("option:selected").val() === "clinical") {
            txt += "*";
        }
        $("label[for=patient_id]").text(txt);
    }

    function set_severity_choices() {

        var it = $("#id_incident_type").find("option:selected").val();
        var element = $("#id_severity");
        var cur_severity = element.find("option:selected").val();

        var severities = SEVERITIES[it] || [];
        var options = '<option value>-----</option>' + _.map(severities, function (e) {
            return '<option value="' + e.id + '">' + e.text + '</option>';
        }).join("");
        var help_text;
        if (it) {
            help_text = _.map(severities, function (e) {
                return '<strong>' + e.text + '</strong><br/>' + e.description;
            }).join("<br/>");
        } else {
            help_text = "Severity definitions are dependent on Incident type. Please select incident type before selecting Severity";
        }

        element.html(options);
        element.popover("dispose").parents(".ils-popover").attr("data-content", help_text);
        element.attr("placeholder", help_text);

        element.on('focus', function () {
            all_popovers.popover('hide');
            $(this).parents(".ils-popover").popover("show");
        });

        //element.val(cur_severity).removeClass("select2-hidden-accessible").select2({ }).on('focus',function(){
        //    all_popovers.popover('hide');
        //    console.log("Actually here");
        //    console.log($(this).parents(".ils-popover"));
        //    $(this).parents(".ils-popover").popover("show");
        //    if ($(this).select2("val")==="" || ($(this).select2("val") && $(this).select2("val").length===0)){
        //        $(this).select2("open");
        //    }
        //});

        // reselect severity if it exists
        element.find('option[value="' + cur_severity + '"]').attr("selected", "selected");
    }

    $(document).ready(function () {

        $("#id_incident_date").flatpickr(flatpickr_options);

        $('.ils-popover').popover({
            trigger: 'manual',
            animation: false,
            delay: 0,
            html: true
        });

        $("label.checkbox").click(function () {
            $(this).find("input").focus();
        });

        $("input, select, textarea").on('focus', function () {
            all_popovers.popover('hide');
            $(this).parents(".ils-popover").popover("show");
        });

        $("#id_actual, #id_incident_type").change(function () {

            // $("#ajax_messages").children(".clinical-actual").remove();
            var actual = $("#id_actual option:selected").text();
            var type = $("#id_incident_type option:selected").text();
            console.log(actual, type)
            if (actual === "Actual" && type === "Clinical") {
                toastr.options = toastr_actual_clinical_options;
                toastr['info'](
                    'You must notify your immediate supervisor of all Actual Clinical incidents. Additional notifications are dependent on the incident severity, please refer to the ILS Reference Guide.' +
                    '<br /><br /><button type="button" class="btn btn-flat btn-default float-right clear">Okay</button>'
                );

                // $("#ajax_messages").append(
                //     "<div class='sized-div alert alert-info clinical-actual' style='display: none;'><a class='close' href='#' data-dismiss='alert'>&times;</a>" +
                //     "<div><i class='icon-exclamation-sign'></i>&nbsp;You must notify your immediate supervisor of all Actual Clinical incidents.</div>" +
                //     "<div>Additional notifications are dependent on the incident severity, please refer to the ILS Reference Guide.</div></div>"
                // );
                // $(".alert").slideDown();
            } else {
                toastr.clear()
            }
        });

        $("#id_incident_type").change(function () {
            set_patient_id_text();
            set_severity_choices();
        }).trigger('change');

        $('select:not(".flatpickr-calendar *")').select2({
            minimumResultsForSearch: 10,
            width: '100%'
        }).on('select2:open', function () {
            all_popovers.popover('hide');
            $(this).parents(".ils-popover").popover("show");
            if ($(this).select2("val") === "" || ($(this).select2("val") && $(this).select2("val").length === 0)) {
                $(this).select2("open");
            }
        });
        $('#report-submit').one('click', function() {
            $('#id_incident_date').prop('readonly', false);
            $("#report-form").submit();
        })
        $("#report-form").submit(function () {
            $(window).unbind("beforeunload");
        });

        $(window).bind("beforeunload", function (e) {
            var form_inputs = $("#report-form").find("input, select, textarea").not("[type=hidden]").not(".select2-focusser, #id_incident_date, #id_email, #id_submitted_by, #id_auth_password");

            if (_.any(_.pluck(form_inputs, "value"))) {
                var e = e || window.event;

                if (e) {
                    e.returnValue = '';
                }

                return 'Are you sure you want to leave this page without submitting your incident?';
            }
        });

        toastr.options = toastr_options;
        _.each(errorMessages, function(error) {
            toastr[error[0]](error[1]);
        });

    });

});