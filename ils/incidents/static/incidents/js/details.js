"use strict";

require(['jquery', 'knockout', 'lodash', 'toastr'], function($, ko, _, toastr) {

    let toastr_options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "3000",
        "extendedTimeOut": "5000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        // "tapToDismiss": true
    };


    let MIN_COMMENT_LENGTH = 8;
    if (typeof String.prototype.startsWith != 'function') {
        String.prototype.startsWith = function (str) {
            return this.indexOf(str) === 0;
        };
    }

    $(document).ready(function () {

        let all_popovers = $(".ils-popover");
        $(all_popovers).popover({
            trigger: 'manual',
            animation: false,
            delay: 0,
            html: true,
            placement: 'right'
        });

        function set_severity_choices() {

            const it = $("#id_incident_type").find("option:selected").val()
            const element = $("#hide-id_severity").children().find(".select2-selection")
            const cur_severity = element.find("option:selected").val()

            const severities = SEVERITIES[it] || []
            let help_text

            if (it) {
                help_text = _.map(severities, function (e) {
                    return '<strong>' + e.text + '</strong><br/>' + e.description
                }).join("<br/>")
            } else {
                help_text = "Severity definitions are dependent on Incident type. Please select incident type before selecting Severity";
            }

            console.log('----------------------')
            console.log(severities)

            element.popover("dispose").parents(".ils-popover").attr("data-content", help_text)
            element.on('focus', function () {
                all_popovers.popover('hide')
                const $this = $(this)
                $(this).parents(".ils-popover").popover("show")
                $('.popover').each(function () {
                    const x_left = $($this).offset().left + 250
                    $(this).css('left', x_left + 'px')
                })
            }).on("focusout", function () {
                all_popovers.popover('hide')
            });

            // reselect severity if it exists
            element.find('option[value="' + cur_severity + '"]').attr("selected", "selected")
        }

        set_severity_choices();

        $("#id_incident_type").change(function () {
            set_severity_choices();
        });

        const assigned = $("#id_assigned");
        $(assigned).val(formatDate($(assigned).val()));
        // const incident_date = $("#id_incident_date");
        // $(incident_date).val(formatDate($(incident_date).val(), true));

        const s2query = "select:visible:not([readonly]):not('s2-already'):not('#id_theme')";
        const newFormsetChanges = {};

        $('#add_more_act').click(function (e) {
            const actions_index = $('#id_actions-TOTAL_FORMS').val();
            const $newDiv = $($('#empty_action_form').html().replace(/__prefix__/g, actions_index)).hide();
            $('#actions-formset').prepend($newDiv);
            $('#id_actions-TOTAL_FORMS').val(parseInt(actions_index) + 1);
            $("div.modal-trigger").on('click', function () {
                curModal = $(this);
            });
            e.stopPropagation();
            $("#action-form-wrapper").slideDown('fast');
            $("#action-plus").hide();
            $("#action-minus").show();
            $($newDiv).slideDown('fast', function () {
                $(s2query).addClass("s2-already").select2({
                    minimumResultsForSearch: 10
                });
            });
            $("#close-empty-actions-" + actions_index).click(function () {
                $(this).parent().find("select, input, textarea").each(function () {
                    $(this).prop("disabled", true);
                    $($newDiv).slideUp('fast');
                });
            });
            newFormsetChanges["actions-" + actions_index] = {
                "action_type": "",
                "priority": "",
                "escalation": "",
                "responsible": "",
                "description": "",
                "complete": false
            };
        });

        $('#add_more_act').hover(
            function (e) {
                e.stopPropagation();
                $(this).parent().parent().parent().parent().removeClass("fading-header");
            },
            function (e) {
                $(this).parent().parent().parent().parent().addClass("fading-header");
            }
        );

        $('#add_more_sha').click(function (e) {
            const sharing_index = $('#id_sharing-TOTAL_FORMS').val();
            const $newDiv = $("<div style='display: none;'>" + $('#empty_sharing_form').html().replace(/__prefix__/g, sharing_index) + "</div>");
            $('#sharing-formset').prepend($newDiv);
            $('#id_sharing-TOTAL_FORMS').val(parseInt(sharing_index) + 1);
            $("div.modal-trigger").on('click', function () {
                curModal = $(this);
            });
            e.stopPropagation();
            $("#sharing-form-wrapper").slideDown('fast', function () {
                $(s2query).addClass("s2-already").select2({
                    minimumResultsForSearch: 10
                });
            });
            $("#sharing-plus").hide();
            $("#sharing-minus").show();
            $($newDiv).slideDown('fast', function () {
                $(s2query).addClass("s2-already").select2({
                    minimumResultsForSearch: 10
                });
            });
            $("#close-empty-sharing-" + sharing_index).click(function () {
                $(this).parent().find("select, input, textarea").each(function () {
                    $(this).prop("disabled", true);
                    $($newDiv).slideUp('fast');
                });
            });
            newFormsetChanges["sharing-" + sharing_index] = {
                "sharing_audience": "",
                "responsible": "",
                "done": false
            };
        });

        $('#add_more_sha').hover(
            function (e) {
                e.stopPropagation();
                $(this).parent().parent().parent().parent().removeClass("fading-header");
            },
            function (e) {
                $(this).parent().parent().parent().parent().addClass("fading-header");
            }
        );

        const $standDes = $("#id_standard_description");
        const $operaType = $("#id_operational_type");
        const $detDom = $("#id_detection_domain");
        const $oriDom = $("#id_origin_domain");
        const $psls = $("#id_psls_id");

        let originalVals = {
            "psls": $($psls).val(),
            "oriDom": $($oriDom).val(),
            "detDom": $($detDom).val(),
            "operaType": $($operaType).val(),
            "standDes": $($standDes).val()
        };

        // $("#id_theme").selectize({
        //     create: true,
        //     sortField: 'text'
        // });

        // $(".selectize-input input").addClass('basic-text-input input-block-level');

        function showStandDes() {
            $($standDes).val(originalVals.standDes);
            $('#hide-id_standard_description').slideDown('fast', function () {
                $(s2query).addClass("s2-already").select2({
                    minimumResultsForSearch: 10
                });
            });
        }

        function hideStandDes() {
            originalVals.standDes = $($standDes).val();
            $($standDes).prop('selectedIndex', 0);
            $('#hide-id_standard_description').slideUp('fast');
        }

        function showOperaType() {
            $($operaType).val(originalVals.operaType);
            $("#hide-id_operational_type").slideDown('fast', function () {
                $(s2query).addClass("s2-already").select2({
                    minimumResultsForSearch: 10
                });
            });
        }

        function hideOperaType() {
            originalVals.operaType = $($operaType).val();
            $($operaType).prop('selectedIndex', 0);
            $("#hide-id_operational_type").slideUp('fast');
        }

        function showPSLS() {
            $($psls).val(originalVals.psls);
            $("#hide-id_psls_id").slideDown('fast');
        }

        function hidePSLS() {
            originalVals.psls = $($psls).val();
            $($psls).val("");
            $("#hide-id_psls_id").slideUp('fast');
        }

        function showDetDom() {
            $($detDom).val(originalVals.detDom);
            $("#hide-id_detection_domain").slideDown('fast');
        }

        function hideDetDom() {
            originalVals.detDom = $($detDom).val();
            $($detDom).val("");
            $("#hide-id_detection_domain").slideUp('fast');
        }

        function showOriDom() {
            $($oriDom).val(originalVals.oriDom);
            $("#hide-id_origin_domain").slideDown('fast');
        }

        function hideOriDom() {
            originalVals.oriDom = $($oriDom).val();
            $($oriDom).val("");
            $("#hide-id_origin_domain").slideUp('fast');
        }

        // when 'actual' widget is changed
        $('#id_actual').change(function () {
            const act_pot = $('#id_actual').val();

            if (act_pot == "actual" && $("#id_incident_type").val() == "clinical") {
                showStandDes();
                showPSLS();
            } else {
                hideStandDes();
                hidePSLS();
            }
        });

        // when incident type widget is changed
        $("#id_incident_type").change(function () {
            const type = $(this).val();

            if (type == "clinical") {
                showOriDom();
                showDetDom();

                hideOperaType();

                if ($('#id_actual').val() == "actual") {
                    showStandDes();
                    showPSLS();
                }

            } else if (type == "operational") {
                showOperaType();

                hideStandDes();
                hidePSLS();
                hideDetDom();
                hideOriDom();
            } else {
                hideOperaType();
                hideStandDes();
                hidePSLS();
                hideDetDom();
                hideOriDom();
            }
        });

        // when investigator is changed
        $("#id_investigator").change(function () {
            if ($(this).val() == "") {
                $("#id_assigned").val("");
            }
        });

        $(s2query).addClass("s2-already").select2({
            minimumResultsForSearch: 10
        });


        function template_theme(data) {
            const $result = $("<span></span>");
            $result.text(data.text);

            if (data.id && data.id.indexOf('__new__') !== -1) {
                $result.append("<em> (new)</em>");
            }

            return $result;
        }

        var theme_select2_props = {
            templateSelection: template_theme,
            createTag: function (params) {
                if (params.term !== '') {
                    return {
                        id: '__new__' + params.term,
                        text: params.term,
                        newOption: true
                    }
                }
            },
            templateResult: template_theme,
            tags: true,
            width: '100%'
        };
        const $theme = $('#id_theme').select2(theme_select2_props);


        $(".update-investigation").click(function () {
            updateNewForms();
            $.ajax({
                url: AjaxURLS.update_incident.replace('0', thisInc),
                method: "POST",
                data: $("#investigation-form").serialize(),
                success: function (msg) {
                    console.log(msg)
                    newFormPKs(msg.new_form_pks, msg.new_theme);
                    applyErrors(msg.errors, msg.actions_errors, msg.sharing_errors);
                    applySuccess(msg.response_time, msg.changed, msg.formset_changed, msg.comment, msg.formset_comments, msg.assigned_info, msg.missing_fields, msg.investigator_changed);
                    updateMessages(msg.messages);
                    if (msg.errors == null && msg.actions_errors == null && msg.sharing_errors == null)
                        $(".formset-comment").val("");

                },
                error: function(err) {
                    updateMessages(['Internal server error']);
                }

            });
            return false;
        });

        function updateNewForms() {
            for (let prefix in newFormsetChanges) {

                let fieldNames, compDone;
                if (prefix.startsWith("act")) {
                    fieldNames = ["action_type", "priority", "escalation", "responsible", "description"];
                    compDone = "complete";
                } else if (prefix.startsWith("sha")) {
                    fieldNames = ["sharing_audience", "responsible"];
                    compDone = "done";
                }

                $("#initials-" + prefix).val("");
                for (let name_inx in fieldNames) {
                    if ($("#id_" + prefix + "-" + fieldNames[name_inx]).val() != newFormsetChanges[prefix][fieldNames[name_inx]]) {
                        $("#initials-" + prefix).val(function (inx, val) {
                            return val + fieldNames[name_inx] + ",";
                        });
                    }
                }
                if ($("#id_" + prefix + "-" + compDone).is(":checked") != newFormsetChanges[prefix][compDone]) {
                    $("#initials-" + prefix).val(function (inx, val) {
                        return val + compDone + ",";
                    });
                }
            }
        }

        function newFormPKs(forms, new_theme) {
            if (forms) {
                $.each(forms, function (prefix, pk) {
                    $("#new-form-pk-" + prefix).val(pk);
                });
            }
            if (new_theme) {
                var $theme_option = $('#id_theme option[value="__new__' + new_theme.name + '"]');
                $theme_option.val(new_theme.id);
                $theme_option.text(new_theme.name);
                $theme.select2("destroy");
                $theme.select2(theme_select2_props);
            }
        }

        let show_complete = $("#status-label-comp").is(':visible');

        function applySuccess(time, changed, f_changed, comment, f_comments, assigned, missing, inv_chg) {

            // If things are complete adjust complete label accordingly
            if (missing && missing.length == 0) {

                if (!show_complete) {
                    $("#status-label-notcomp").hide();
                    $("#status-label-comp").show();
                    $("#completed-time").text('Just now');
                    $(".missing").hide();
                    $(".complete").show();
                    show_complete = true;
                }

            } else if (show_complete) {
                $("#status-label-notcomp").show();
                $("#status-label-comp").hide();
                $(".missing").show();
                $(".complete").hide();
                show_complete = false;
            }

            $(".success").removeClass("success");
            // update form fields
            if (changed) {
                $.each(changed, function (i, m) {
                    if (m != "complete") {
                        $("#id_" + m).parent().parent().addClass("success");
                        if (m == 'flag') {
                            if ($("#id_flag").is(':checked')) {
                                $('.incident-flag i.icon-flag-alt').addClass('fas').removeClass('far');
                            } else {
                                $('.incident-flag i.icon-flag').addClass('far').removeClass('fas');
                            }
                        }
                    }
                });
            }

            // update investigator changed details
            if (inv_chg) {
                $("#id_assigned").val(formatDate(inv_chg));
            }

            // update form comments
            if (comment) {
                var date = formatDate(comment.datetime, false);

                $("<div class='form-group' style='display: none;'>" +
                    "<div class='row'>" +
                    "<div class='col-lg-4'>" +
                    date + "<br/>by " + comment.commenter + "</div>" +
                    "<div class='col-lg-8'><pre>" +
                    comment.text + "</pre></div><hr></div>"
                ).insertBefore("#div_id_comment").slideDown();
            }

            // update formset comments
            if (f_comments) {
                $.each(f_comments, function (prefix, comment_obj) {

                    const date = formatDate(comment_obj.datetime, false);
                    const $numCommentsElem = $("#num-comments-" + prefix);
                    const num = parseInt($($numCommentsElem).val());
                    $($numCommentsElem).val(num + 1);

                    $("<div class='form-group' style='display: none;'>" +
                        "<div class='row'>" +
                        "<div class='col-lg-4'>" +
                        date + "<br/>by " + comment_obj.commenter + "</div>" +
                        "<div class='col-lg-8'><pre>" +
                        comment_obj.text + "</pre></div><hr></div>"
                    ).insertBefore("#comment-div-" + prefix).slideDown();
                });
            }
            $('.comments textarea').val('');

            // update formset fields
            if (f_changed) {
                $.each(f_changed, function (prefix, fields) {

                    const $incompleteDiv = $("#incomplete-" + prefix);
                    const complete = ($("#id_" + prefix + "-done").is(":checked") || $("#id_" + prefix + "-complete").is(":checked"));
                    const numComments = parseInt($("#num-comments-" + prefix).val());

                    $.each(fields, function (index, field) {
                        const $id_prefix_field = $("#id_" + prefix + "-" + field);
                        $($id_prefix_field).parent().parent().addClass("success");

                        if (complete) $(".colour-" + prefix + "-" + field).addClass("success");

                        if (field == "complete" || field == "done") {

                            $($incompleteDiv).slideToggle();
                            $("#complete-" + prefix).slideToggle();

                            $("#comment-div-" + prefix).slideToggle();
                            if (numComments == 0 && complete) $("#comment-well-" + prefix).slideUp();
                            else $("#comment-well-" + prefix).slideDown();

                            let type, priority, escalation, responsible, description, audience;

                            if (complete) {

                                const formattedTime = formatDate(time, false);

                                if ($($incompleteDiv).hasClass("empty-act")) {

                                    type = $("#incomplete-" + prefix + " div.modal-trigger").text();
                                    priority = $("#id_" + prefix + "-priority option:selected").text();
                                    escalation = $("#id_" + prefix + "-escalation option:selected").text();
                                    responsible = $("#id_" + prefix + "-responsible option:selected").text();
                                    description = $("#id_" + prefix + "-description").val();

                                    $("#new-completed-" + prefix + "-type").text(type);
                                    $("#new-completed-" + prefix + "-priority").text(priority);
                                    $("#new-completed-" + prefix + "-escalation").text(escalation);
                                    $("#new-completed-" + prefix + "-responsible").text(responsible);
                                    $("#new-completed-" + prefix + "-description").text(description);

                                    $("#complete-date-" + prefix).text("Completed on " + formattedTime);

                                } else if ($($incompleteDiv).hasClass("empty-sha")) {

                                    audience = $("#incomplete-" + prefix + " div.modal-trigger").text();
                                    responsible = $("#id_" + prefix + "-responsible option:selected").text();

                                    $("#new-completed-" + prefix + "-sharing_audience").text(audience);
                                    $("#new-completed-" + prefix + "-responsible").text(responsible);

                                    $("#complete-date-" + prefix).text("Completed on " + formattedTime);

                                } else if ($($incompleteDiv).hasClass("action-form")) {

                                    type = $("#incomplete-" + prefix + " div.modal-trigger").text();
                                    priority = $("#id_" + prefix + "-priority option:selected").text();
                                    escalation = $("#id_" + prefix + "-escalation option:selected").text();
                                    responsible = $("#id_" + prefix + "-responsible option:selected").text();
                                    description = $("#id_" + prefix + "-description").val();

                                    $("#completed-" + prefix + "-type").text(type);
                                    $("#completed-" + prefix + "-priority").text(priority);
                                    $("#completed-" + prefix + "-escalation").text(escalation);
                                    $("#completed-" + prefix + "-responsible").text(responsible);
                                    $("#completed-" + prefix + "-description").text(description);

                                    $("#complete-date-" + prefix).text("Completed on " + formattedTime);

                                } else if ($($incompleteDiv).hasClass("sharing-form")) {

                                    audience = $("#incomplete-" + prefix + " div.modal-trigger").text();
                                    responsible = $("#id_" + prefix + "-responsible option:selected").text();

                                    $("#completed-" + prefix + "-sharing_audience").text(audience);
                                    $("#completed-" + prefix + "-responsible").text(responsible);

                                    $("#complete-date-" + prefix).text("Completed on " + formattedTime);
                                }
                            } else {
                                $("#complete-date-" + prefix).text("");
                            }
                        }
                    });

                    if ($($incompleteDiv).hasClass("empty-act")) {
                        $("#close-empty-" + prefix).remove();
                        $("#new-form-pk-" + prefix).val();

                        var fieldNames = ["action_type", "priority", "escalation", "responsible", "description"];
                        for (var name_inx in fieldNames) {
                            newFormsetChanges[prefix][fieldNames[name_inx]] = $("#id_" + prefix + "-" + fieldNames[name_inx]).val();
                        }
                        newFormsetChanges[prefix]["complete"] = $("#id_" + prefix + "-complete").is(":checked");

                    } else if ($($incompleteDiv).hasClass("empty-sha")) {
                        $("#close-empty-" + prefix).remove();
                        $("#new-form-pk-" + prefix).val();

                        var fieldNames = ["sharing_audience", "responsible"];
                        for (var name_inx in fieldNames) {
                            newFormsetChanges[prefix][fieldNames[name_inx]] = $("#id_" + prefix + "-" + fieldNames[name_inx]).val();
                        }
                        newFormsetChanges[prefix]["done"] = $("#id_" + prefix + "-done").is(":checked");
                    }

                    $("#new-form-" + prefix).val(false);
                });
            }

            // update assigned by and time info for responsible
            if (assigned) {
                $.each(assigned, function (prefix, assigned_obj) {
                    const when = formatDate(assigned_obj.when, false);

                    $("#assigned-tag-" + prefix).text("Assigned " + when + " by " + assigned_obj.who);
                    $("#completed-" + prefix + "-assigned-tag").text("(Assigned " + when + " by " + assigned_obj.who + ")");
                    const newAssignedTag = $("#new-assigned-tag-" + prefix);
                    $(newAssignedTag).text("Assigned " + when + " by " + assigned_obj.who);
                    if (!$(newAssignedTag).is(":visible")) $(newAssignedTag).slideDown();
                    $("#new-completed-" + prefix + "-assigned-tag").text("(Assigned " + when + " by " + assigned_obj.who + ")");
                });
            }

            // Check missing fields
            if (missing) {
                $("#missing_fields").html("");
                $.each(missing, function (index, missed) {
                    $("#missing_fields").append("<em>" + missed[1] + "</em>,&nbsp");
                });
            }

            originalVals = {
                "psls": $($psls).val(),
                "oriDom": $($oriDom).val(),
                "detDom": $($detDom).val(),
                "operaType": $($operaType).val(),
                "standDes": $($standDes).val()
            };

            if (originalVals.oriDom == "") {
                $("#ori-dom-btn").html('<i class="far fa-question-circle"></i>');
                $("#ori-dom-text").text("");
            }
            if (originalVals.detDom == "") {
                $("#det-dom-btn").html('<i class="far fa-question-circle"></i>');
                $("#det-dom-text").text("");
            }
        }

        function applyErrors(errors, act_errors, sha_errors) {

            $(".error-msg").addClass("remove-msg");

            if (errors) {
                $.each(errors, function (field, message) {
                    const $errorMsg = $("#error-field-" + field);
                    if ($errorMsg.length == 0) {
                        const $newDiv = $("<div id='error-field-" + field + "' class='error-msg help-block' style='display: none;'>" + message + "</div>");
                        const $control = $("#id_" + field);
                        $($control).parent().parent().addClass("has-error");
                        $newDiv.insertAfter($control).slideDown('fast');
                    } else {
                        $($errorMsg).text(message).removeClass("remove-msg");
                    }
                });
            }
            if (act_errors) {
                $.each(act_errors, function (prefix, error_obj) {
                    $.each(error_obj, function (field, message) {
                        const $errorMsg = $("#error-field-" + prefix + "-" + field);
                        if ($errorMsg.length == 0) {
                            const $newDiv = $("<div id='error-field-" + prefix + "-" + field + "'class='error-msg help-block' style='display: none;'>" + message + "</div>");
                            const $control = $("#id_" + prefix + "-" + field);
                            $($control).parent().parent().addClass("has-error");
                            $newDiv.insertAfter($control).slideDown('fast');
                        } else {
                            $($errorMsg).text(message).removeClass("remove-msg");
                        }
                    });
                })
            }
            if (sha_errors) {
                $.each(sha_errors, function (prefix, error_obj) {
                    $.each(error_obj, function (field, message) {
                        const $errorMsg = $("#error-field-" + prefix + "-" + field);
                        if ($errorMsg.length == 0) {
                            const $newDiv = $("<div id='error-field-" + prefix + "-" + field + "'class='error-msg help-block' style='display: none;'>" + message + "</div>");
                            const $control = $("#id_" + prefix + "-" + field);
                            $($control).parent().parent().addClass("has-error");
                            $newDiv.insertAfter($control).slideDown('fast');
                        } else {
                            $($errorMsg).text(message).removeClass("remove-msg");
                        }
                    });
                })
            }

            $.each($(".remove-msg"), function () {
                $(this).slideUp('fast', function () {
                    $(this).parent().parent().removeClass("has-error");
                    $(this).remove();
                });
            });
        }

        function updateMessages(messages) {
            // $("#ajax_messages").html("");
            // if (messages) {
            //     $.each(messages, function (i, m) {
            //         $("#ajax_messages").append("<div class='sized-div alert alert-" + m.extra_tags + "' style='display: none;'><a class='close' href='#' data-dismiss='alert'>&times;</a>" + m.message + "</div>");
            //     });
            //     $(".alert").slideDown();
            // }
            toastr.options = toastr_options;
            _.each(messages, function(v) {
                if (v.extra_tags)
                    toastr[v.extra_tags](v.message);
                else
                    toastr['error'](v);
            })
        }

        function formatDate(date, short) {

            const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            const month_sub = date.substr(3, 3);
            if ($.inArray(month_sub, months) > -1) {
                return date;
            }

            if (date) {
                if (short) {
                    /**
                     * Change date format to 'dd mmm yyyy
                     */
                    var year, month, day;

                    day = date.substr(8, 2).trim();
                    month = months[parseInt(date.substr(5, 2).trim()) - 1];
                    year = date.substr(0, 4);

                    date = day + " " + month + " " + year;
                    return date;

                } else {
                    /**
                     * Change date format to 'dd mmm yyyy hh:mm am
                     */
                    var year, month, day, hour, min, ampm;

                    day = date.substr(8, 2).trim();
                    month = months[parseInt(date.substr(5, 2).trim()) - 1];
                    year = date.substr(0, 4);
                    hour = date.substr(11, 2);
                    min = date.substr(14, 2);
                    ampm = "AM";

                    if (parseInt(hour) > 12) {
                        hour = (parseInt(hour) - 12).toString();
                        ampm = "PM";
                    }
                    date = day + " " + month + " " + year + " " + hour + ":" + min + " " + ampm;

                    return date;
                }
            }
            return '';
        }

        $("#show-submitter").click(function () {
            const sub_input = $("#id_submitted_by")[0];
            const value = sub_input.value;
            const sub_input_string = "input id='id_submitted_by' class='form-control' name='submitted_by' value='" + value + "' disabled readonly";
            if (sub_input.type == 'text') {
                $("<" + sub_input_string + " type='password'/>").insertBefore(sub_input);
            } else {
                $("<" + sub_input_string + " type='text'/>").insertBefore(sub_input);
            }
            $(sub_input).remove();
        });

        $("#hide-comments").click(function () {
            $("#comment-div").slideToggle('fast');
        });

        $("#reported-head").click(function () {
            $("#reported-form-wrapper").slideToggle('fast');
            $('#reported-head .card-tools i.fas').toggleClass('fa-minus').toggleClass('fa-plus');
        });

        $("#investigation-head").click(function () {
            $("#investigation-form-wrapper").slideToggle('fast');
            $('#investigation-head .card-tools i.fas').toggleClass('fa-minus').toggleClass('fa-plus');
        });

        $("#action-head").click(function () {
            $("#action-form-wrapper").slideToggle('fast');
            $('#action-head .card-tools i.fas').toggleClass('fa-minus').toggleClass('fa-plus');
            $(s2query).addClass("s2-already").select2({
                minimumResultsForSearch: 10
            });
        });

        $("#sharing-head").click(function () {
            $("#sharing-form-wrapper").slideToggle('fast');
            $("#sharing-plus").toggle();
            $("#sharing-minus").toggle();
            $(s2query).addClass("s2-already").select2({
                minimumResultsForSearch: 10
            });
        });

        $("#edit-investigation, #cancel-edit-investigation").click(function () {
            $("#investigation-form-wrapper, #investigation-details-display").toggle();
        });

        $("#id_technique").change(function (e) {
            const modality = TechniqueModality[$(this).val()];
            $("#id_modality").val(modality);
        });

        $("#submit-invalid, #submit-reopen").prop("disabled", true);

        $("#invalid-comment").bind('input propertychange', function () {
            const disabled = $(this).val().length < MIN_COMMENT_LENGTH;
            $("#submit-invalid").prop("disabled", disabled);
        });

        $("#reopen-comment").bind('input propertychange', function () {
            const disabled = $(this).val().length < MIN_COMMENT_LENGTH;
            $("#submit-reopen").prop("disabled", disabled);
        });

        $("a.modal-link").popover({
            trigger: "hover",
            container: "body"
        });

        $("#complete").click(function (e) {
            e.preventDefault();
        });

        var curModal = null;

        $(".modal-link").on('click', function (evt) {
            const pk = $(evt.target).data("pk");
            let ancestors = "";
            let txt = '<i class="far fa-question-circle"></i>';
            if (pk) {
                ancestors = $(evt.target).siblings("input").val();
                txt = $(this).text();
            }

            curModal.html(txt);
            curModal.parent().find("input").val(pk);//select2("val",pk);
            curModal.parent().find("span.ancestors").html(ancestors);
            curModal = null;
        });

        $("div.modal-trigger").on('click', function () {
            curModal = $(this);
        });

        $("a.incident-flag").click(function () {
            const el = $(this);
            const icon = el.find("i");
            const id = el.data("incident");

            // note, I have no idea why Django.js is putting "triage" rather than "incident" in the url :(
            const url = AjaxURLS.incident_flag.replace('0', id);

            $.ajax(url, {
                method: "PATCH",
                data: { flag: !el.hasClass("flagged") },
                success: function (res) {
                    el.toggleClass("flagged");
                    $("#id_flag").prop("checked", el.hasClass("flagged"));
                    icon.toggleClass('far').toggleClass('fas');
                },
            });
        });

        $(".save-and-continue").click(function () {
            $("#do-save-and-continue").val(1);
            $(this).prop('disabled', true);
            $('#investigation-form').submit();
        });

        // $("select#incident-selector").select2({}).on('select2-focus', function () {
        //     if ($(this).select2("val") === "" || ($(this).select2("val") && $(this).select2("val").length === 0)) {
        //         $(this).select2("open");
        //     }
        // });

        // Formats select2 selection options for duplicate
        function formatIncident(incident) {
            let $inc;
            if (incident.fields) {
                $inc = $(
                    "<span>" + incident.pk + ", " + incident.fields.incident_type + ", " + incident.fields.actual + "</span>"
                );
            } else {
                $inc = $(
                    "<span>" + incident.pk + "</span>"
                );
            }
            return $inc;
        }

        $("#incident-selector").select2({
            width: '100%',
            ajax: {
                //method: "GET",
                url: AjaxURLS.select_incident,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        thisInc: thisInc,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    const incs = JSON.parse(data.items);
                    for (let inc in incs) {
                        incs[inc].id = incs[inc].pk;
                    }
                    return {
                        results: incs
                    };
                },
                cache: false
            },
            templateResult: formatIncident,
            templateSelection: function (incident) {
                let $inc;
                if (incident.fields) {
                    $inc = incident.pk + ", " + incident.fields.incident_type + ", " + incident.fields.actual;
                } else {
                    $inc = incident.pk;
                }
                $("#duplicate-comment").val(incident.pk);
                return $inc;
            },
            escapeMarkup: function (markup) {
                return markup;
            } // let our custom formatter work

        }).on("change", function (e) {
            $("#submit-duplicate").prop('disabled', false);
        });

        let $unsubscribe_btn = $('#unsubscribe_btn'),
            $subscribe_btn = $('#subscribe_btn'),
            $subscribed_p = $('#subscribed'),
            $unsubscribed_p = $('#unsubscribed'),
            $unsub_toast = $('#unsub_toast'),
            $sub_toast = $('#sub_toast'),
            // $unsub_cancel = $('#unsub_cancel'),
            $unsub_okay = $('#unsub_okay'),
            show_sub_toast = true;

        let toastr_options_sub = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "100",
            "hideDuration": "100",
            "timeOut": "0",
            "extendedTimeOut": "0",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false,
            onShown: function() {
                $('.toast-message #unsub_cancel').click(function() {
                    show_sub_toast = true;
                    toastr.clear($('.toast'), {force: true});
                });
                $('.toast-message #unsub_okay').one('click', function() {
                    toastr.clear($('.toast'), {force: true});
                    sub_unsub();
                });
                $('.toast-message #sub_cancel').click(function() {
                    show_sub_toast = true;
                    toastr.clear($('.toast'), {force: true});
                });
                $('.toast-message #sub_okay').one('click', function() {
                    toastr.clear($('.toast'), {force: true});
                    sub_unsub();
                });
            }
        };

        $unsubscribe_btn.click(function () {
            if (!show_sub_toast) return;

            show_sub_toast = false;
            toastr.options = toastr_options_sub;
            toastr['info']($unsub_toast.html());

        });

        $subscribe_btn.click(function () {
            if (!show_sub_toast) return;

            show_sub_toast = false;
            toastr.options = toastr_options_sub;
            toastr['info']($sub_toast.html());

        });

        function sub_unsub() {
            $.ajax({
                url: AjaxURLS.sub_unsub,
                data: {object_id: object_id},
                method: 'POST',
                success: function(res) {
                    setTimeout(function() {
                        console.log(res)
                        show_sub_toast = true;
                        toastr.options = toastr_options;
                        toastr['success'](res.message);
                        if (res.subbed) {
                            $unsubscribed_p.hide();
                            $subscribed_p.fadeIn('fast');
                        } else {
                            $subscribed_p.hide();
                            $unsubscribed_p.fadeIn('fast');
                        }
                    }, 500);
                },
                error: function (res) {
                    console.log(res)
                    show_sub_toast = true;
                    toastr.options = toastr_options;
                    toastr['error'](res.responseJSON.message);
                }
            })
        }





    });
});