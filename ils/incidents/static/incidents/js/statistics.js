
require(['jquery', 'lodash', 'flot', 'flot.categories', 'flot.stack', 'flot.tickrotor', 'listable', 'select2'], function($, _) {

    function setupSummary() {

        const stack = 0,
            bars = true,
            lines = false,
            steps = false;

        const options = {
            series: {
                stack: true,
                lines: {
                    show: false,
                    fill: true,
                    steps: false
                },
                bars: {
                    show: true,
                    barWidth: 0.8,
                    align: "center",
                    lineWidth: 1
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0

            },
            yaxis: {
                autoscaleMargin: 0.08,
                minTickSize: 1,
                tickDecimals: 0,
                min: 0
            },
            legend: {
                show: false
            }
        };


        const countsToFlot = function (counts) {
            return _.map(counts, function (i, l) {
                return {label: l, data: i};
            });
        };

        const countsToFlotHoriz = function (counts) {
            return _.map(counts, function (i, l) {
                return {label: i, data: l};
            });
        };

        function plotStats(toChartData, legend, chart) {
            /** This function is called by each chart/legend combo in the Incidents Counts section of the Statistics page.
             * it creates the intereactive graph/legends.
             *
             * Uses flot: https://github.com/flot/flot/blob/master/API.md
             *
             * @param legend: the div that contains the clickable legend.
             * @param chart: the div that contains the chart.
             * @param toChartData: the data set needed to be graphed */
            let i = 0;

            $.each(toChartData, function (key, val) {
                val.color = i;
                ++i;
                l = val.label;
                const theDiv = $('<div class="small-legend-select row checked"name="' + l + '" id="' + l + '">').appendTo(legend);
                const cbb = $('<div class="checkbox-div-border">').appendTo(theDiv);
                $('<div class="checkbox-div" id="' + l + '-checkbox">').appendTo(cbb);
                $('<label>', {
                    text: l
                    //'for': l
                }).appendTo(theDiv);

                theDiv.click(function () {
                    $(this).toggleClass("checked");
                    $(this).toggleClass("unchecked");
                    plotAccordingToChoices();
                    legend.find(".checked").change(plotAccordingToChoices);
                });

                theDiv.hover(
                    function () {
                        $(".checkbox-div-border", this).css("background-color", "#dd4814");
                        $("label", this).css("color", "#dd4814")
                    },
                    function () {
                        $(".checkbox-div-border", this).css("background-color", "#474747");
                        $("label", this).css("color", "#474747")
                    }
                );
            });

            function plotAccordingToChoices() {
                const data = [];

                legend.find(".checked").each(function () {
                    const key = this.id;

                    for (let i = 0; i < toChartData.length; i++) {
                        if (toChartData[i].label === key) {
                            data.push(toChartData[i]);
                            return true;
                        }
                    }
                });

                const series = $.plot(chart, data, options).getData()
                console.log(series)
                for (let i = 0; i < series.length; ++i) {
                    const label = series[i].label
                    console.log(label)
                    console.log('#' + label + '-checkbox')
                    console.log(legend.find('#' + label + '-checkbox'))
                    legend.find('#' + label + '-checkbox').css("background-color", series[i].color)
                }
            }

            let previousPoint = null;

            chart.bind("plothover", function (event, pos, item) {
                $("#x").text(pos.x.toFixed(2));
                $("#y").text(pos.y.toFixed(2));

                if (item) {
                    if (previousPoint != item.datapoint) {
                        previousPoint = item.datapoint;

                        $("#tooltip").remove();
                        const x = item.datapoint[0].toFixed(2),
                            y = item.datapoint[1].toFixed(2);

                        showTooltip(item.pageX, item.pageY, item.series.label + " $" + y);
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });

            //function showTooltip(x, y, contents) {
            //    $('<div id="tooltip">' + contents + '</div>').css({
            //        position: 'absolute',
            //        display: 'none',
            //        top: y + 5,
            //        left: x + 15,
            //        border: '1px solid #fdd',
            //        padding: '2px',
            //        backgroundColor: '#fee',
            //        opacity: 0.80
            //    }).appendTo("body").fadeIn(200);
            //}

            plotAccordingToChoices();
            legend.find(".checked").change(plotAccordingToChoices);

            $('.legendColorBox > div').each(function (i) {
                $(this).clone().prependTo(legend.find("li").eq(i));
            });


        }

        const countData = countsToFlot(typeCounts);
        const severityData = countsToFlot(severityCounts);
        const statusData = countsToFlot(statusCounts);

        plotStats(countData, $("#incident-legend"), $("#incident-chart"));
        plotStats(severityData, $("#severity-legend"), $("#severity-chart"));
        plotStats(statusData, $("#status-legend"), $("#status-chart"));

    }

    function setupDomains() {

        const nCols = $("#domain-table").last().find("thead tr th").length;

        let i;
        const aoColumns = [];
        for (i = 0; i < nCols - 2; i++) {
            aoColumns.push({type: "select"});
        }

        const dt = $("#domain-table").dataTable({
            iDisplayLength: 20,
            sDom: '<<irp>><<rt>><<p>>',
            fnDrawCallback: function() {
                $('a.paginate_disabled_previous, a.paginate_disabled_next').addClass('btn btn-flat btn-small btn-light margin-left-5');
                $('.dataTables_info').addClass('float-left');
            }
        }).columnFilter({
            sPlaceHolder: "head:after",
            aoColumns: aoColumns
        });

        // $('a.paginate_disabled_previous, a.paginate_disabled_next').addClass('btn btn-flat btn-small btn-light margin-left-5 float-right');
        // $('.dataTables_info').addClass('float-left')


        const originDomainData = _.map(domainCounts.Origin, function (d) {
            return [d[1], d[0]];
        });
        const detectionDomainData = _.map(domainCounts.Detection, function (d) {
            return [d[1], d[0]];
        });
        const domainTicks = _.map(originDomainData, function (d, i) {
            return [i, d[1]];
        });

        const domainOptions = {
            series: {
                lines: {
                    show: false,
                    fill: true,
                    steps: false
                },
                bars: {
                    show: true,
                    horizontal: true,
                    barWidth: 0.8,
                    align: "center",
                    lineWidth: 1
                }
            },
            yaxis: {
                mode: "categories",
                tickLength: 0,
                ticks: domainTicks
            },
            xaxis: {
                autoscaleMargin: 0.08,
                minTickSize: 1,
                tickDecimals: 0
            }
        };

        $('a[href="#origin-domain-chart-tabpanel"]').on('shown.bs.tab', function (e) {
            console.log("SHOE")
            $.plot("#origin-domain-chart", [originDomainData], _.extend({}, domainOptions));
            $("#origin-domain-chart .flot-y-axis .tickLabel").css("left", 0);
        });
        //

        $('a[href="#detection-domain-chart-tabpanel"]').on('shown.bs.tab', function (e) {
            $.plot("#detection-domain-chart", [detectionDomainData], _.extend({}, domainOptions));
            $("#detection-domain-chart .flot-y-axis .tickLabel").css("left", 0);
        });

    }

    function setupCauses() {

        const nCols = $("#cause-table").last().find("thead tr th").length;

        let i;
        const aoColumns = [];
        for (i = 0; i < nCols - 1; i++) {
            aoColumns.push({type: "select"});
        }

        const dt = $("#cause-table").dataTable({
            iDisplayLength: 20,
            sDom: '<<irp>><<rt>><<p>>',
            fnDrawCallback: function() {
                $('a.paginate_disabled_previous, a.paginate_enabled_next, a.paginate_enabled_previous, a.paginate_disabled_next').addClass('btn btn-flat btn-small btn-light margin-left-5');
                $('.dataTables_info').addClass('float-left');
            }
        }).columnFilter({
            sPlaceHolder: "head:after",
            aoColumns: aoColumns
        });

        const data = _.map(causeCounts, function (d) {
            return [d[1], d[0]];
        });
        const ticks = _.map(data, function (d, i) {
            return [i, d[1]];
        });

        const options = {
            series: {
                lines: {
                    show: false,
                    fill: true,
                    steps: false
                },
                bars: {
                    show: true,
                    horizontal: true,
                    barWidth: 0.8,
                    align: "center",
                    lineWidth: 1
                }
            },
            yaxis: {
                mode: "categories",
                tickLength: 0,
                ticks: ticks
            },
            xaxis: {
                autoscaleMargin: 0.08,
                minTickSize: 1,
                tickDecimals: 0
            }
        };

        $('a[href="#cause-chart-tabpanel"]').on('shown.bs.tab', function (e) {
            $.plot("#cause-chart", [data], _.extend({}, options));
            $("#cause-chart .flot-y-axis .tickLabel").css("left", 0);
        });
    }

    $(document).ready(function () {

        let $year_select = $('#year-select');
        $year_select.select2({});
        $year_select.change(function () {
            const new_year = $(this).find("option:selected").val();
            window.location = './?year=' + new_year;
        });

        setupSummary();
        setupDomains();
        setupCauses();
    });
});
