import collections
import tablib

from django_comments.models import Comment
from django.contrib.contenttypes.models import ContentType
from django.utils import formats, timezone
from import_export import resources
from import_export.fields import Field

from incidents import models


class IncidentResource(resources.ModelResource):

    incident_id = Field("id", "ID")
    psls_id = Field("investigation__psls_id", "PSLS ID")

    incident_date = Field("incident_date", "Incident Date")
    submitted = Field("submitted", "Date Reported")
    submitted_by = Field("submitted_by", "Reported By")
    patient_id = Field("patient_id", "Patient ID")
    actual = Field("actual", "Actual/Potential")
    incident_type = Field("incident_type", "Incident Type")
    severity = Field("severity", "Inv Assigned Severity")
    severity_reported = Field('severity_reported', "Severity Reported")
    technique = Field("technique__name", "Technique")
    location = Field("location__name", "Location")
    description = Field("description", "Description")
    investigator = Field("investigation__investigator", "Investigator")
    theme = Field("investigation__theme", "Theme")
    date_assigned = Field("investigation__assigned", "Date Assigned")
    notes = Field(column_name="Investigation Notes")
    origin_domain = Field(column_name="Origin Domain")
    detection_domain = Field(column_name="Detection Domain")
    standard_description = Field("standard_description", "Standard Description")
    intent = Field("intent", "Treatment Intent")
    harm = Field("investigation__harm", "Degree of Harm")
    cause = Field("investigation__cause__name", "Cause")
    operational_type = Field("investigation__operational_type__name", column_name="Operational Incident Type")
    completed = Field("investigation__completed", "Date Completed")
    valid = Field("valid", "Used in Trending")
    duplicate_of = Field("duplicate_of", "Duplicate of Incident")
    n_actions = Field(column_name="# Actions")
    n_sharing = Field(column_name="# Sharing")

    class Meta:
        model = models.Incident
        fields = (
            "incident_id",
            "psls_id",
            "submitted",
            "submitted_by",
            "incident_date",
            "location",
            "patient_id",
            "incident_type",
            "severity_reported",
            "severity",
            "actual",
            "technique",
            "description",
            "investigator",
            "theme",
            "date_assigned",
            "notes",
            "detection_domain",
            "origin_domain",
            "standard_description",
            "intent",
            "harm",
            "cause",
            "operational_type",
            "completed",
            "valid",
            "duplicate_of",
            "n_actions",
            "n_sharing",
        )

        export_order = fields

    def __init__(self, *args, **kwargs):
        super(IncidentResource, self).__init__(**kwargs)

        self.domain_cache = {}
        for parent in models.Domain.objects.filter(parent=None):
            for leaf in parent.get_leafnodes():
                self.domain_cache[leaf.pk] = leaf.ancestor_names_plain()

        all_comments = Comment.objects.filter(
            content_type=ContentType.objects.get_for_model(models.Investigation),
        ).select_related("user")

        self.comment_cache = collections.defaultdict(list)
        for c in all_comments:
            comment = "%s : %s : %s" %(formats.date_format(timezone.localtime(c.submit_date), "DATETIME_FORMAT"), c.user, c.comment)
            self.comment_cache[c.object_pk].append(comment)

        all_actions = models.IncidentAction.objects.select_related(
            "incident",
            "responsible",
            "action_type",
            "priority",
            "escalation"
        )

        self.actions_cache = collections.defaultdict(list)
        for ia in all_actions:
            self.actions_cache[ia.incident.pk].append(ia)

    def get_queryset(self, qs=None):
        if qs is None:
            qs = super(IncidentResource, self).get_queryset()

        return qs.select_related(
            "technique",
            "location",
            "standard_description",
            "investigation",
            "investigation__investigator",
            "investigation__origin_domain",
            "investigation__detection_domain",
            "investigation__operational_type",
            "investigation__cause",
        )

    def export(self, queryset=None, **kwargs):
        """
        Exports a resource.
        """
        queryset = self.get_queryset(queryset)
        headers = self.get_export_headers()
        data = tablib.Dataset(headers=headers)
        # Iterate without the queryset cache, to avoid wasting memory when
        # exporting large datasets.
        for obj in queryset:
            try:
                data.append(self.export_resource(obj))
            except:
                pass
        return data

    def dehydrate_incident_type(self, incident):
        return incident.get_incident_type_display()

    def dehydrate_actual(self, incident):
        return incident.get_actual_display()

    def dehydrate_intent(self, incident):
        return incident.get_intent_display()

    def dehydrate_notes(self, incident):
        return "\n-----------------------------------------------------------------\n".join(self.comment_cache[str(incident.investigation.pk)])

    def dehydrate_detection_domain(self, incident):
        d = incident.investigation.detection_domain
        return self.domain_cache[d.pk] if d else ""

    def dehydrate_origin_domain(self, incident):
        d = incident.investigation.origin_domain
        return self.domain_cache[d.pk] if d else ""

    def dehydrate_harm(self, incident):
        return incident.investigation.get_harm_display()

    def dehydrate_psls_id(self, incident):
        return incident.investigation.psls_id or ""

    def get_incident_actions(self, incident):
        return self.actions_cache[incident.pk]

    def dehydrate_action_type(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join([ia.action_type.name for ia in ias])

    def dehydrate_action_responsible(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join(["%s" % ia.responsible for ia in ias])

    def dehydrate_action_assigned(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join(["%s" % ia.assigned for ia in ias])

    def dehydrate_action_completed(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join(["%s" % ia.completed for ia in ias])

    def dehydrate_action_priority(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join([ia.priority.name if ia.priority else "N/A" for ia in ias])

    def dehydrate_action_escalation(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join([ia.escalation.name if ia.escalation else "N/A" for ia in ias])

    def dehydrate_action_description(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join([ia.description for ia in ias])

    def dehydrate_date_assigned(self, incident):
        if incident.investigation.assigned:
            return formats.date_format(timezone.localtime(incident.investigation.assigned), "DATETIME_FORMAT")
        else:
            return ""

    def dehydrate_submitted(self, incident):
        return formats.date_format(timezone.localtime(incident.submitted), "DATETIME_FORMAT")

    def dehydrate_incident_date(self, incident):
        return formats.date_format(incident.incident_date, "DATE_FORMAT")

    def dehydrate_n_actions(self, incident):
        return len(models.IncidentAction.objects.filter(incident=incident.pk))

    def dehydrate_n_sharing(self, incident):
        return len(models.IncidentSharing.objects.filter(incident=incident.pk))

    def dehydrate_completed(self, incident):
        if incident.investigation.completed:
            return formats.date_format(timezone.localtime(incident.investigation.completed), "DATETIME_FORMAT")
        else:
            return ""

    def dehydrate_duplicate_of(self, incident):
        if incident.duplicate_of:
            return incident.duplicate_of.pk
        else:
            return ""


class IncidentActionResource(resources.ModelResource):

    action_type = Field('action_type', 'Action Type')
    assigned_by = Field('assigned_by', 'Assigned By')

    class Meta:
        model = models.IncidentAction
        fields = (
            'id',
            'incident',
            'action_type',
            'priority',
            'escalation',
            'assigned',
            'assigned_by',
            'complete',
            'completed',
            'responsible',
            'description'
        )
        export_order = fields

    def dehydrate_action_type(self, ia):
        return ia.action_type.name

    def dehydrate_priority(self, ia):
        if ia.priority:
            return ia.priority.name
        return ''

    def dehydrate_escalation(self, ia):
        if ia.escalation:
            return ia.escalation.name
        return ''

    def dehydrate_responsible(self, ia):
        return ia.responsible.username

    def dehydrate_assigned_by(self, ia):
        return ia.assigned_by.username

    def dehydrate_complete(self, ia):
        return ["No", "Yes"][ia.complete]

    def dehydrate_completed(self, ia):
        if ia.complete:
            return str(timezone.localtime(ia.completed).replace(microsecond=0, tzinfo=None))
        return ''

    def dehydrate_assigned(self, ia):
        if ia.assigned:
            return str(timezone.localtime(ia.assigned).replace(microsecond=0, tzinfo=None))
        return ''


class IncidentSharingResource(resources.ModelResource):

    complete = Field('done', "Complete")
    completed = Field('done_date', "Completed")
    sharing_audience = Field('sharing_audience', 'Sharing Audience')
    assigned_by = Field('assigned_by', 'Assigned By')

    class Meta:
        model = models.IncidentSharing
        fields = (
            'id',
            'incident',
            'sharing_audience',
            'assigned',
            'assigned_by',
            'complete',
            'completed',
            'responsible'
        )
        export_order = fields

    def dehydrate_sharing_audience(self, ish):
        return ish.sharing_audience.name

    def dehydrate_responsible(self, ish):
        return ish.responsible.username

    def dehydrate_assigned_by(self, ish):
        return ish.assigned_by.username

    def dehydrate_complete(self, ish):
        return ["No", "Yes"][ish.done]

    def dehydrate_completed(self, ish):
        if ish.done:
            return str(timezone.localtime(ish.done_date).replace(microsecond=0, tzinfo=None))
        return ''

    def dehydrate_assigned(self, ish):
        if ish.assigned:
            return str(timezone.localtime(ish.assigned).replace(microsecond=0, tzinfo=None))
        return ''
