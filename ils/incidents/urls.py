
from django.urls import include, path, re_path
# import incidents.api.urls as api_urls
from incidents import views

urlpatterns = [
    # path('api/', include(api_urls)),
    path('config/', views.Config.as_view(), name="config"),
    path('dashboard/', views.Dashboard.as_view(), name="dashboard"),
    path('stats/', views.Statistics.as_view(), name="statistics"),
    path('stats/incomplete/', views.IncompleteStats.as_view(), name="incomplete-statistics"),
    path('report/', views.Report.as_view(), name="report"),
    path('', views.IncidentsList.as_view(), name="all-incidents"),
    path('triage/', views.Triage.as_view(), name="triage"),
    path('incomplete/', views.Incomplete.as_view(), name="incomplete-incidents"),
    path('complete/', views.Complete.as_view(), name="complete-incidents"),
    re_path(r'^complete/(?P<pk>\d+)/$', views.CompleteIncident.as_view(), name="complete-details"),  # TODO: Is this even used?
    path('actual/', views.Actual.as_view(), name="actual-incidents"),
    path('invalid/', views.Invalid.as_view(), name="invalid-incidents"),
    path('investigations/mine', views.MyInvestigations.as_view(), name="my-investigations"),
    path('mine/', views.MyIncidents.as_view(), name="my-incidents"),
    path('actions/', views.BaseIncidentActionList.as_view(), name="all-actions"),
    path('actions/incomplete/', views.IncompleteIncidentActions.as_view(), name="incomplete-actions"),
    path('actions/complete/', views.CompleteIncidentActions.as_view(), name="complete-actions"),
    path('actions/mine/', views.MyIncidentActions.as_view(), name="my-actions"),
    path('actions/mine/incomplete/', views.MyIncompleteIncidentActions.as_view(), name="my-incomplete-actions"),
    path('actions/mine/complete/', views.MyCompleteIncidentActions.as_view(), name="my-complete-actions"),
    path('sharing/', views.BaseIncidentShareList.as_view(), name="all-sharing"),
    path('sharing/incomplete/', views.IncompleteIncidentShares.as_view(), name="incomplete-sharing"),
    path('sharing/complete/', views.CompleteIncidentShares.as_view(), name="complete-sharing"),
    path('sharing/mine/', views.MyIncidentShares.as_view(), name="my-sharing"),
    path('sharing/mine/incomplete/', views.MyIncompleteIncidentShares.as_view(), name="my-incomplete-sharing"),
    path('sharing/mine/complete/', views.MyCompleteIncidentShares.as_view(), name="my-complete-sharing"),
    #
    # url(r'^updateincident/(?P<pk>\d+)/$', views.UpdateIncident.as_view(), name="update-incident"),
    path('selectincident/', views.SelectIncident.as_view(), name="select_incident"),
    re_path(r'update(?:/(?P<pk>\d+))?/$', views.Incident.as_view(), name="update_incident"),
    re_path(r'flag(?:/(?P<pk>\d+))?/$', views.IncidentFlag.as_view(), name="incident_flag"),
]

