
from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
from django.core.exceptions import ValidationError
from django.db.models import ObjectDoesNotExist
from django import forms
from django.forms import widgets
from django.forms.models import modelformset_factory
from django.utils import timezone
from django.utils.encoding import force_str
from django.utils.translation import gettext as _

from ils.utils import get_users_by_permission
from incidents import models

User = get_user_model()

REMOVE_HELP_MESSAGE = _('Hold down "Control", or "Command" on a Mac, to select more than one.')


class IncidentReportForm(forms.ModelForm):

    email = forms.BooleanField(label="Email", help_text="Email me updates about this incident", required=False)
    auth_password = forms.CharField(label="Password", required=True, widget=forms.PasswordInput())
    submitted_by = forms.CharField(label="Username", required=True)
    actual = forms.ChoiceField(choices=[("", "---------")]+models.ACTUAL_POTENTIAL_CHOICES, label="Actual or Potential")

    class Meta:
        model = models.Incident
        fields = ('incident_date', 'patient_id', 'actual', 'incident_type', 'severity', 'location', 'description', "email", "submitted_by", "auth_password", )

        widgets = {
            "description": forms.Textarea(attrs={"rows": 6}),
            "auth_password": forms.PasswordInput(),
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user")

        super(IncidentReportForm, self).__init__(*args, **kwargs)
        self.fields["incident_date"].input_formats = [settings.PYTHON_DATE_FORMAT]

        self.fields["actual"].help_text = "<br/>".join(["<strong>%s</strong><br/>%s" % (x[1], models.ACTUAL_POTENTIAL_DESCRIPTIONS[x[0]]) for x in models.ACTUAL_POTENTIAL_CHOICES])
        self.fields["incident_type"].help_text = "<br/>".join(["<strong>%s</strong><br/>%s" % (x[1], models.INCIDENT_TYPE_DESCRIPTIONS[x[0]]) for x in models.INCIDENT_TYPE_CHOICES])

        for field_name in self.fields:
            field = self.fields.get(field_name)
            field.help_text = field.help_text.replace(REMOVE_HELP_MESSAGE, "")
            if field:
                field.widget.attrs["placeholder"] = field.help_text
            if field_name != 'email':
                classes = field.widget.attrs.get('class', False)
                field.widget.attrs.update({'class': classes + ' form-control' if classes else 'form-control'})

    def clean(self):
        cleaned_data = super().clean()
        submitted_by = cleaned_data.get("submitted_by")
        password = cleaned_data.get("auth_password")
        self.user = authenticate(username=submitted_by, password=password)

        if self.user is not None:

            # replace string with actual user instance
            cleaned_data["submitted_by"] = self.user
        else:
            for n in ["submitted_by", "auth_password"]:
                if n in cleaned_data:
                    del cleaned_data[n]
                self._errors[n] = self.error_class(["Invalid Username & Password"])

        patient_id = cleaned_data.get("patient_id")
        if cleaned_data.get("incident_type", None) == models.CLINICAL and not patient_id:
            self._errors["patient_id"] = self.error_class(["Clinical incidents must include a Patient ID"])

        cleaned_data["severity_reported"] = cleaned_data.get("severity")

        return cleaned_data


class IncidentSharingForm(forms.ModelForm):

    class Meta:
        model = models.IncidentSharing
        fields = ("sharing_audience", "responsible", "done")

    def __init__(self, *args, **kwargs):

        super(IncidentSharingForm, self).__init__(*args, **kwargs)

        valid_resp = get_users_by_permission("incidents.can_be_responsible_sharing", False).order_by("username")
        if self.instance.responsible_id:
            curr_resp = User.objects.filter(pk=self.instance.responsible_id)
            self.fields['responsible'].queryset = valid_resp | curr_resp
        else:
            self.fields['responsible'].queryset = valid_resp

        for name, f in self.fields.items():

            if not isinstance(f.widget, forms.CheckboxInput):
                f.widget.attrs['class'] = 'form-control'

            if isinstance(f.widget, forms.Textarea):
                f.widget.attrs['rows'] = 3


class IncidentActionForm(forms.ModelForm):

    class Meta:
        model = models.IncidentAction
        fields = ("action_type", "priority", "escalation", "responsible", "description", "complete",)

    def __init__(self, *args, **kwargs):

        super(IncidentActionForm, self).__init__(*args, **kwargs)

        valid_resp = get_users_by_permission("incidents.can_be_responsible_actions", False).order_by("username")
        if self.instance.responsible_id:
            curr_resp = User.objects.filter(pk=self.instance.responsible_id)
            self.fields['responsible'].queryset = valid_resp | curr_resp
        else:
            self.fields['responsible'].queryset = valid_resp

        for name, f in self.fields.items():

            if not isinstance(f.widget, forms.CheckboxInput):
                f.widget.attrs['class'] = 'form-control'

            if isinstance(f.widget, forms.Textarea):
                f.widget.attrs['rows'] = 3


class SelectizeModelChoiceField(forms.ModelChoiceField):

    def to_python(self, value):
        try:
            return super(SelectizeModelChoiceField, self).to_python(value)
        except ValidationError:
            models.Theme.objects.create(name=value)
        return super(SelectizeModelChoiceField, self).to_python(value)


class ThemeField(forms.ChoiceField):

    def clean(self, value):
        """

        Args:
            value: Either the pk of the storage as selected by room and location, or a string in the format
            of '__new__<name>'.

        Returns:
            String: if new storage is to be created. Returned value is location of new storage.
            Storage: if value was given as existing storage id.
            (field_name, ValidationError):  To raise ValidationError on related field in form.
        """
        if value in [None, '']:
            return None
            # return 'theme', ValidationError('This field is required')
        elif '__new__' in value:
            value = value.replace('__new__', '')
            if value.strip() == '':
                return 'theme', ValidationError('Invalid theme')
            return models.Theme(name=value.strip())
        else:
            try:
                theme = models.Theme.objects.get(pk=value)
                return theme
            except ObjectDoesNotExist:
                return 'theme', ValidationError("Incorrect Storage value")

    def has_changed(self, initial, data, tliib=False):
        if initial is None:
            if data in [None, '']:
                return False
            return True
        else:
            return force_str(initial) != force_str(data)


class Investigation(forms.ModelForm):

    incident_date = forms.DateField()
    actual = forms.ChoiceField(choices=[("", "---------")] + models.ACTUAL_POTENTIAL_CHOICES, label="Actual or Potential")
    patient_id = forms.CharField(max_length=15, help_text=_("Patient ID (where applicable)"), required=False)
    incident_type = forms.ChoiceField(choices=[("", "---------")]+list(models.INCIDENT_TYPE_CHOICES), required=True)
    severity = forms.ChoiceField(
        choices=[("", "--------")] + list(models.SEVERITY_CHOICES),
        required=False,
        label='Investigator Assigned Severity'
    )
    severity_reported = forms.CharField(required=False)
    location = forms.ModelChoiceField(queryset=models.LocationChoice.objects.all(), required=True)
    description = forms.CharField(widget=forms.Textarea(attrs={"rows": 2}))
    submitted_by = forms.CharField(label='Submitted By', required=False, widget=forms.TextInput(attrs={'type': 'password'}))

    flag = forms.BooleanField(required=False, label="Flag for discussion")
    intent = forms.ChoiceField(choices=[('', '---------')] + list(models.INTENT_CHOICES), required=False, label='Intent')
    num_affected = forms.ChoiceField(label="# Patients Affected", choices=models.NUM_AFFECTED_CHOICES, required=False)
    standard_description = forms.ModelChoiceField(queryset=models.StandardDescription.objects.all(), required=False, label='Standard Description')
    technique = forms.ModelChoiceField(queryset=models.Technique.objects.all(), required=False, label='Technique')
    complete = forms.BooleanField(required=False)

    assigned = forms.DateTimeField(required=False, label='Date Investigator Assigned')
    psls_id = forms.CharField(required=False, label='Psls ID')
    theme = ThemeField(
        # queryset=models.Theme.objects.all(),
        # to_field_name='name',
        required=False,
        label='Theme (optional)',
        # widget=widgets.ChoiceWidget(
        #     choices=(('', '-------') + (t.id, t.name) for t in models.Theme.objects.all())
        #
        # )
    )
    modality = forms.CharField(required=False)

    incident_fields = ["id_incident_date", "id_actual", "id_patient_id", "id_incident_type", "id_location", "id_description", "id_severity_reported", "id_submitted_by"]

    class Meta:

        model = models.Investigation

        fields = (
            'flag',
            'investigator',
            'assigned',
            'severity',
            'psls_id',
            'num_affected',
            'standard_description',
            'intent',
            'technique',
            'modality',
            'harm',
            'detection_domain',
            'origin_domain',
            'operational_type',
            'cause',
            'theme',
            'complete',
        )

    def __init__(self, *args, **kwargs):

        # used for permission checking here instead of in html
        if 'user' in kwargs:
            user = kwargs['user']
            del kwargs['user']
        else:
            user = None

        super(Investigation, self).__init__(*args, **kwargs)

        # if self.instance.theme:
        #     self.initial['theme'] = self.instance.theme.name

        if self.instance.incident.is_actual():
            self.fields["actual"].initial = models.ACTUAL
        elif self.instance.incident.is_potential():
            self.fields["actual"].initial = models.POTENTIAL

        self.fields["incident_date"].input_formats = settings.DATE_INPUT_FORMATS
        self.fields["incident_date"].initial = self.instance.incident.incident_date
        self.fields["patient_id"].initial = self.instance.incident.patient_id
        self.fields["incident_type"].initial = self.instance.incident.incident_type
        self.fields["location"].initial = self.instance.incident.location
        self.fields["description"].initial = self.instance.incident.description
        self.fields["severity_reported"].initial = self.instance.incident.severity_reported
        # if self.instance.incident.severity is not None:
        #     self.fields["severity"].initial = self.instance.incident.severity
        # else:
        #     self.fields["severity"].initial = self.instance.incident.severity_reported
        self.fields["severity"].initial = self.instance.incident.severity
        self.fields['submitted_by'].initial = self.instance.incident.submitted_by
        self.fields['submitted_by'].widget.attrs['readonly'] = True
        self.fields['submitted_by'].widget.attrs['editable'] = False
        self.fields['submitted_by'].widget.attrs['disabled'] = True

        # If assigned investigator has previously had investigation permissions removed, add them to query to avoid
        # forcing a new investigator
        valid_investigators = get_users_by_permission("incidents.can_investigate", False).order_by("username")
        if self.instance.incident.investigation and self.instance.incident.investigation.investigator:
            curr_investigator = User.objects.filter(pk=self.instance.incident.investigation.investigator.pk)
            self.fields['investigator'].queryset = valid_investigators | curr_investigator
        else:
            self.fields['investigator'].queryset = valid_investigators
        self.fields["flag"].initial = self.instance.incident.flag
        self.fields["intent"].initial = self.instance.incident.intent
        self.fields["num_affected"].initial = self.instance.incident.num_affected
        self.fields["standard_description"].initial = self.instance.incident.standard_description
        self.fields["technique"].initial = self.instance.incident.technique
        self.fields["complete"].widget.attrs['readonly'] = True
        self.fields["incident_date"].widget.attrs['readonly'] = True
        if self.instance.incident.technique:
            self.fields["modality"].initial = self.instance.incident.technique.modality
        else:
            self.fields["modality"].initial = ""

        self.fields["modality"].widget.attrs['readonly'] = True
        self.fields["severity_reported"].widget.attrs['readonly'] = True
        self.fields['assigned'].widget.attrs['readonly'] = True
        self.fields['assigned'].widget.attrs['disabled'] = True
        # if self.instance.assigned:
        #     self.fields['assigned'].initial = str(timezone.localtime(self.instance.assigned))

        if not self.instance.incident.valid:
            for field in self.fields:
                self.fields[field].widget.attrs['readonly'] = True
                self.fields[field].widget.attrs['editable'] = False
                self.fields[field].widget.attrs['disabled'] = True

        # if for some reason user has got this far and does not have perms to edit original report fields
        if user is not None:
            if not user.has_perm('incidents.change_incident'):
                for field in self.incident_fields:
                    self.fields[field.replace("id_", "", 1)].widget.attrs['readonly'] = True
        for field in self.fields:
            if type(self.fields[field].widget) != widgets.CheckboxInput:
                c = self.fields[field].widget.attrs.get('class', '')
                c += ' form-control'
                self.fields[field].widget.attrs.update({'class': c})
        # Formset invalid?
        self.set_theme_choices()

    def set_theme_choices(self):
        self.fields['theme'].widget.choices = [('', '-------')] + [(t.id, t.name) for t in models.Theme.objects.all()]

    def save(self, commit=True):

        super().save(commit=False)

        if self.cleaned_data.get("actual") == "actual":
            self.instance.incident.actual = models.ACTUAL
        else:
            self.instance.incident.actual = models.POTENTIAL

        self.instance.incident.incident_date = self.cleaned_data.get("incident_date")
        self.instance.incident.patient_id = self.cleaned_data.get("patient_id")
        self.instance.incident.incident_type = self.cleaned_data.get("incident_type")
        self.instance.incident.location = self.cleaned_data.get("location")
        self.instance.incident.description = self.cleaned_data.get("description")
        self.instance.incident.valid = self.cleaned_data.get("valid", True)
        self.instance.incident.flag = self.cleaned_data.get("flag", False)
        self.instance.incident.severity = self.cleaned_data.get("severity")
        self.instance.incident.intent = self.cleaned_data.get("intent")
        self.instance.incident.num_affected = self.cleaned_data.get("num_affected")
        self.instance.incident.technique = self.cleaned_data.get("technique")
        self.instance.incident.standard_description = self.cleaned_data.get("standard_description")

        if commit:
            if not self.instance.theme.id:
                self.instance.theme.save()
            self.instance.incident.save()

        return self.instance

    def clean(self):
        initial_assigned = self.initial['assigned']
        to_return = super().clean()
        to_return['assigned'] = initial_assigned
        return to_return


# # TODO implement this form (ie split up investigation form into Reported and Investigation
class ReportedForm(forms.ModelForm):

    incident_date = forms.DateField()
    actual = forms.ChoiceField(choices=[("", "---------")]+models.ACTUAL_POTENTIAL_CHOICES, label="Actual or Potential")
    patient_id = forms.CharField(max_length=255, help_text=_("Patient ID (where applicable)"), required=False)
    incident_type = forms.ChoiceField(choices=[("", "---------")]+list(models.INCIDENT_TYPE_CHOICES), required=True)
    severity = forms.ChoiceField(choices=[("", "---------")]+list(models.SEVERITY_CHOICES), required=False)
    location = forms.ModelChoiceField(queryset=models.LocationChoice.objects.all(), required=True)
    description = forms.CharField(widget=forms.Textarea(attrs={"class": "input-block-level", "rows": 2}))

    class Meta:

        model = models.Investigation

        fields = (
            "incident_date",
            "actual",
            "patient_id",
            "incident_type",
            "severity",
            "location",
            "description",
        )

    def __init__(self, *args, **kwargs):

        # used for permission checking here instead of in html
        if 'user' in kwargs:
            user = kwargs['user']
            del kwargs['user']
        else:
            user = None

        super(ReportedForm, self).__init__(*args, **kwargs)

        if self.instance.incident.is_actual():
            self.fields["actual"].initial = models.ACTUAL
        elif self.instance.incident.is_potential():
            self.fields["actual"].initial = models.POTENTIAL

        self.fields["incident_date"].input_formats = settings.DATE_INPUT_FORMATS
        self.fields["incident_date"].initial = self.instance.incident.incident_date
        self.fields["patient_id"].initial = self.instance.incident.patient_id
        self.fields["incident_type"].initial = self.instance.incident.incident_type
        self.fields["location"].initial = self.instance.incident.location
        self.fields["description"].initial = self.instance.incident.description
        self.fields["severity"].initial = self.instance.incident.severity

        if not self.instance.incident.valid:
            for field in self.fields:
                self.fields[field].widget.attrs['readonly'] = True
                self.fields[field].widget.attrs['editable'] = False
                self.fields[field].widget.attrs['disabled'] = True

    def save(self, *args, **kwargs):

        super(ReportedForm, self).save(*args, **kwargs)

        if self.cleaned_data.get("actual") == "actual":
            self.instance.incident.actual = models.ACTUAL
        elif self.cleaned_data.get("actual") == "potential":
            self.instance.incident.actual = models.POTENTIAL

        self.instance.incident.incident_date = self.cleaned_data.get("incident_date")
        self.instance.incident.patient_id = self.cleaned_data.get("patient_id")
        self.instance.incident.incident_type = self.cleaned_data.get("incident_type")
        self.instance.incident.location = self.cleaned_data.get("location")
        self.instance.incident.description = self.cleaned_data.get("description")
        self.instance.incident.severity = self.cleaned_data.get("severity")

        if "commit" not in kwargs or kwargs["commit"]:
            self.instance.incident.save()

        return self.instance


IncidentActionFormSet = modelformset_factory(models.IncidentAction, form=IncidentActionForm, extra=0)
IncidentSharingFormSet = modelformset_factory(models.IncidentSharing, form=IncidentSharingForm, extra=0)
