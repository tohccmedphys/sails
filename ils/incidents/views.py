
import collections
# import dateutil.parser
# import forms
import json
import re
import time

from listable import views as l_views

# from braces.views import JSONResponseMixin
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.core import serializers
from django.core.cache import cache
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.urls import reverse_lazy, resolve
from django.db.models import Max
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse, QueryDict
from django.views.generic import TemplateView, CreateView, DetailView, View, UpdateView
from django.template import Context, RequestContext
from django.template.loader import get_template
from django.utils import formats, timezone
from django.utils.translation import gettext as _
from django_comments.models import Comment

from incidents import forms as in_forms
from incidents import models
from notifications import signals
from notifications.models import Subscription


class StaffRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm("incidents.change_incident"):
            raise PermissionDenied("You don't have authorization to view this page")
        return super(StaffRequiredMixin, self).dispatch(request, *args, **kwargs)


User = get_user_model()
MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
cache.clear()


class RequestUserFormMixin(object):
    def get_form_kwargs(self):
        kwargs = super(RequestUserFormMixin, self).get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs


class Report(RequestUserFormMixin, CreateView):
    model = models.Incident
    form_class = in_forms.IncidentReportForm

    def form_valid(self, form):
        # self.get_form()

        incident = form.instance
        incident.flag = settings.FLAG_ALL_ON_REPORT
        incident.severity_reported = incident.severity
        response = super(Report, self).form_valid(form)
        user = form.cleaned_data["submitted_by"]
        signals.incident_submitted.send(
            sender=self,
            incident=incident,
            user=user,
            subscribe=bool(form.cleaned_data["email"])
        )
        messages.success(self.request,
                         "Thanks %s! Incident id=%d successfully reported" % (user.username, self.object.pk))
        return response

    def get_success_url(self):
        return reverse_lazy("report")

    def get_context_data(self, *args, **kwargs):
        context = super(Report, self).get_context_data(*args, **kwargs)
        severities = collections.defaultdict(list)
        for sdef in models.SeverityDefinition.objects.all():
            severities[sdef.incident_type].append(
                {'id': sdef.severity, 'text': sdef.get_severity_display(), 'description': sdef.definition})

        context["severities"] = json.dumps(severities)

        return context


class CompleteIncident(StaffRequiredMixin, DetailView):
    model = models.Incident
    context_object_name = "incident"
    template_name = "incidents/incident_complete_detail.html"

    def get_queryset(self):
        return models.Incident.complete.get_queryset()


class Incident(StaffRequiredMixin, DetailView):
    model = models.Incident
    next_id = 0

    def post(self, request, *args, **kwargs):

        self.object = self.model.objects.get(pk=self.kwargs["pk"])

        context = self.get_context_data(**kwargs)
        context["object"] = self.object

        dispatch = {
            "investigation_form": self.handle_investigation_form,
            "invalid_form": self.handle_invalid,
            "reopen_form": self.handle_reopen,
            "duplicate_form": self.handle_duplicate,
        }

        return dispatch[self.request.POST.get("form_name")](context)

    def get_queryset(self):
        return super(Incident, self).get_queryset().select_related(
            "investigation",
            "technique",
            "location",
            "submitted_by",
            "standard_description",
            "duplicate_of"
        )

    def get_context_data(self, *args, **kwargs):

        context = super(Incident, self).get_context_data(*args, **kwargs)
        incident = self.model.objects.get(pk=self.kwargs["pk"])
        investigation = incident.investigation
        context["incident"] = incident
        context["investigation"] = investigation

        to_handle = self.request.POST.get("form_name")

        if to_handle == "investigation_form":
            context["investigation_form"] = in_forms.Investigation(self.request.POST, instance=investigation)
            context["actions_formset"] = in_forms.IncidentActionFormSet(self.request.POST, queryset=models.IncidentAction.objects.filter(incident=self.object), prefix="actions")
            context["sharing_formset"] = in_forms.IncidentSharingFormSet(self.request.POST, queryset=models.IncidentSharing.objects.filter(incident=self.object), prefix="sharing")
        else:
            context["investigation_form"] = in_forms.Investigation(instance=investigation, user=self.request.user)
            context["actions_formset"] = in_forms.IncidentActionFormSet(queryset=models.IncidentAction.objects.filter(incident=self.object), prefix="actions")
            context["sharing_formset"] = in_forms.IncidentSharingFormSet(queryset=models.IncidentSharing.objects.filter(incident=self.object), prefix="sharing")

        for form in context["actions_formset"]:
            form.empty_permitted = True

        for form in context["sharing_formset"]:
            form.empty_permitted = True

        domain_roots = models.Domain.objects.filter(level=0).values_list("tree_id", flat=True)
        cause_roots = models.Cause.objects.filter(level=0).values_list("tree_id", flat=True)
        actions = models.ActionType.objects.order_by("strength").all()
        sharings = models.SharingAudience.objects.order_by("audience").all()

        context["causes"] = [models.Cause.objects.filter(tree_id=tree_id) for tree_id in cause_roots]
        context["domains"] = [models.Domain.objects.filter(tree_id=tree_id) for tree_id in domain_roots]
        context["actions"] = [(s[1], models.ActionType.objects.filter(strength=s[0])) for s in models.STRENGTH_CHOICES]
        context["sharing_audiences"] = [(s[1], models.SharingAudience.objects.filter(audience=s[0])) for s in models.SHARING_AUDIENCE_LEVELS]
        context["harms"] = [(h[0], h[1], models.DOH_DESCRIPTIONS[h[0]]) for h in models.DOH_CHOICES]
        context["severities"] = [
            (
                s.severity,
                s.get_severity_display(),
                s.definition
            ) for s in models.SeverityDefinition.objects.filter(incident_type=self.object.incident_type)
        ]
        context["techniques"] = json.dumps(dict([(x.pk, x.modality) for x in models.Technique.objects.all()]))
        context["themes"] = json.dumps(dict([(t.pk, t.name) for t in models.Theme.objects.all()]))

        context['subscription'] = incident.subscriptions.filter(user=self.request.user).count() > 0

        context['edit_actions'] = bool(self.request.GET.get("edit-actions", False))

        severities_mod = collections.defaultdict(list)
        for sdef in models.SeverityDefinition.objects.all():
            severities_mod[sdef.incident_type].append(
                {'id': sdef.severity, 'text': sdef.get_severity_display(), 'description': sdef.definition}
            )

        context["severities_mod"] = json.dumps(severities_mod)

        return context

    def get_next_url(self):

        default = reverse_lazy("update_incident", kwargs={"pk": self.object.pk})

        next_ = self.request.GET.get("next", default)
        # next_ = re.sub("^(.*)data\/?$", "\g<1>", next_)

        next_from = self.request.GET.get("continue", None)
        self.next_id = self.object.id

        if self.request.POST.get("do-save-and-continue", "0") != "0":
            try:
                pk = getattr(models.Incident, next_from).exclude(pk=self.object.pk).values("pk").latest("pk")['pk']
                self.next_id = pk
                next_ = "%s?next=%s&continue=%s&basic=1" % (reverse_lazy("update_incident", kwargs={"pk": pk}), next_, next_from)
            except (AttributeError, TypeError, ObjectDoesNotExist) as e:
                self.next_id = 0
                pass

        edit_actions = self.request.GET.get("edit-actions", "")
        if edit_actions:
            if "?" in next_:
                next_ += "&edit-actions=%s" % edit_actions
            else:
                next_ += "?edit-actions=%s" % edit_actions

        return next_

    def handle_investigation_form(self, context):

        form = context['investigation_form']
        actions_formset = context['actions_formset']
        sharing_formset = context['sharing_formset']
        response_data = {}
        response_data['formset_changed'] = {}
        response_data['new_form_pks'] = {}
        response_data['assigned_info'] = {}
        django_messages = []

        # Check here if form is not valid and comment is added
        # TODO create way for comments on non valid formset submitting to be saved for page reload
        for a_form in actions_formset:
            comment = self.request.POST.get("comment-" + a_form.prefix, "")
            if comment and comment.strip() and not a_form.has_changed():
                a_form.changed_data.append(comment)

        for s_form in sharing_formset:
            comment = self.request.POST.get("comment-" + s_form.prefix, "")
            if comment and comment.strip() and not s_form.has_changed():
                s_form.changed_data.append(comment)

        form.changed_data.remove('submitted_by')
        if 'assigned' in form.changed_data:
            form.changed_data.remove('assigned')

        if form.is_valid() and actions_formset.is_valid() and sharing_formset.is_valid():

            if not self.request.user.has_perm("incidents.change_investigation"):
                django_messages.append(messages.error(self.request, "Error updating Incident. You don't have the required permissions."))
                response_data['messages'] = django_messages
                return HttpResponse(json.dumps(response_data), content_type="application/json")

            inv = form.save(commit=False)
            cur_investigator = inv.investigator
            if form.initial['investigator']:
                prev_investigator = User.objects.get(id=form.initial['investigator'])
            else:
                prev_investigator = None

            investigator_changed = "investigator" in form.changed_data
            if investigator_changed:
                if form.cleaned_data['investigator'] is not None:
                    inv.assigned_by = self.request.user
                    inv.assigned = timezone.now()
                    response_data['investigator_changed'] = str(timezone.localtime(timezone.now()))
                else:
                    inv.assigned = None
                    inv.assigned_by = None
                form.changed_data.append('assigned')

            needed_to_complete = set(inv.missing_fields())
            required_complete = inv.required_complete()

            """ len(form.changed_data) here because changed_data always contains 'complete'. This removes
             the possibility of updating the completed data when changing nothing on a completed investigation"""
            just_completed = (not models.Investigation.objects.get(pk=inv.pk).complete) and required_complete

            inv.complete = required_complete
            if just_completed:
                inv.completed = timezone.now()

            if not required_complete:
                inv.completed = None

            if inv.theme and not inv.theme.id:
                inv.theme.save()
                response_data['new_theme'] = {'id': inv.theme.id, 'name': inv.theme.name}

            inv.save()
            inv.incident.save()

            comment = self.request.POST.get("comment", "")
            if comment and comment.strip():
                response_data['comment'] = {}
                saved_comment = self.create_comment_inv(comment)
                response_data['comment']['text'] = comment
                response_data['comment']['commenter'] = self.request.user.username
                response_data['comment']['datetime'] = str(timezone.localtime(timezone.now()))

            if investigator_changed and not required_complete:
                signals.investigator_assigned.send(sender=self, incident=inv.incident)
                if prev_investigator is not None and (prev_investigator != cur_investigator):
                    signals.investigator_unassigned.send(sender=self, incident=inv.incident, old_investigator=prev_investigator)

            if just_completed:
                signals.incident_completed.send(sender=self, incident=inv.incident)

            changed_act = [f for f in actions_formset if f.has_changed()]
            changed_sha = [f for f in sharing_formset if f.has_changed()]
            response_data['formset_comments'] = {}

            # Action formset stuff:
            to_update_act = []
            for a_form in changed_act:

                # is_new_form is found only in new form template. Initially it is set to True.
                # After initial abject saving, template value of is_new_form is set to 'false'
                # The logic here stops Django from making a new action object every time the update
                # button is clicked.
                is_new_form = self.request.POST.get("new-form-" + a_form.prefix)
                append_action_changed = False
                if is_new_form == "false":
                    the_pk = self.request.POST.get("new-form-pk-" + a_form.prefix)
                    action = models.IncidentAction.objects.get(pk=the_pk)
                    a_form.instance = action
                    del a_form.changed_data[:]
                    new_changed_data = self.request.POST.get("initials-" + a_form.prefix)
                    if new_changed_data is not None and new_changed_data != "":
                        append_action_changed = True
                        new_changed_data = new_changed_data[:-1].split(",")
                        for changed_field in new_changed_data:
                            a_form.changed_data.append(str(changed_field))
                            setattr(action, changed_field, a_form.cleaned_data[changed_field])
                else:
                    append_action_changed = True
                    action = a_form.save(commit=False)

                try:
                    prev_responsible = models.IncidentAction.objects.get(pk=a_form.instance.pk).responsible
                except ObjectDoesNotExist:
                    prev_responsible = None
                cur_responsible = action.responsible

                responsible_changed = "responsible" in a_form.changed_data
                if responsible_changed:
                    action.assigned_by = self.request.user
                    action.assigned = timezone.now()
                    response_data['assigned_info'][a_form.prefix] = {}
                    response_data['assigned_info'][a_form.prefix]['who'] = self.request.user.username
                    response_data['assigned_info'][a_form.prefix]['when'] = str(timezone.localtime(action.assigned))

                if "complete" in a_form.changed_data:
                    if a_form.cleaned_data['complete']:
                        action.completed = timezone.now()
                    else:
                        action.completed = None

                action.incident = self.object
                action.save()

                if is_new_form == "True":
                    response_data['new_form_pks'][a_form.prefix] = a_form.instance.pk

                comment = self.request.POST.get("comment-" + a_form.prefix, "")
                if comment and comment.strip():
                    saved_act_comment = self.create_comment_act(comment, action)
                    response_data['formset_comments'][a_form.prefix] = {}
                    response_data['formset_comments'][a_form.prefix]['text'] = comment
                    response_data['formset_comments'][a_form.prefix]['commenter'] = self.request.user.username
                    response_data['formset_comments'][a_form.prefix]['datetime'] = str(timezone.localtime(timezone.now()))

                if append_action_changed:
                    to_update_act.append(action)

                if responsible_changed:
                    signals.incident_action_assigned.send(sender=self, action=action)
                    if prev_responsible is not None and (prev_responsible != cur_responsible):
                        signals.incident_action_unassigned.send(sender=self, action=action, old_responsible=prev_responsible)

                response_data['formset_changed'][a_form.prefix] = a_form.changed_data

            # Sharing formset
            to_update_sha = []
            for s_form in changed_sha:

                # is_new_form is found only in new form template. Initially it is set to True.
                # After initial object saving, template value of is_new_form is set to 'false'
                # The logic here stops Django from making a new sharing object every time the update
                # button is clicked.
                is_new_form = self.request.POST.get("new-form-" + s_form.prefix)
                append_share_changed = False
                if is_new_form == "false":
                    the_pk = self.request.POST.get("new-form-pk-" + s_form.prefix)
                    share = models.IncidentSharing.objects.get(pk=the_pk)
                    s_form.instance = share
                    del s_form.changed_data[:]
                    new_changed_data = self.request.POST.get("initials-" + s_form.prefix)
                    if new_changed_data is not None and new_changed_data != "":
                        append_share_changed = True
                        new_changed_data = new_changed_data[:-1].split(",")
                        for changed_field in new_changed_data:
                            s_form.changed_data.append(str(changed_field))
                            setattr(share, changed_field, s_form.cleaned_data[changed_field])
                else:
                    append_share_changed = True
                    share = s_form.save(commit=False)

                try:
                    prev_responsible = models.IncidentSharing.objects.get(pk=s_form.instance.pk).responsible
                except ObjectDoesNotExist:
                    prev_responsible = None

                cur_responsible = share.responsible

                responsible_changed = "responsible" in s_form.changed_data
                if responsible_changed:
                    share.assigned_by = self.request.user
                    share.assigned = timezone.now()
                    response_data['assigned_info'][s_form.prefix] = {}
                    response_data['assigned_info'][s_form.prefix]['who'] = self.request.user.username
                    response_data['assigned_info'][s_form.prefix]['when'] = str(timezone.localtime(share.assigned))

                if "done" in s_form.changed_data:
                    if s_form.cleaned_data['done']:
                        share.done_date = timezone.now()
                    else:
                        share.done_date = None

                share.incident = self.object
                share.save()

                if is_new_form == "True":
                    response_data['new_form_pks'][s_form.prefix] = s_form.instance.pk

                comment = self.request.POST.get("comment-" + s_form.prefix, "")
                if comment and comment.strip():
                    self.create_comment_sha(comment, share)
                    response_data['formset_comments'][s_form.prefix] = {}
                    response_data['formset_comments'][s_form.prefix]['text'] = comment
                    response_data['formset_comments'][s_form.prefix]['commenter'] = self.request.user.username
                    response_data['formset_comments'][s_form.prefix]['datetime'] = str(timezone.localtime(timezone.now()))

                if append_share_changed:
                    to_update_sha.append(share)

                if responsible_changed:
                    signals.incident_sharing_assigned.send(sender=self, sharing=share)
                    if prev_responsible is not None and (prev_responsible != cur_responsible):
                        signals.incident_sharing_unassigned.send(sender=self, sharing=share, old_responsible=prev_responsible)

                response_data['formset_changed'][s_form.prefix] = s_form.changed_data

            # if to_update_act:
            #     signals.incident_actions.send(sender=self, incident=self.object, actions=to_update_act)
            # if to_update_sha:
            #     signals.incident_sharing.send(sender=self, incident=self.object, sharing=to_update_sha)

            messages.success(self.request, "Investigation successfully updated.")

            # try:
            next_ = self.get_next_url()

            if self.next_id == inv.incident.pk:

                for message in messages.get_messages(self.request):
                    django_messages.append({
                        "level": message.level,
                        "message": message.message,
                        "extra_tags": message.tags,
                    })
                response_data['messages'] = django_messages
                response_data['changed'] = form.changed_data
                response_data['response_time'] = str(timezone.localtime(timezone.now()))

                response_data['missing_fields'] = []

                for field in form.instance.missing_fields():
                    f = form.fields[field.replace('incident__', '')]
                    response_data['missing_fields'].append([field, f.label])
                # response_data['missing_fields'].sort()

                return HttpResponse(json.dumps(response_data), content_type="application/json")
            else:
                return HttpResponseRedirect(self.get_next_url())
            # except Exception as e:
            #     return HttpResponseRedirect(self.get_next_url())

        error_msg_act = ""
        error_msg_inv = ""
        error_msg_sha = ""
        if not actions_formset.is_valid():
            error_msg_act = " Actions."
        if not form.is_valid():
            error_msg_inv = " Investigation."
        if not sharing_formset.is_valid():
            error_msg_sha = " Sharing."
        messages.error(self.request, "Error updating:%s%s%s Please check form and try again." % (error_msg_act, error_msg_inv, error_msg_sha))
        for message in messages.get_messages(self.request):
            django_messages.append({
                "level": message.level,
                "message": message.message,
                "extra_tags": message.tags,
            })
        response_data['messages'] = django_messages
        response_data['errors'] = form.errors
        response_data['actions_errors'] = {}
        response_data['sharing_errors'] = {}

        for a_form in actions_formset:
            if a_form.errors:
                response_data['actions_errors'][a_form.prefix] = a_form.errors

        for s_form in sharing_formset:
            if s_form.errors:
                response_data['sharing_errors'][s_form.prefix] = s_form.errors

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

    def handle_invalid(self, context):

        if not self.request.user.has_perm("incidents.change_investigation"):
            messages.error(self.request, "Error updating Incident. You don't have the required permissions.")
            return self.render_to_response(context)

        obj = context["object"]
        obj.flag = False
        obj.valid = False
        obj.investigation.investigator = self.request.user
        obj.investigation.complete = True
        obj.investigation.completed = timezone.now().date()
        obj.save()
        obj.investigation.save()

        comment = self.request.POST.get("invalid-comment")
        self.create_comment_inv("NOT FOR TRENDING: " + comment)

        signals.incident_completed.send(sender=self, incident=obj)
        messages.success(self.request, "Incident Marked Not Appropriate for Trending")
        return HttpResponseRedirect(reverse_lazy("update_incident", kwargs={"pk": self.object.pk}))

    def handle_duplicate(self, context):

        if not self.request.user.has_perm("incidents.change_investigation"):
            messages.error(self.request, "Error updating Incident. You don't have the required permissions.")
            return self.render_to_response(context)

        obj = context["object"]
        dup = self.request.POST.get("duplicate-comment")

        try:
            if int(dup, base=10) < 0:
                raise ValueError
            obj.duplicate_of = self.model.objects.get(pk=int(dup, base=10))
        except (ValueError, TypeError):
            messages.error(self.request, "Error Marking Duplicate. Invalid Incident Number.")
            return HttpResponseRedirect(reverse_lazy("update_incident", kwargs={"pk": self.object.pk}))

        obj.flag = False
        obj.valid = False
        obj.investigation.investigator = self.request.user
        obj.investigation.complete = True
        obj.investigation.completed = timezone.now().date()
        obj.duplicate = True

        obj.save()
        obj.investigation.save()

        self.create_comment_inv("DUPLICATE OF " + dup)
        subs = Subscription.objects.filter(incident_id=obj.pk).all()
        for sub in subs:
            Subscription.objects.get_or_create(user=sub.user, incident_id=obj.duplicate_of.pk)

        signals.incident_duplicate.send(sender=self, incident=obj, duplicate=obj.duplicate_of)
        messages.success(self.request, "Incident Marked as Duplicate of Incident " + dup)
        return HttpResponseRedirect(reverse_lazy("update_incident", kwargs={"pk": self.object.pk}))

    def create_comment_inv(self, comment):

        c = Comment(
            content_type=ContentType.objects.get_for_model(models.Investigation),
            object_pk=self.object.investigation.pk,
            user=self.request.user,
            user_url="",
            comment=comment,
            submit_date=timezone.now(),
            site_id=settings.SITE_ID,
            is_public=True,
            is_removed=False,
        )
        c.save()
        return c

    def create_comment_act(self, comment, action):

        c = Comment(
            content_type=ContentType.objects.get_for_model(models.IncidentAction),
            object_pk=action.pk,
            user=self.request.user,
            user_url="",
            comment=comment,
            submit_date=timezone.now(),
            site_id=settings.SITE_ID,
            is_public=True,
            is_removed=False,
        )
        c.save()
        return c

    def create_comment_sha(self, comment, share):

        c = Comment(
            content_type=ContentType.objects.get_for_model(models.IncidentSharing),
            object_pk=share.pk,
            user=self.request.user,
            user_url="",
            comment=comment,
            submit_date=timezone.now(),
            site_id=settings.SITE_ID,
            is_public=True,
            is_removed=False,
        )
        c.save()
        return c

    def handle_reopen(self, context):

        if not self.request.user.has_perm("incidents.change_investigation"):
            messages.error(self.request, "Error updating Incident. You don't have the required permissions.")
            return self.render_to_response(context)

        obj = context["object"]
        obj.valid = True
        obj.flag = True
        obj.duplicate = False
        obj.duplicate_of = None

        if obj.investigation.required_complete():
            obj.investigation.complete = True
            obj.investigation.completed = timezone.now()
        else:
            obj.investigation.complete = False
            obj.investigation.completed = None

        obj.investigation.save()
        obj.save()

        comment = self.request.POST.get("reopen-comment", "")
        comment = self.create_comment_inv("REOPENED: " + comment)

        signals.incident_reopened.send(sender=self, incident=obj, comment=comment)
        messages.success(self.request, "Incident Re-opened")
        return HttpResponseRedirect(reverse_lazy("update_incident", kwargs={"pk": self.object.pk}))


class BaseIncidentsList(StaffRequiredMixin, l_views.BaseListableView):
    model = models.Incident
    template = 'incidents/list_incidents.html'
    page_title = 'Incidents'
    view_name = 'all-incidents'
    paginate_by = 50

    fields = (
        'flag',
        # 'button_actions',
        'id',
        'submitted',
        'incident_date',
        'investigation__completed',
        'severity',
        'actual',
        'incident_type',
        'description',
        'investigation__investigator__username',
        'actions',
        'sharing'
    )

    widgets = {
        'submitted': l_views.DATE_RANGE,
        'incident_date': l_views.DATE_RANGE,
        'investigation__completed': l_views.DATE_RANGE,
        'actual': l_views.SELECT,
        'severity': l_views.SELECT_MULTI,
        'incident_type': l_views.SELECT_MULTI,
        # 'investigation__investigator__username': l_views.SELECT
    }

    date_ranges = {
        'last_login': [
            l_views.TODAY, l_views.YESTERDAY, l_views.THIS_WEEK, l_views.LAST_WEEK,
            l_views.THIS_MONTH, l_views.LAST_MONTH, l_views.THIS_YEAR
        ],
    }

    order_fields = {
        'flag': 'flag',
        # 'button_actions': False,
        'id': 'id',
        'submitted': 'submitted',
        'incident_date': 'incident_date',
        'investigation__completed': 'investigation__completed',
        'severity': True,
        'actual': 'actual',
        'incident_type': 'incident_type',
        'description': False,
        'investigation__investigator__username': 'investigation__investigator__username',
        'actions': False,
        'sharing': False,
        'view': False
    }

    search_fields = {
        'flag': False,
        # 'button_actions': False,
        'id': 'id',
        'submitted': 'submitted',
        'incident_date': 'incident_date',
        'investigation__completed': 'investigation__completed',
        'severity': ('severity', 'actual'),
        'actual': 'actual',
        'incident_type': 'incident_type',
        'description': False,
        'investigation__investigator__username': 'investigation__investigator__username',
        'actions': False,
        'sharing': False,
        'view': False
    }

    headers = {
        'incident_date': _('Date'),
        'incident_type': _('Type'),
        'investigation__investigator__username': _('Investigator'),
        'investigation__completed': _('Completed'),
        'id': _('ID'),
        # 'button_actions': _('View')
    }

    order_by = ['-id']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Store templates on view initialization so we don't have to reload them for every row!
        self.templates = {
            'id': get_template('incidents/link_incident.html'),
        }

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['view_name'] = self.view_name
        context['page_title'] = self.page_title
        # context['icon'] = 'fa-user'

        return context

    def flag(self, obj):
        context = {'incident': obj}
        tmp = get_template('incidents/incident_flag.html')
        return tmp.render(context)

    def investigation__completed(self, obj):

        if obj.investigation.completed:
            txt = "%s" % formats.date_format(obj.investigation.completed, "SHORT_DATE_FORMAT")
            if not obj.valid:
                if obj.duplicate:
                    txt = 'Duplicate of %d' % obj.duplicate_of.pk
                else:
                    txt = 'Not Trending'
            # save_and_continue = self.get_save_and_continue(obj)
            # context = RequestContext(self.request, {
            #     "incident": obj,
            #     "save_and_continue": save_and_continue,
            #     "display": txt,
            #     "class": "btn btn-small",
            #     "style": "min-width: 90px"
            # })
            # tmp = get_template('incidents/link_incident_complete.html')
            # return tmp.render(context)
            return txt
        else:
            return ''

    def submitted(self, obj):
        return formats.date_format(obj.submitted, "SHORT_DATE_FORMAT")

    def severity(self, obj):
        return obj.get_severity_display()

    def incident_type(self, obj):
        return obj.get_incident_type_display()

    def actual(self, obj):
        return obj.actual_display()

    def investigation__investigator__username(self, obj):
        if not obj.investigation.investigator:
            return ''
        return obj.investigation.investigator.username

    def description(self, obj):
        context = {"incident": obj}
        tmp = get_template('incidents/incident_description.html')
        return tmp.render(context)

    def actions(self, obj):
        actions = obj.incidentaction_set.all()
        dirs = dir(obj)
        complete = actions.filter(complete=True).count()
        total = actions.count()
        return '<abbr title="%d of %d corrective actions completed">%d/%d</abbr>' % (complete, total, complete, total)

    def sharing(self, obj):
        sharing = obj.incidentsharing_set.all()
        complete = sharing.filter(done=True).count()
        total = sharing.count()
        return '<abbr title="%d of %d corrective sharing completed">%d/%d</abbr>' % (complete, total, complete, total)

    def get_save_and_continue(self, obj):
        return False

    def id(self, obj):
        save_and_continue = self.get_save_and_continue(obj)
        context = {
            "incident": obj,
            "save_and_continue": save_and_continue,
            "display": obj.id,
            'basic': True,
            'class': 'btn btn-sm btn-flat btn-default btn-table',
        }
        return self.templates['id'].render(context)

    # def view(self, obj):
    #     save_and_continue = self.get_save_and_continue(obj)
    #     context = {
    #         "incident": obj,
    #         "save_and_continue": save_and_continue,
    #         "display": "View",
    #         "class": "btn btn-small",
    #         'basic': True
    #     }
    #     tmp = get_template('incidents/link_incident.html')
    #     return tmp.render(context)


class IncidentsList(BaseIncidentsList):
    template_name = 'incidents/list_incidents.html'


class Complete(BaseIncidentsList):
    queryset = models.Incident.complete.all()
    template_name = "incidents/list_incidents_complete.html"
    view_name = 'complete-incidents'


class Actual(BaseIncidentsList):
    queryset = models.Incident.actuals.all()
    template_name = "incidents/list_incidents_actual.html"
    view_name = 'actual-incidents'

    fields = (
        'flag',
        'id',
        'submitted',
        'incident_date',
        'investigation__completed',
        'severity',
        'investigation__psls_id',
        'incident_type',
        'description',
        'investigation__investigator__username',
        'actions'
    )

    headers = {
        'investigation__investigator__username': _('Investigator'),
        'investigation__completed': _('Completed'),
        'investigation__psls_id': _('Psls id')
    }

    # def psls_id(self, obj):
    #     return obj.investigation.psls_id


class Incomplete(BaseIncidentsList):

    fields = (
        'flag',
        'id',
        'submitted',
        'incident_date',
        'severity',
        'actual',
        'incident_type',
        'description',
        'investigation__investigator__username',
        'actions',
        'sharing'
    )

    queryset = models.Incident.incomplete_or_flagged.all()
    template_name = "incidents/list_incidents_incomplete.html"
    view_name = 'incomplete-incidents'


class Invalid(BaseIncidentsList):
    queryset = models.Incident.objects.filter(valid=False)
    template_name = "incidents/list_incidents_invalid.html"
    view_name = 'invalid-incidents'


class MyIncidents(BaseIncidentsList):
    template_name = "incidents/list_incidents_mine.html"
    view_name = 'my-incidents'

    def get_queryset(self):
        qs = super(MyIncidents, self).get_queryset()
        return qs.filter(submitted_by=self.request.user)


class MyInvestigations(BaseIncidentsList):
    template_name = "incidents/list_incidents_investigations_mine.html"
    view_name = 'my-investigations'

    def get_queryset(self):
        qs = super(MyInvestigations, self).get_queryset()
        return qs.filter(investigation__investigator=self.request.user)


class Triage(BaseIncidentsList):
    queryset = models.Incident.triage.all()
    template_name = "incidents/list_incidents_triage.html"
    view_name = 'triage'

    fields = (
        'flag',
        'id',
        'submitted',
        'incident_date',
        'severity',
        'incident_type',
        'description'
    )

    def get_save_and_continue(self, obj):
        return "triage"

    def id(self, obj):
        save_and_continue = self.get_save_and_continue(obj)
        context = {
            "incident": obj,
            "save_and_continue": save_and_continue,
            "display": '{} (Triage)'.format(obj.id),
            'basic': True,
            'class': 'btn btn-xs btn-flat btn-default btn-table',
            "next": reverse_lazy("triage")
        }
        return self.templates['id'].render(context)


class BaseIncidentActionList(StaffRequiredMixin, l_views.BaseListableView):
    model = models.IncidentAction
    template_name = "incidents/list_actions.html"
    view_name = 'all-actions'
    paginate_by = 50

    fields = (
        'button_actions',
        'pk',
        'incident__id',
        'assigned',
        'responsible__username',
        'completed',
        'action_type__name',
        'priority__name',
        'escalation__name',
        'description'
    )

    widgets = {
        'button_actions': False,
        'responsible__username': l_views.SELECT_MULTI,
        'action_type__name': l_views.SELECT_MULTI,
        'priority__name': l_views.SELECT_MULTI,
        'escalation__name': l_views.SELECT_MULTI,
        'completed': l_views.DATE_RANGE,
        'assigned': l_views.DATE_RANGE
    }

    date_ranges = {
        'completed': [
            l_views.TODAY, l_views.YESTERDAY, l_views.THIS_WEEK, l_views.LAST_WEEK,
            l_views.THIS_MONTH, l_views.LAST_MONTH, l_views.THIS_YEAR
        ],
        'assigned': [
            l_views.TODAY, l_views.YESTERDAY, l_views.THIS_WEEK, l_views.LAST_WEEK,
            l_views.THIS_MONTH, l_views.LAST_MONTH, l_views.THIS_YEAR
        ],
    }

    order_fields = {
        'button_actions': False,
        'pk': 'pk',
        'incident__id': 'incident__id',
        'assigned': 'assigned',
        'responsible__username': 'responsible__username',
        'completed': 'completed',
        'action_type__name': 'action_type__name',
        'priority__name': 'priority__name',
        'escalation__name': 'escalation__name',
        'description': False
    }

    search_fields = {
        'button_actions': False,
        'pk': 'pk',
        'incident__id': 'incident__id',
        'assigned': 'assigned',
        'responsible__username': 'responsible__username',
        'completed': 'completed',
        'action_type__name': 'action_type__name',
        'priority__name': 'priority__name',
        'escalation__name': 'escalation__name',
        'description': False
    }

    headers = {
        'button_actions': 'View',
        'pk': _('ID'),
        'incident__id': _('Incident'),
        'responsible__username': _('Responsible'),
        'action_type__name': _('Action Type'),
        'priority__name': _('Priority'),
        'escalation__name': _('Escalation')
    }

    order_by = ["-pk"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Store templates on view initialization so we don't have to reload them for every row!
        self.templates = {
            'button_actions': get_template('incidents/link_incident.html'),
        }

    def get_context_data(self, *args, **kwargs):
        cd = super().get_context_data(*args, **kwargs)
        cd['view_name'] = self.view_name
        return cd

    def get_queryset(self):
        qs = super(BaseIncidentActionList, self).get_queryset()
        qs = qs.select_related("incident", "action_type", "assigned_by")
        return qs

    def assigned(self, obj):
        if obj.assigned is not None:
            return formats.date_format(obj.assigned, "SHORT_DATE_FORMAT")
        return "00 XXX 0000"

    def completed(self, obj):
        if obj.completed:
            return formats.date_format(obj.completed, "SHORT_DATE_FORMAT")
        return ""

    def responsible(self, obj):
        date = self.assigned(obj)
        return '<abbr class="popoverDesc" title="Assigned by %s on %s" data-title="Action #%d">%s</abbr>' % (
        obj.assigned_by, date, obj.pk, obj.responsible)

    def description(self, obj):
        context = {"action": obj}
        tmp = get_template('incidents/action_description.html')
        return tmp.render(context)

    def escalation__name(self, obj):
        return "" if obj.escalation is None else obj.escalation.name

    def priority__name(self, obj):
        return '' if obj.priority is None else obj.priority.name

    def button_actions(self, obj):
        context = {
            "incident": obj.incident,
            "display": 'View',
            'basic': True,
            'class': 'btn btn-xs btn-flat btn-default',
            "to_actions": True
        }
        return self.templates['button_actions'].render(context)

    # def incident(self, obj):
    #     context = {
    #         "incident": obj.incident,
    #         "save_and_continue": "",
    #         "display": "%s" % obj.incident.pk,
    #         "class": "",
    #         "anchor": "#learning",
    #         "to_actions": True,
    #         'basic': False
    #     }
    #     tmp = get_template('incidents/link_incident.html')
    #     return tmp.render(context)


class IncompleteIncidentActions(BaseIncidentActionList):
    template_name = "incidents/list_actions_incomplete.html"
    view_name = 'incomplete-actions'

    def get_queryset(self):
        qs = super(IncompleteIncidentActions, self).get_queryset().filter(complete=False)
        return qs


class CompleteIncidentActions(BaseIncidentActionList):
    template_name = "incidents/list_actions_complete.html"
    view_name = 'complete-actions'

    def get_queryset(self):
        qs = super(CompleteIncidentActions, self).get_queryset().filter(complete=True)
        return qs


class MyIncidentActions(BaseIncidentActionList):
    template_name = "incidents/list_actions_mine.html"
    view_name = 'my-actions'

    def get_queryset(self):
        qs = super(MyIncidentActions, self).get_queryset().filter(responsible=self.request.user)
        return qs


class MyIncompleteIncidentActions(MyIncidentActions):
    template_name = "incidents/list_actions_mine_incomplete.html"
    view_name = 'my-incomplete-actions'

    def get_queryset(self):
        qs = super(MyIncompleteIncidentActions, self).get_queryset().filter(responsible=self.request.user, complete=False)
        return qs


class MyCompleteIncidentActions(MyIncidentActions):
    template_name = "incidents/list_actions_mine_complete.html"
    view_name = 'my-complete-actions'

    def get_queryset(self):
        qs = super(MyCompleteIncidentActions, self).get_queryset().filter(responsible=self.request.user, complete=True)
        return qs


# # Sharing views ------------------------------------------------------------------------------------------
class BaseIncidentShareList(StaffRequiredMixin, l_views.BaseListableView):
    model = models.IncidentSharing
    template_name = "incidents/list_sharing.html"
    view_name = 'all-sharing'
    paginate_by = 50

    fields = (
        'button_actions',
        'pk',
        'incident__id',
        'assigned',
        'responsible__username',
        'done_date',
        'sharing_audience__name'
    )

    widgets = {
        'button_actions': False,
        'responsible__username': l_views.SELECT_MULTI,
        'sharing_audience__name': l_views.SELECT_MULTI,
        'done_date': l_views.DATE_RANGE,
        'assigned': l_views.DATE_RANGE
    }

    date_ranges = {
        'done_date': [
            l_views.TODAY, l_views.YESTERDAY, l_views.THIS_WEEK, l_views.LAST_WEEK,
            l_views.THIS_MONTH, l_views.LAST_MONTH, l_views.THIS_YEAR
        ],
        'assigned': [
            l_views.TODAY, l_views.YESTERDAY, l_views.THIS_WEEK, l_views.LAST_WEEK,
            l_views.THIS_MONTH, l_views.LAST_MONTH, l_views.THIS_YEAR
        ],
    }

    order_fields = {
        'button_actions': False,
        'pk': 'pk',
        'incident__id': 'incident__id',
        'assigned': 'assigned',
        'responsible__username': 'responsible__username',
        'done_date': 'done_date',
        'sharing_audience__name': 'sharing_audience__full_display'
    }

    search_fields = {
        'button_actions': False,
        'pk': True,
        'incident__id': 'incident__id',
        'assigned': 'assigned',
        'responsible__username': 'responsible__username',
        'done_date': 'done_date',
        'sharing_audience__name': 'sharing_audience__name'
    }

    headers = {
        'button_actions': _('View'),
        'pk': _('Sharing ID'),
        'responsible__username': _('Responsible'),
        'sharing_audience__name': _('Sharing Audience'),
        'done_date': _('Completed')
    }

    order_by = ["-pk"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Store templates on view initialization so we don't have to reload them for every row!
        self.templates = {
            'button_actions': get_template('incidents/link_incident.html'),
        }

    def get_context_data(self, *args, **kwargs):
        cd = super().get_context_data(*args, **kwargs)
        cd['view_name'] = self.view_name
        return cd

    def get_queryset(self):
        qs = super(BaseIncidentShareList, self).get_queryset()
        qs = qs.select_related("incident", "sharing_audience", "assigned_by", 'responsible')
        return qs

    def assigned(self, obj):
        if obj.assigned is not None:
            return formats.date_format(obj.assigned, "SHORT_DATE_FORMAT")
        return "00 XXX 0000"

    def done_date(self, obj):
        if obj.done_date:
            return formats.date_format(obj.done_date, "SHORT_DATE_FORMAT")
        return ""

    def responsible(self, obj):
        date = self.assigned(obj)
        return '<abbr class="popoverDesc" title="Assigned by %s on %s" data-title="Sharing #%d">%s</abbr>' % (obj.assigned_by, date, obj.pk, obj.responsible)

    # def incident(self, obj):
    #     context = {
    #         "incident": obj.incident,
    #         "save_and_continue": "",
    #         "display": "%s" % obj.incident.pk,
    #         "class": "",
    #         "anchor": "#learning",
    #         "to_sharing": True,
    #         'basic': False
    #     }
    #     tmp = get_template('incidents/link_incident.html')
    #     return tmp.render(context)

    def sharing_audience__name(self, obj):
        return obj.sharing_audience.full_display()

    def button_actions(self, obj):
        context = {
            "incident": obj.incident,
            "display": 'View',
            'basic': True,
            'class': 'btn btn-xs btn-flat btn-default',
            "to_sharing": True
        }
        return self.templates['button_actions'].render(context)


class IncompleteIncidentShares(BaseIncidentShareList):
    template_name = "incidents/list_sharing_incomplete.html"
    view_name = 'incomplete-sharing'

    def get_queryset(self):
        qs = super(IncompleteIncidentShares, self).get_queryset().filter(done=False)
        return qs


class CompleteIncidentShares(BaseIncidentShareList):
    template_name = "incidents/list_sharing_complete.html"
    view_name = 'complete-sharing'

    def get_queryset(self):
        qs = super(CompleteIncidentShares, self).get_queryset().filter(done=True)
        return qs


class MyIncidentShares(BaseIncidentShareList):
    template_name = "incidents/list_sharing_mine.html"
    view_name = 'my-sharing'

    def get_queryset(self):
        qs = super(MyIncidentShares, self).get_queryset().filter(responsible=self.request.user)
        return qs


class MyIncompleteIncidentShares(MyIncidentShares):
    template_name = "incidents/list_sharing_mine_incomplete.html"
    view_name = 'my-incomplete-sharing'

    def get_queryset(self):
        qs = super(MyIncompleteIncidentShares, self).get_queryset().filter(
            responsible=self.request.user,
            done=False
        )
        return qs


class MyCompleteIncidentShares(MyIncidentShares):
    template_name = "incidents/list_sharing_mine_complete.html"
    view_name = 'my-complete-sharing'

    def get_queryset(self):
        qs = super(MyCompleteIncidentShares, self).get_queryset().filter(
            responsible=self.request.user,
            done=True
        )
        return qs
# # End Sharing views ------------------------------------------------------------------------------------------


class Config(StaffRequiredMixin, TemplateView):
    template_name = "incidents/config.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Config, self).get_context_data(*args, **kwargs)
        context["report_containers"] = [
            ("location", "Locations", "choice"),
            ("severity", "Incident Severities", "choice"),
            ("standarddescription", "Standard Descriptions", "choice"),
        ]
        context["investigation_containers"] = [
            ("domain", "Detection & Origin Domains", "tree"),
            ("cause", "Causes", "tree"),
            ("priority", "Priority Choices", "choice"),
            ("escalation", "Escalation Choices", "choice"),
        ]
        return context


class Dashboard(StaffRequiredMixin, TemplateView):
    template_name = "incidents/dashboard.html"

    def post(self, request, *args, **kwargs):
        investigations = request.POST.get("investigations") == "on"
        actions = request.POST.get("actions") == "on"
        sharing = request.POST.get("sharing") == "on"

        self.request.user.investigation_notifications = investigations
        self.request.user.action_notifications = actions
        self.request.user.sharing_notifications = sharing
        self.request.user.save()
        messages.success(request, "Successfully updated your preferences")

        return HttpResponseRedirect(reverse_lazy("dashboard"))

    def add_incident_counts(self, context):

        from_ = timezone.now() - timezone.timedelta(days=365)

        now = timezone.now()
        last_year = now - timezone.timedelta(days=365)
        months = [time.localtime(time.mktime((now.year, now.month - n, 1, 0, 0, 0, 0, 0, 0)))[:2] for n in range(12)]
        months.reverse()

        def count_by_date(incidents):
            counts = []
            for year, month in months:
                inc = [x for x in incidents if x.incident_date.month == month and x.incident_date.year == year]
                counts.append(["%s" % (MONTHS[month - 1]), len(inc)])
            return counts

        def group_by(incidents, field, grouping):
            grouped_counts = {}
            for group_key, name in grouping:
                grouped_counts[name] = count_by_date(incidents.filter(**{field: group_key}))
            return grouped_counts

        incidents = models.Incident.objects.filter(incident_date__gte=last_year)

        type_counts = group_by(incidents, "incident_type", models.INCIDENT_TYPE_CHOICES)

        severity_grouping = models.SEVERITY_CHOICES
        severity_counts = group_by(incidents, "severity", severity_grouping)

        status_counts = {
            "Actual": count_by_date(incidents.filter(actual=models.ACTUAL, valid=True)),
            "Potential": count_by_date(incidents.filter(actual=models.POTENTIAL, valid=True)),
            "Invalid": count_by_date(incidents.filter(valid=False)),
        }

        context["months_ago_new_year"] = 12 - now.month - 0.5
        context["type_counts"] = json.dumps(type_counts)
        context["severity_counts"] = json.dumps(severity_counts)
        context["status_counts"] = json.dumps(status_counts)

    def add_summary_items(self, context):
        triage = models.Incident.triage.count()
        last_4_weeks = timezone.now() - timezone.timedelta(4 * 7)
        incs = models.Incident.objects.filter(submitted__gte=last_4_weeks)

        counts = collections.Counter()
        for inc in incs:
            counts[inc.incident_type] += 1

        type_counts = ["%d %s" % (cnt, dict(models.INCIDENT_TYPE_CHOICES)[itype],) for itype, cnt in counts.items()]

        items = [
            ("Triage",
             'There are currently %d incidents awaiting <a href="%s">triage</a>' % (triage, reverse_lazy('triage'),)),
            ("Last 4 weeks:", ", ".join(type_counts)),
        ]
        context["summary_items"] = items

    def get_context_data(self, *args, **kwargs):
        context = super(Dashboard, self).get_context_data(*args, **kwargs)

        qs = models.Investigation.objects.user_incomplete(self.request.user)
        qs = qs.select_related("incident")
        context['user_incomplete_investigations'] = qs

        actions = models.IncidentAction.objects.filter(responsible=self.request.user, complete=False)
        actions = actions.select_related("action_type", "incident", "assigned_by")
        sharing = models.IncidentSharing.objects.filter(responsible=self.request.user, done=False)

        context["user_actions"] = actions
        context["user_sharings"] = sharing

        self.add_incident_counts(context)
        self.add_summary_items(context)

        return context


class IncompleteStats(StaffRequiredMixin, TemplateView):
    template_name = "incidents/statistics_incomplete.html"

    def get_context_data(self):
        context = super(IncompleteStats, self).get_context_data()

        year = timezone.now().year
        year_start = timezone.datetime(year=year, day=1, month=1)

        from_date = self.request.GET.get("from_date", None)
        from_date = timezone.datetime.strptime(from_date, settings.PYTHON_DATE_FORMAT) if from_date is not None else year_start

        to_date = self.request.GET.get("to_date", None)
        to_date = timezone.datetime.strptime(to_date, settings.PYTHON_DATE_FORMAT) if to_date is not None else timezone.now().date()

        incidents = models.Incident.objects.filter(
            incident_date__gte=from_date,
            incident_date__lte=to_date
        ).select_related(
            "investigation",
            "investigation__investigator"
        )

        incomplete_and_valid = incidents.filter(valid=True, investigation__complete=False)

        by_user = collections.defaultdict(list)
        for inc in incomplete_and_valid:
            by_user[inc.investigation.investigator].append(inc)

        by_user = dict(by_user)

        context.update({
            'from_date': from_date.strftime(settings.PYTHON_DATE_FORMAT),
            'to_date': to_date.strftime(settings.PYTHON_DATE_FORMAT),
            'all_incidents': incidents,
            'invalid': incidents.filter(valid=False),
            'complete_and_valid': incidents.filter(valid=True, investigation__complete=True),
            'incomplete_and_valid': incomplete_and_valid,
            'by_user': by_user
        })

        return context


class Statistics(LoginRequiredMixin, TemplateView):
    template_name = "incidents/statistics.html"

    def add_incident_counts(self, context, start, end):

        # months = [time.localtime(time.mktime([start.year, n, 1, 0, 0, 0, 0, 0, 0]))[:2] for n in range(1, 13)]
        dates = [timezone.datetime(start.year, m, 1) for m in range(1, 13)]

        def count_by_date(incident_list):
            counts = []
            for date in dates:
                month = date.month
                year = date.year
                inc = [x for x in incident_list if x.incident_date.month == month and x.incident_date.year == year]
                # counts.append(["%s '%s" % (MONTHS[month - 1], str(year)[2:]), len(inc)])
                counts.append(["%s" % (MONTHS[month - 1]), len(inc)])
            return counts

        def group_by(incident_list, field, grouping):
            grouped_counts = {}
            grouped_counts2 = {}
            list_incidents = list(incident_list)
            for group_key, name in grouping:
                filtered = [x for x in list_incidents if getattr(x, field) == group_key]
                grouped_counts[name] = count_by_date(filtered)

            return grouped_counts

        incidents = models.Incident.objects.filter(incident_date__gte=start, incident_date__lt=end)

        type_counts = group_by(incidents, "incident_type", models.INCIDENT_TYPE_CHOICES)

        severity_grouping = models.SEVERITY_CHOICES
        severity_counts = group_by(incidents, "severity", severity_grouping)

        status_counts = {
            "Actual": count_by_date(incidents.filter(actual=models.ACTUAL, valid=True)),
            "Potential": count_by_date(incidents.filter(actual=models.POTENTIAL, valid=True)),
            "Invalid": count_by_date(incidents.filter(valid=False)),
        }

        context["type_counts"] = json.dumps(type_counts)
        context["severity_counts"] = json.dumps(severity_counts)
        context["status_counts"] = json.dumps(status_counts)

    def add_summary_table(self, context, start, end):

        incidents = models.Incident.objects.valid().filter(
            incident_date__gte=start,
            incident_date__lt=end
        )

        actuals = incidents.filter(actual=models.ACTUAL)
        potentials = incidents.filter(actual=models.POTENTIAL)
        invalid = models.Incident.objects.filter(
            valid=False,
            incident_date__gte=start,
            incident_date__lt=end
        )

        severities = [x[0] for x in models.SEVERITY_CHOICES]

        header = ["Status", "Type"] + [s[1] for s in models.SEVERITY_CHOICES] + ['Total']

        def tabulate_type(qs, name):
            list_qs = list(qs)
            totals = [len([x for x in list_qs if x.severity == s]) for s in severities]
            totals_row = [name, ''] + totals + [sum(totals)]
            table = [totals_row]
            for idx, (itype, name) in enumerate(models.INCIDENT_TYPE_CHOICES):
                sev_counts = [len([x for x in list_qs if x.incident_type == itype and x.severity == s]) for s in
                              severities]
                row = ['', name] + sev_counts + [sum(sev_counts)]
                table.append(row)
            return table

        potential_table = tabulate_type(potentials, "Potential")
        actual_table = tabulate_type(actuals, "Actual")
        invalid_row = ["Invalid", ""] + [''] * len(severities) + [len(invalid)]
        grand_totals = ["Grand Total", ""] + [incidents.filter(severity=s).count() for s in severities] + [
            incidents.exclude(severity=None).count() + invalid.count()]
        table = [header] + potential_table + actual_table + [invalid_row] + [grand_totals]

        context['summary_header'] = header
        context['actual_table'] = actual_table
        context['potential_table'] = potential_table
        context['invalid'] = invalid_row
        context['grand_totals'] = grand_totals

    def add_cause_summary(self, context, start, end):

        incidents = models.Incident.objects.valid().filter(
            incident_date__gte=start,
            incident_date__lt=end
        ).select_related("investigation__cause")
        incidents_list = list(incidents)

        max_depth = 1 + models.Cause.objects.aggregate(Max('level'))['level__max']

        table = []
        chart_data = []

        root_causes = models.Cause.objects.filter(parent=None)
        namesc = dict(models.Cause.objects.values_list("pk", "name"))

        # TODO fix caching errors here
        # sub_causes = cache.get("sub_causes")
        # if sub_causes is None:
        sub_causes = dict((r.pk, r.get_leafnodes()) for r in root_causes)
        # cache.set("sub_causes", sub_causes)

        # ancestors = cache.get("cause_ancestors")
        # if ancestors is None:
        ancestors = dict((x.pk, x.get_ancestors()) for x in models.Cause.objects.exclude(parent=None))
            # cache.set("cause_ancestors", ancestors)

        for root_cause in root_causes:
            if sub_causes[root_cause.pk]:
                for sub_cause in sub_causes[root_cause.pk]:
                    cause_count = len([x for x in incidents_list if x.investigation.cause == sub_cause])
                    if cause_count > 0:
                        names = [namesc[a.pk] for a in ancestors[sub_cause.pk]] + [sub_cause.name]
                        row = names + [""] * (max_depth - len(names)) + [cause_count]
                        chart_data.append([" &rarr; ".join(names), cause_count])
                        table.append(row)
            else:
                cause_count = len([x for x in incidents_list if x.investigation.cause == root_cause])
                if cause_count > 0:
                    names = [root_cause.name] + [""] * (max_depth - 1)
                    row = names + [cause_count]
                    chart_data.append([" &rarr; ".join(names), cause_count])
                    table.append(row)

        header = ["Cause"] + ['Sub-category'] * (max_depth - 1) + ["Count"]
        context["cause_header"] = header
        context['cause_table'] = table
        context['cause_counts'] = json.dumps(chart_data)

    def add_domain_summary(self, context, start, end):

        incidents = models.Incident.objects.valid().filter(
            incident_date__gte=start,
            incident_date__lt=end
        ).select_related("investigation__origin_domain", "investigation__detection_domain")

        list_incidents = list(incidents)
        max_depth = 1 + (models.Domain.objects.aggregate(Max('level'))['level__max'] or 0)

        table = []
        chart_data = {
            "Origin": [],
            "Detection": []
        }

        root_domains = models.Domain.objects.filter(parent=None)

        sub_domains = cache.get("sub_domains")
        if sub_domains is None or len(sub_domains) == 0:
            sub_domains = dict((r.pk, r.get_leafnodes()) for r in root_domains)
            cache.set("sub_domains", sub_domains)

        ancestors = cache.get("domain_ancestors")
        if ancestors is None or len(ancestors) == 0:
            ancestors = dict((x.pk, x.get_ancestors()) for x in models.Domain.objects.all())
            cache.set("domain_ancestors", ancestors)

        for root_domain in root_domains:
            for sub_domain in sub_domains[root_domain.pk]:
                origin_count = len([x for x in incidents if x.investigation.origin_domain == sub_domain])
                detection_count = len([x for x in incidents if x.investigation.detection_domain == sub_domain])

                names = [a.name for a in ancestors[sub_domain.pk]] + [sub_domain.name]
                row = names + [origin_count, detection_count]
                chart_data["Origin"].append([" &rarr; ".join(names), origin_count])
                chart_data["Detection"].append([" &rarr; ".join(names), detection_count])
                table.append(row)

        header = ["Domain"] + ['Sub-Domain'] * (max_depth - 1) + ["Origin Occurences", "Detection Occurences"]
        context["domain_header"] = header
        context['domain_table'] = table
        context['domain_counts'] = json.dumps(chart_data);

    def get_context_data(self):

        context = super(Statistics, self).get_context_data()
        try:
            year = int(self.request.GET.get("year", timezone.now().year))
            start = timezone.datetime(year=year, day=1, month=1)
            end = timezone.datetime(year=year + 1, day=1, month=1)
        except (ValueError):
            raise Http404("Invalid year parameter")

        # the +1 day here is because django-mssql has a bug where it returns Dec 31
        # instead of Jan 1st (probably due to timezone issue) so the +1day bumps
        # it into the correct year
        years = [(d + timezone.timedelta(days=1)).year for d in models.Incident.objects.dates("incident_date", "year")]

        context['years'] = years
        context['selected_year'] = year
        context['current_year'] = timezone.now().year

        self.add_summary_table(context, start, end)
        self.add_incident_counts(context, start, end)
        self.add_domain_summary(context, start, end)
        self.add_cause_summary(context, start, end)

        return context


class SelectIncident(View):
    """
    Responsible for returning a list of Incidents when searching for the duplicate.
    """

    def get(self, request,  *args, **kwargs):

        this_inc = request.GET.get('thisInc')
        q = request.GET.get("q")

        try:
            q = int(q, base=10)
            this_inc = int(this_inc, base=10)
            qs = models.Incident.objects.filter(pk__contains=q)
        except (ValueError, TypeError):
            qs = models.Incident.objects

        return_list = serializers.serialize("json", qs.exclude(pk=this_inc).exclude(duplicate_of=this_inc))

        return JsonResponse({"success": True, "items": return_list})


class IncidentFlag(UpdateView):

    model = models.Incident

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return JsonResponse({'flag': self.object.flag})

    def patch(self, request, *args, **kwargs):
        self.object = self.get_object()

        flag = QueryDict(request.body).get('flag', None)
        if flag is None:
            return JsonResponse({'success': False}, status=422)

        flag = True if flag == 'true' else False
        self.object.flag = flag
        self.object.save()
        return JsonResponse({'success': True}, status=200)
