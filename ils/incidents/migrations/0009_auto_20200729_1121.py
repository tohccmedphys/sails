# Generated by Django 3.0.8 on 2020-07-29 15:21

import colorfield.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('incidents', '0008_auto_20200729_1042'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actionescalation',
            name='color',
            field=colorfield.fields.ColorField(default='#FF0000', max_length=18),
        ),
        migrations.AlterField(
            model_name='actionpriority',
            name='color',
            field=colorfield.fields.ColorField(default='#FF0000', max_length=18),
        ),
        migrations.AlterField(
            model_name='actiontype',
            name='color',
            field=colorfield.fields.ColorField(default='#FF0000', max_length=18),
        ),
        migrations.AlterField(
            model_name='locationchoice',
            name='color',
            field=colorfield.fields.ColorField(default='#FF0000', max_length=18),
        ),
        migrations.AlterField(
            model_name='operationaltype',
            name='color',
            field=colorfield.fields.ColorField(default='#FF0000', max_length=18),
        ),
        migrations.AlterField(
            model_name='sharingaudience',
            name='color',
            field=colorfield.fields.ColorField(default='#FF0000', max_length=18),
        ),
        migrations.AlterField(
            model_name='standarddescription',
            name='color',
            field=colorfield.fields.ColorField(default='#FF0000', max_length=18),
        ),
        migrations.AlterField(
            model_name='technique',
            name='color',
            field=colorfield.fields.ColorField(default='#FF0000', max_length=18),
        ),
    ]
