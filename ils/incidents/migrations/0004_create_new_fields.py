# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import incidents.models


class Migration(migrations.Migration):

    dependencies = [
        ('incidents', '0003_create_theme'),
    ]

    operations = [
        migrations.AddField(
            model_name='incident',
            name='duplicate_of',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='incidents.Incident'),
        ),
        migrations.AddField(
            model_name='incident',
            name='duplicate',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='investigation',
            name='theme',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='incidents.Theme'),
        ),
    ]
