
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.manager
import incidents.models
import mptt.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ActionEscalation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(help_text='URL friendly version of name (only a-Z, 0-9 and _ allowed)', max_length=255, unique=True)),
                ('description', models.TextField(blank=True, help_text='Concise description of this incident type', null=True)),
                ('color', models.CharField(blank=True, max_length=7, null=True)),
                ('order', models.PositiveIntegerField(help_text='Order in which this Incident Type will be displayed')),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ActionPriority',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(help_text='URL friendly version of name (only a-Z, 0-9 and _ allowed)', max_length=255, unique=True)),
                ('description', models.TextField(blank=True, help_text='Concise description of this incident type', null=True)),
                ('color', models.CharField(blank=True, max_length=7, null=True)),
                ('order', models.PositiveIntegerField(help_text='Order in which this Incident Type will be displayed')),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ActionType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(help_text='URL friendly version of name (only a-Z, 0-9 and _ allowed)', max_length=255, unique=True)),
                ('description', models.TextField(blank=True, help_text='Concise description of this incident type', null=True)),
                ('color', models.CharField(blank=True, max_length=7, null=True)),
                ('order', models.PositiveIntegerField(help_text='Order in which this Incident Type will be displayed')),
                ('strength', models.CharField(blank=True, choices=[(b'weaker', 'Weaker'), (b'intermediate', 'Intermediate'), (b'stronger', 'Stronger')], max_length=15, null=True)),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
                'verbose_name': 'Action Type',
                'verbose_name_plural': 'Action Types',
            },
        ),
        migrations.CreateModel(
            name='Cause',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True)),
                ('description', models.TextField(blank=True, help_text='A short description of this cause type. (Optional)', null=True)),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, help_text='If this is a sub-classification choose the causes parent class', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='incidents.Cause')),
            ],
            options={
                'abstract': False,
            },
            bases=(incidents.models.MPTTModelMixin, models.Model),
            managers=[
                ('objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='Domain',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True, help_text='A short description of this cause type. (Optional)', null=True)),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, help_text='If this is a sub-classification choose the causes parent class', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='incidents.Domain')),
            ],
            options={
                'abstract': False,
            },
            bases=(incidents.models.MPTTModelMixin, models.Model),
            managers=[
                ('objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='LocationChoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(help_text='URL friendly version of name (only a-Z, 0-9 and _ allowed)', max_length=255, unique=True)),
                ('description', models.TextField(blank=True, help_text='Concise description of this incident type', null=True)),
                ('color', models.CharField(blank=True, max_length=7, null=True)),
                ('order', models.PositiveIntegerField(help_text='Order in which this Incident Type will be displayed')),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='OperationalType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(help_text='URL friendly version of name (only a-Z, 0-9 and _ allowed)', max_length=255, unique=True)),
                ('description', models.TextField(blank=True, help_text='Concise description of this incident type', null=True)),
                ('color', models.CharField(blank=True, max_length=7, null=True)),
                ('order', models.PositiveIntegerField(help_text='Order in which this Incident Type will be displayed')),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
                'verbose_name': 'Operational Incident Type',
                'verbose_name_plural': 'Operational Incident Types',
            },
        ),
        migrations.CreateModel(
            name='SeverityDefinition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('incident_type', models.CharField(choices=[(b'clinical', 'Clinical'), (b'occupational', 'Occupational'), (b'operational', 'Operational'), (b'environmental', 'Environmental'), (b'security', 'Security')], help_text='Incident Type', max_length=15)),
                ('severity', models.CharField(choices=[(b'critical', 'Critical'), (b'serious', 'Serious'), (b'major', 'Major'), (b'minor', 'Minor')], help_text='Incident Severity', max_length=15)),
                ('definition', models.TextField(help_text='A short description of this incident type/severity')),
            ],
            options={
                'ordering': ('incident_type', 'severity'),
            },
        ),
        migrations.CreateModel(
            name='StandardDescription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(help_text='URL friendly version of name (only a-Z, 0-9 and _ allowed)', max_length=255, unique=True)),
                ('description', models.TextField(blank=True, help_text='Concise description of this incident type', null=True)),
                ('color', models.CharField(blank=True, max_length=7, null=True)),
                ('order', models.PositiveIntegerField(help_text='Order in which this Incident Type will be displayed')),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Technique',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(help_text='URL friendly version of name (only a-Z, 0-9 and _ allowed)', max_length=255, unique=True)),
                ('description', models.TextField(blank=True, help_text='Concise description of this incident type', null=True)),
                ('color', models.CharField(blank=True, max_length=7, null=True)),
                ('order', models.PositiveIntegerField(help_text='Order in which this Incident Type will be displayed')),
                ('modality', models.CharField(choices=[(b'n/a', b'N/A'), (b'photons', b'Photons'), (b'electrons', b'Electrons'), (b'brachy', b'Brachy'), (b'ortho', b'Ortho')], max_length=15)),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Incident',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('incident_date', models.DateField(help_text='Date that the incident occured')),
                ('patient_id', models.CharField(blank=True, help_text='Patient ID (where applicable)', max_length=255, null=True)),
                ('incident_type', models.CharField(choices=[(b'clinical', 'Clinical'), (b'occupational', 'Occupational'), (b'operational', 'Operational'), (b'environmental', 'Environmental'), (b'security', 'Security')], help_text='Incident Type', max_length=15)),
                ('severity', models.CharField(blank=True, choices=[(b'critical', 'Critical'), (b'serious', 'Serious'), (b'major', 'Major'), (b'minor', 'Minor')], max_length=15, null=True)),
                ('intent', models.CharField(blank=True, choices=[(b'palliative', 'Palliative'), (b'curative', 'Curative'), (b'adjuvant', 'Adjuvant'), (b'neoadjuvant', 'Neoadjuvant'), (b'unknown', 'Unknown'), (b'n/a', 'N/A')], help_text='What was the intent of the treatment?', max_length=15, null=True)),
                ('num_affected', models.CharField(blank=True, choices=[(b'', b'---------'), (b'none', 'None'), (b'one', 'One'), (b'morethanone', 'More Than One')], help_text='How many patients were affected?', max_length=15, null=True, verbose_name='# Patients Affected')),
                ('description', models.TextField(help_text='Briefly summarize the incident')),
                ('submitted', models.DateTimeField(auto_now_add=True)),
                ('actual', models.CharField(choices=[(b'actual', 'Actual'), (b'potential', 'Potential')], help_text='Was this an actual incident or a potential/near miss incident', max_length=255)),
                ('valid', models.BooleanField(default=True, verbose_name=b'Trending')),
                ('flag', models.BooleanField(default=False, verbose_name='Flag for discussion')),
                ('location', models.ForeignKey(help_text='Location where the incident was discovered.', on_delete=django.db.models.deletion.CASCADE, to='incidents.LocationChoice')),
                ('standard_description', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='incidents.StandardDescription')),
                ('submitted_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('technique', models.ForeignKey(blank=True, help_text='What treatment technique was involved?', null=True, on_delete=django.db.models.deletion.CASCADE, to='incidents.Technique')),
            ],
            options={
                'ordering': ('-submitted',),
            },
        ),
        migrations.CreateModel(
            name='IncidentAction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField(verbose_name='Action Description')),
                ('assigned', models.DateTimeField(auto_now_add=True)),
                ('complete', models.BooleanField(default=False)),
                ('completed', models.DateTimeField(blank=True, null=True)),
                ('action_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='incidents.ActionType')),
                ('assigned_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='incidentactions_assigned', to=settings.AUTH_USER_MODEL)),
                ('escalation', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='incidents.ActionEscalation')),
                ('incident', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='incidents.Incident')),
                ('priority', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='incidents.ActionPriority')),
                ('responsible', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='incidentactions', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Actions',
            },
        ),
        migrations.CreateModel(
            name='Investigation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('assigned', models.DateTimeField(blank=True, null=True, verbose_name='Date Investigator Assigned')),
                ('harm', models.CharField(blank=True, choices=[(b'unknown', 'Unknown'), (b'none', 'None'), (b'mild', 'Mild'), (b'moderate', 'Moderate'), (b'severe', 'Severe'), (b'death', 'Death'), (b'n/a', 'N/A')], max_length=15, null=True, verbose_name='Degree of Harm')),
                ('psls_id', models.CharField(blank=True, help_text='Hospital ILS #', max_length=20, null=True)),
                ('complete', models.BooleanField(default=False)),
                ('completed', models.DateTimeField(blank=True, null=True)),
                ('assigned_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='incidents_assigned', to=settings.AUTH_USER_MODEL)),
                ('cause', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='incidents.Cause')),
                ('detection_domain', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='dection_incidents', to='incidents.Domain', verbose_name='Detection - Domain')),
                ('incident', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='incidents.Incident')),
                ('investigator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='investigations', to=settings.AUTH_USER_MODEL)),
                ('operational_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='incidents.OperationalType', verbose_name='Operational Incident Type')),
                ('origin_domain', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='origin_incidents', to='incidents.Domain', verbose_name='Origin - Domain')),
            ],
        ),
    ]
