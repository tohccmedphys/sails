
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import incidents.models


class Migration(migrations.Migration):

    dependencies = [
        ('incidents', '0002_create_incidentsharing'),
    ]

    operations = [
        migrations.CreateModel(
            name='Theme',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True)),
            ],
        ),
    ]
