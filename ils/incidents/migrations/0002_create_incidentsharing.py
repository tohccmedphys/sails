# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('incidents', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SharingAudience',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(help_text='URL friendly version of name (only a-Z, 0-9 and _ allowed)', max_length=255, unique=True)),
                ('description', models.TextField(blank=True, help_text='Concise description of this incident type', null=True)),
                ('color', models.CharField(blank=True, max_length=7, null=True)),
                ('order', models.PositiveIntegerField(help_text='Order in which this Incident Type will be displayed')),
                ('audience', models.CharField(blank=True, choices=[(b'external', 'External'), (b'program', 'Program'), (b'committee', 'Committee'), (b'staffmeeting', 'Staff Meeting')], max_length=15, null=True)),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
                'verbose_name': 'Sharing Audience',
                'verbose_name_plural': 'Sharing Audiences',
            },
        ),
        migrations.CreateModel(
            name='IncidentSharing',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('assigned', models.DateTimeField(blank=True, null=True)),
                ('done', models.BooleanField(default=False)),
                ('done_date', models.DateTimeField(blank=True, null=True)),
                ('assigned_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='incidentsharing_assigned', to=settings.AUTH_USER_MODEL)),
                ('incident', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='incidents.Incident')),
                ('responsible', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='incidentsharing', to=settings.AUTH_USER_MODEL)),
                ('sharing_audience', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='incidents.SharingAudience', verbose_name='Sharing Audience')),
            ],
            options={
                'verbose_name_plural': 'Sharing',
            }
        )
    ]
