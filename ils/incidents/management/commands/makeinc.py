from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

User = get_user_model()
from django.utils import timezone

import random
import datetime
import sys

try:
    from faker import Factory

    faker = Factory.create()
except ImportError:  # pragma: nocover
    faker = None


class Command(BaseCommand):
    help = 'command to generate the input number of fake incidents'

    def handle(self, *args, **kwargs):

        global models
        import incidents.models as models

        if faker is None:  # pragma: nocover
            sys.stdout.write("This command requires the fake-factory/faker library\n")
            return

        try:
            ninc = int(args[0])
        except (IndexError, ValueError):
            sys.stdout.write("This command expects an integer as an argument\n")
            return

        for i in range(ninc):
            inc = self.make_incident()
            sys.stdout.write("Made %d incidents %s\n" % (i + 1, inc))

    def make_incident(self):
        user, _ = User.objects.get_or_create(
            username=faker.user_name(),
            last_name=faker.last_name(),
            first_name=faker.first_name(),
        )
        investigator, _ = User.objects.get_or_create(
            username=faker.user_name(),
            last_name=faker.last_name(),
            first_name=faker.first_name(),
        )
        incident_date = faker.date_time_this_year()
        submitted_date = faker.date_time_between(incident_date, incident_date + datetime.timedelta(days=random.randint(0, 7)))
        i = models.Incident(
            incident_date=incident_date,
            submitted_by=user,
            patient_id='99999999' if random.random() < 0.5 else None,
            location=random.choice(models.LocationChoice.objects.all()),
            incident_type=random.choice(models.INCIDENT_TYPE_CHOICES)[0],
            description=faker.text(),
            valid=True if random.random() > 0.05 else False,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            actual=random.choice([models.ACTUAL, models.POTENTIAL]),
        )

        i.save()
        i.submitted = timezone.make_aware(submitted_date, timezone=timezone.get_current_timezone())
        i.investigation.investigator = investigator if random.random() > 0.1 else None
        if i.investigation.investigator:
            i.investigation.save()

        i.save()

        for a in range(random.randint(1, 3)):
            assigned = faker.date_time_between(submitted_date, submitted_date + datetime.timedelta(days=14))
            if random.random() > 0.5:
                complete = True,
                completed = faker.date_time_between(assigned, assigned + datetime.timedelta(days=365))
                completed = timezone.make_aware(completed, timezone=timezone.get_current_timezone())
            else:
                complete = False
                completed = None

            ia = models.IncidentAction(
                incident=i,
                action_type=random.choice(models.ActionType.objects.all()),
                description=faker.text(),
                responsible=random.choice(User.objects.all()),
                priority=random.choice(models.ActionPriority.objects.all()),
                assigned_by=investigator,
                complete=complete,
                completed=completed
            )
            ia.save()

        return i
