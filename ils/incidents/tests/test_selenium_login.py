
from django.test.testcases import LiveServerTestCase
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as e_c

from .utils import SailsTestCase


class TestLoginScreen(SailsTestCase):

    def setUp(self):
        self.createTwoAdmins()

    def tearDown(self):
        self.driver.quit()

    def test_admin_login_dropdown(self):

        d = self.driver
        w = self.wait
        self.load_main()

        d.find_element_by_id("sign-in-dropdown").click()
        w.until(self.find_visible_by_id('id_username_dropdown'), 'id_username_dropdown is not visible')

        d.find_element_by_id('id_username_dropdown').send_keys(self.admin1.username)
        d.find_element_by_id('id_password_dropdown').send_keys(self.password)
        d.find_element_by_id('sign-in-from-dropdown').click()
        w.until(self.find_visible_by_id('dashboard-link'), 'dashboard-link is not visible')
        d.find_element_by_id("sign-in-dropdown").click()
        w.until(self.find_visible_by_id('admin-link'), 'admin-link is not visible')

        self.assertTrue(d.find_element_by_id('logout-link'))

    def test_admin_login(self):

        d = self.driver
        w = self.wait
        self.load_main()
        d.get(self.live_server_url + '/login/')
        w.until(self.find_visible_by_id('id_username'), 'id_username is not visible')

        d.find_element_by_id('id_username').send_keys(self.admin1.username)
        d.find_element_by_id('id_password').send_keys(self.password)
        d.find_element_by_xpath('//button[text()="Log in"]').click()
        w.until(self.find_visible_by_id('dashboard-link'), 'dashboard-link is not visible')
        d.find_element_by_id("sign-in-dropdown").click()
        w.until(self.find_visible_by_id('admin-link'), 'admin-link is not visible')

        self.assertTrue(d.find_element_by_id('logout-link'))
