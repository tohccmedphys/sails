
import time

from django.utils import timezone

from .utils import SailsTestCase
from incidents import models


class TestLists(SailsTestCase):

    def setUp(self):
        self.createTwoInvestigations()
        self.createTwoActions()
        self.createTwoSharing()
        #
        # self.investigation1.investigator = self.admin1
        # self.investigation2.investigator = self.admin2
        # self.investigation1.save()
        # self.investigation2.save()

    def tearDown(self):
        self.driver.quit()

    def test_triage(self):

        d = self.driver
        w = self.wait
        self.load_main()

        d.find_element_by_id("sign-in-dropdown").click()
        w.until(self.find_visible_by_id('id_username_dropdown'), 'id_username_dropdown is not visible')
        d.find_element_by_id('id_username_dropdown').send_keys(self.admin1.username)
        d.find_element_by_id('id_password_dropdown').send_keys(self.password)
        d.find_element_by_id('sign-in-from-dropdown').click()

        d.find_element_by_id("incidents-lists").click()
        w.until(self.find_visible_by_id('triage-link'), 'triage is not visible')
        d.find_element_by_id('triage-link').click()

        w.until(self.find_visible_by_xpath('//div[@class="dataTables_processing"]'))
        w.until(self.element_hidden_xpath('//div[@class="dataTables_processing"]'))

        inc_table = d.find_element_by_id('listable-table-triage')
        self.assertTrue(inc_table.find_element_by_xpath('//a[text()="1"]'))
        self.assertTrue(inc_table.find_element_by_xpath('//a[text()="2"]'))

    def test_incomplete(self):

        d = self.driver
        w = self.wait
        self.load_main()

        self.investigation1.investigator = self.admin1
        self.investigation2.investigator = self.admin2
        self.investigation1.save()
        self.investigation2.save()

        d.find_element_by_id("sign-in-dropdown").click()
        w.until(self.find_visible_by_id('id_username_dropdown'), 'id_username_dropdown is not visible')
        d.find_element_by_id('id_username_dropdown').send_keys(self.admin1.username)
        d.find_element_by_id('id_password_dropdown').send_keys(self.password)
        d.find_element_by_id('sign-in-from-dropdown').click()

        d.find_element_by_id("incidents-lists").click()
        w.until(self.find_visible_by_id('incomplete-inc-link'), 'triage is not visible')
        d.find_element_by_id('incomplete-inc-link').click()

        w.until(self.find_visible_by_xpath('//div[@class="dataTables_processing"]'))
        w.until(self.element_hidden_xpath('//div[@class="dataTables_processing"]'))

        inc_table = d.find_element_by_id('listable-table-incomplete-incidents')
        self.assertTrue(inc_table.find_element_by_xpath('//a[text()="1"]'))
        self.assertTrue(inc_table.find_element_by_xpath('//a[text()="2"]'))

    def test_complete(self):

        d = self.driver
        w = self.wait
        self.load_main()

        self.investigation1.investigator = self.admin1
        self.investigation1.complete = True
        self.investigation1.completed = timezone.now()
        self.investigation1.save()

        d.find_element_by_id("sign-in-dropdown").click()
        w.until(self.find_visible_by_id('id_username_dropdown'), 'id_username_dropdown is not visible')
        d.find_element_by_id('id_username_dropdown').send_keys(self.admin1.username)
        d.find_element_by_id('id_password_dropdown').send_keys(self.password)
        d.find_element_by_id('sign-in-from-dropdown').click()

        d.find_element_by_id("incidents-lists").click()
        w.until(self.find_visible_by_id('complete-inc-link'), 'triage is not visible')
        d.find_element_by_id('complete-inc-link').click()

        w.until(self.find_visible_by_xpath('//div[@class="dataTables_processing"]'))
        w.until(self.element_hidden_xpath('//div[@class="dataTables_processing"]'))

        inc_table = d.find_element_by_id('listable-table-complete-incidents')
        self.assertTrue(inc_table.find_element_by_xpath('//a[text()="1"]'))
        self.assertNotHere_xpath('//a[text()="2"]')

    def test_actual(self):

        d = self.driver
        w = self.wait
        self.load_main()

        d.find_element_by_id("sign-in-dropdown").click()
        w.until(self.find_visible_by_id('id_username_dropdown'), 'id_username_dropdown is not visible')
        d.find_element_by_id('id_username_dropdown').send_keys(self.admin1.username)
        d.find_element_by_id('id_password_dropdown').send_keys(self.password)
        d.find_element_by_id('sign-in-from-dropdown').click()

        d.find_element_by_id("incidents-lists").click()
        w.until(self.find_visible_by_id('actual-inc-link'), 'actual is not visible')
        d.find_element_by_id('actual-inc-link').click()

        w.until(self.find_visible_by_xpath('//div[@class="dataTables_processing"]'))
        w.until(self.element_hidden_xpath('//div[@class="dataTables_processing"]'))

        inc_table = d.find_element_by_id('listable-table-actual-incidents')
        self.assertTrue(inc_table.find_element_by_xpath('//a[text()="2"]'))
        self.assertNotHere_xpath('//a[text()="1"]')

    def test_invalid(self):

        d = self.driver
        w = self.wait
        self.load_main()

        self.incident1.valid = False
        self.incident1.save()

        d.find_element_by_id("sign-in-dropdown").click()
        w.until(self.find_visible_by_id('id_username_dropdown'), 'id_username_dropdown is not visible')
        d.find_element_by_id('id_username_dropdown').send_keys(self.admin1.username)
        d.find_element_by_id('id_password_dropdown').send_keys(self.password)
        d.find_element_by_id('sign-in-from-dropdown').click()

        d.find_element_by_id("incidents-lists").click()
        w.until(self.find_visible_by_id('invalid-inc-link'), 'invalid is not visible')
        d.find_element_by_id('invalid-inc-link').click()

        w.until(self.find_visible_by_xpath('//div[@class="dataTables_processing"]'))
        w.until(self.element_hidden_xpath('//div[@class="dataTables_processing"]'))

        inc_table = d.find_element_by_id('listable-table-invalid-incidents')
        self.assertTrue(inc_table.find_element_by_xpath('//a[text()="1"]'))
        self.assertNotHere_xpath('//a[text()="2"]')

    def test_all(self):

        d = self.driver
        w = self.wait
        self.load_main()

        self.incident1.valid = False
        self.incident1.save()

        d.find_element_by_id("sign-in-dropdown").click()
        w.until(self.find_visible_by_id('id_username_dropdown'), 'id_username_dropdown is not visible')
        d.find_element_by_id('id_username_dropdown').send_keys(self.admin1.username)
        d.find_element_by_id('id_password_dropdown').send_keys(self.password)
        d.find_element_by_id('sign-in-from-dropdown').click()

        d.find_element_by_id("incidents-lists").click()
        w.until(self.find_visible_by_id('all-inc-link'), 'invalid is not visible')
        d.find_element_by_id('all-inc-link').click()

        w.until(self.find_visible_by_xpath('//div[@class="dataTables_processing"]'))
        w.until(self.element_hidden_xpath('//div[@class="dataTables_processing"]'))

        inc_table = d.find_element_by_id('listable-table-all-incidents')
        self.assertTrue(inc_table.find_element_by_xpath('//a[text()="1"]'))
        self.assertTrue(inc_table.find_element_by_xpath('//a[text()="2"]'))
