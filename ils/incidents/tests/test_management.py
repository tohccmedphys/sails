from django.test import TestCase
from .. import models
import incidents.management.commands.makeinc as makeinc

from django.contrib.auth import get_user_model
User = get_user_model()

from faker import Faker

faker = Faker()


class TestReportForm(TestCase):
    fixtures = [
        'domains.json',
        #'severities.json',
        'techniques.json',
        'actionescalation.json',
        'actionpriority.json',
        'actiontypes.json',
        'causes.json',
        'locations.json',
        'standarddescriptions.json',
    ]

    def test_incidents_created(self):
        makeinc.Command().handle(2)
        self.assertEqual(len(models.Incident.objects.all()), 2)

    def test_no_arg(self):
        makeinc.Command().handle()
        self.assertTrue(len(models.Incident.objects.all()) == 0)

