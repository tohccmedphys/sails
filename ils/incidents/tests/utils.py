
import random

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.test import TestCase
from django.test.testcases import LiveServerTestCase
from django.utils import timezone
from faker import Faker
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as e_c
from selenium.webdriver.support.wait import WebDriverWait, NoSuchElementException

from incidents import models

faker = Faker()
User = get_user_model()

# DRIVER = settings.SELENIUM_DRIVER(settings.SELENIUM_DRIVER_PATH, service_args=['--proxy-type=none'])
# DRIVER.set_window_size(1250, 1080)
# WAIT = WebDriverWait(DRIVER, 5)


class SailsTestCase(TestCase, LiveServerTestCase):

    fixtures = [
        'domains.json',
        'severitydefinitions.json',
        'techniques.json',
        'actionescalation.json',
        'actionpriority.json',
        'actiontypes.json',
        'causes.json',
        'locations.json',
        'standarddescriptions.json',
        'operationaltype.json',
        'sharingaudience.json'
    ]

    def __init__(self, *args, **kwargs):
        super(SailsTestCase, self).__init__(*args, **kwargs)
        self.driver = settings.SELENIUM_DRIVER(settings.SELENIUM_DRIVER_PATH, service_args=['--proxy-type=none'])
        self.driver.set_window_size(1270, 1100)
        self.wait = WebDriverWait(self.driver, 5)

    def load_main(self):
        self.driver.get(self.live_server_url)
        self.wait.until(e_c.presence_of_element_located((By.ID, "page-title")))

    def find_visible_by_id(self, e_id):

        d = self.driver

        def find_it(webdriver):
            e = d.find_element_by_id(e_id)
            if not e.is_displayed():
                return False
            return e

        return find_it

    def find_visible_by_xpath(self, e_xpath):

        d = self.driver

        def find_it(webdriver):
            e = d.find_element_by_xpath(e_xpath)
            if not e.is_displayed():
                return False
            return e

        return find_it

    def element_hidden_xpath(self, e_xpath):

        d = self.driver

        def is_hidden(webdriver):
            e = d.find_element_by_xpath(e_xpath)
            if e.is_displayed():
                return False
            return True

        return is_hidden

    def element_hidden_id(self, e_id):

        d = self.driver

        def is_hidden(webdriver):
            e = d.find_element_by_id(e_id)
            if e.is_displayed():
                return False
            return True

        return is_hidden

    def assertNotHere_xpath(self, e_xpath):

        d = self.driver

        try:
            e = d.find_element_by_xpath(e_xpath)
            return e
        except NoSuchElementException:
            return True

    def createTwoIncidents(self):
        """
        Creates two incidents. Checks if two admins have been created.
        """
        if not hasattr(self, 'admin1') or not hasattr(self, 'admin2'):
            self.createTwoAdmins()

        self.incident1 = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
            valid=True,
        )

        self.incident2 = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.OPERATIONAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin2,
            actual=models.ACTUAL,
            valid=True,
        )

        self.incident1.save()
        self.incident2.save()

    def createTwoAdmins(self):
        """
        Creates two admins for testing.
        """
        self.password = "pwd"

        self.admin1 = User.objects.create_superuser('admin1', 'myemail1@test.com', self.password)
        self.admin2 = User.objects.create_superuser('admin2', 'myemail2@test.com', self.password)

        perms = [
            'can_investigate', 'can_assign', 'can_be_responsible_actions', 'can_assign_responsible_actions',
            'can_be_responsible_sharing', 'can_assign_responsible_sharing'
        ]

        permission_list = Permission.objects.all().filter(codename__in=perms)

        self.admin1.user_permissions.set(permission_list)
        self.admin2.user_permissions.set(permission_list)

        self.admin1.save()
        self.admin2.save()

    def createTwoActions(self):

        if not hasattr(self, 'admin1') or not hasattr(self, 'admin2'):
            self.createTwoAdmins()
        if not hasattr(self, 'incident1') or not hasattr(self, 'incident2'):
            self.createTwoIncidents()

        self.action1 = models.IncidentAction(
            incident=self.incident1,
            action_type=random.choice(models.ActionType.objects.all()),
            description=faker.text(),
            responsible=self.admin1,
            priority=random.choice(models.ActionPriority.objects.all()),
            escalation=random.choice(models.ActionEscalation.objects.all()),
            assigned=timezone.now(),
            assigned_by=self.admin1,
            complete=False,
            completed=None
        )

        self.action2 = models.IncidentAction(
            incident=self.incident2,
            action_type=random.choice(models.ActionType.objects.all()),
            description=faker.text(),
            responsible=self.admin2,
            priority=random.choice(models.ActionPriority.objects.all()),
            escalation=random.choice(models.ActionEscalation.objects.all()),
            assigned=timezone.now(),
            assigned_by=self.admin2,
            complete=False,
            completed=None
        )

        self.action1.save()
        self.action2.save()

    def createTwoInvestigations(self):

        if not hasattr(self, 'admin1') or not hasattr(self, 'admin2'):
            self.createTwoAdmins()
        if not hasattr(self, 'incident1') or not hasattr(self, 'incident2'):
            self.createTwoIncidents()

        self.investigation1 = models.Investigation.objects.get(pk=1)
        self.investigation2 = models.Investigation.objects.get(pk=2)

    def createTwoSharing(self):

        if not hasattr(self, 'admin1') or not hasattr(self, 'admin2'):
            self.createTwoAdmins()
        if not hasattr(self, 'incident1') or not hasattr(self, 'incident2'):
            self.createTwoIncidents()

        self.sharing1 = models.IncidentSharing(
            incident=self.incident1,
            sharing_audience=random.choice(models.SharingAudience.objects.all()),
            responsible=self.admin1,
            assigned=timezone.now(),
            assigned_by=self.admin1
        ).save()

        self.sharing2 = models.IncidentSharing(
            incident=self.incident2,
            sharing_audience=random.choice(models.SharingAudience.objects.all()),
            responsible=self.admin2,
            assigned=timezone.now(),
            assigned_by=self.admin2
        ).save()
