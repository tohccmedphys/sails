
from selenium.webdriver.common.action_chains import ActionChains

from .utils import SailsTestCase


class TestStats(SailsTestCase):

    def setUp(self):
        self.createTwoInvestigations()
        self.createTwoActions()
        self.createTwoSharing()

        self.investigation1.investigator = self.admin1
        self.investigation2.investigator = self.admin2
        self.investigation1.save()
        self.investigation2.save()

    def tearDown(self):
        self.driver.quit()

    def test_stats(self):
        d = self.driver
        w = self.wait
        self.load_main()

        d.find_element_by_id("sign-in-dropdown").click()
        w.until(self.find_visible_by_id('id_username_dropdown'), 'id_username_dropdown is not visible')
        d.find_element_by_id('id_username_dropdown').send_keys(self.admin1.username)
        d.find_element_by_id('id_password_dropdown').send_keys(self.password)
        d.find_element_by_id('sign-in-from-dropdown').click()

        hover = ActionChains(d).move_to_element(d.find_element_by_id("stats-drop"))
        hover.perform()
        w.until(self.find_visible_by_id('stats-link'), 'stats is not visible')
        d.find_element_by_id('stats-link').click()
        w.until(self.find_visible_by_id('summary-wrapper'))





