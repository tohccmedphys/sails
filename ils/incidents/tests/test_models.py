
import random

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone
from faker import Faker

from incidents import models

faker = Faker()
User = get_user_model()


class TestMPTT(TestCase):

    def setUp(self):

        self.c1 = models.Cause(name="c1")
        self.c1.save()
        self.c2 = models.Cause(name="c2", parent=self.c1)
        self.c2.save()
        self.c3 = models.Cause(name="c3", parent=self.c2)
        self.c3.save()

    def test_ancestor_names(self):
        self.assertEqual(u'&rarr;'.join([self.c1.name, self.c2.name, self.c3.name]), self.c3.ancestor_names())

    def test_ancestor_names_plain(self):
        self.assertEqual(u' -> '.join([self.c1.name, self.c2.name, self.c3.name]), self.c3.ancestor_names_plain())

    def test_raw_save(self):
        self.c3.lft = 99
        self.c3.raw_save()
        self.assertEqual(self.c3.lft, 99)


class TestIncidentManagers(TestCase):

    fixtures = [
        'domains.json',
        'severitydefinitions.json',
        'techniques.json',
        'actionescalation.json',
        'actionpriority.json',
        'actiontypes.json',
        'causes.json',
        'locations.json',
        'standarddescriptions.json',
        'operationaltype.json',
        'sharingaudience.json'
    ]

    def setUp(self):

        self.password = "pwd"

        self.admin1 = User.objects.create_superuser('admin1', 'myemail1@test.com', self.password)
        self.admin2 = User.objects.create_superuser('admin2', 'myemail2@test.com', self.password)

        self.valid = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            intent=models.INTENT_CHOICES[0][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[0][0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.ACTUAL,
        )

        self.valid.save()

        self.invalid = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            intent=models.INTENT_CHOICES[0][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[0][0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
            valid=False,
        )

        self.invalid.save()

    def test_valid(self):
        self.assertListEqual(list(models.Incident.objects.valid()), [self.valid])

    def test_move_from_triage_to_incomplete(self):
        self.assertListEqual(list(models.Incident.triage.all()), [self.valid])
        self.assertListEqual(list(models.Incident.incomplete.all()), [])
        self.valid.investigation.investigator = self.admin1
        self.valid.investigation.save()
        self.assertListEqual(list(models.Incident.triage.all()), [])
        self.assertListEqual(list(models.Incident.incomplete.all()), [self.valid])

    def test_move_incomplete_to_triage(self):
        self.valid.investigation.investigator = self.admin1
        self.valid.investigation.save()
        self.assertListEqual(list(models.Incident.triage.all()), [])

        self.valid.investigation.investigator = None
        self.valid.investigation.save()
        self.assertListEqual(list(models.Incident.triage.all()), [self.valid])

    def test_complete(self):
        self.valid.investigation.complete = True
        self.valid.investigation.save()
        self.assertListEqual(list(models.Incident.complete.all().order_by('pk')), [self.valid, self.invalid])

    def test_actual(self):
        self.valid.valid = True
        self.valid.save()
        self.assertListEqual(list(models.Incident.actuals.all()), [self.valid])

    def test_flagged(self):
        self.valid.flag = True
        self.valid.investigation.investigator = self.admin1
        self.valid.save()
        self.valid.investigation.save()
        self.assertListEqual(list(models.Incident.incomplete_or_flagged.all()), [self.valid])
        self.invalid.investigation.complete = False
        self.invalid.investigation.investigator = self.admin2
        self.invalid.investigation.save()
        self.invalid.valid = True
        self.invalid.save()
        self.assertListEqual(list(models.Incident.incomplete_or_flagged.all().order_by('pk')), [self.valid, self.invalid])


class TestIncident(TestCase):

    fixtures = [
        'domains.json',
        'severitydefinitions.json',
        'techniques.json',
        'actionescalation.json',
        'actionpriority.json',
        'actiontypes.json',
        'causes.json',
        'locations.json',
        'standarddescriptions.json',
        'operationaltype.json',
        'sharingaudience.json'
    ]

    def setUp(self):

        self.password = "pwd"

        self.admin1 = User.objects.create_superuser('admin1', 'myemail1@test.com', self.password)
        self.admin2 = User.objects.create_superuser('admin2', 'myemail2@test.com', self.password)

        self.valid = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.OPERATIONAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            intent=models.INTENT_CHOICES[-1][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[-1][0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
        )

        self.valid.save()

        self.invalid = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            intent=models.INTENT_CHOICES[-1][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[-1][0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
            valid=False,
        )

        self.invalid.save()

        self.clinical = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
            valid=True,
        )

        self.clinical.save()

    def test_complete_fields_set(self):
        self.assertTrue(self.valid.complete_fields_set())

    def test_complete_fields_missing_fk(self):
        self.valid.intent = None
        self.assertFalse(self.valid.complete_fields_set())

    def test_is_potential(self):
        self.assertTrue(self.valid.is_potential())
        self.assertFalse(self.valid.is_actual())

    def test_is_actual(self):
        self.valid.actual = models.ACTUAL
        self.assertTrue(self.valid.is_actual())
        self.assertFalse(self.valid.is_potential())

    def test_actual_display(self):
        self.assertEqual(self.valid.actual_display(), dict(models.ACTUAL_POTENTIAL_CHOICES)[models.POTENTIAL])

    def test_url(self):
        self.assertEqual(self.valid.get_absolute_url(), "/incidents/update/%d/" % self.valid.pk)

    def test_missing_fields_clinical(self):
        self.assertListEqual(self.clinical.missing_fields(), ['intent', 'num_affected', 'technique'])

    def test_needs_std_des(self):
        self.assertFalse(self.clinical.needs_standard_description())
        self.clinical.actual = models.ACTUAL
        self.clinical.save()
        self.assertTrue(self.clinical.needs_standard_description())
        self.valid.actual = models.ACTUAL
        self.valid.save()
        self.assertFalse(self.valid.needs_standard_description())


# TODO: add test_actions
#   - test incident.get_actions
#   - test action model
class TestAction(TestCase):

    fixtures = [
        'domains.json',
        'severitydefinitions.json',
        'techniques.json',
        'actionescalation.json',
        'actionpriority.json',
        'actiontypes.json',
        'causes.json',
        'locations.json',
        'standarddescriptions.json',
        'operationaltype.json',
        'sharingaudience.json'
    ]

    def setUp(self):
        self.password = "pwd"

        self.admin1 = User.objects.create_superuser('admin1', 'myemail1@test.com', self.password)
        self.admin2 = User.objects.create_superuser('admin2', 'myemail2@test.com', self.password)

        self.incident1 = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
            valid=True,
        )

        self.incident2 = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.OPERATIONAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin2,
            actual=models.ACTUAL,
            valid=True,
        )

        self.incident1.save()
        self.incident2.save()

        self.action1 = models.IncidentAction(
            incident=self.incident1,
            action_type=models.ActionType.objects.get(pk=1),
            description=faker.text(),
            responsible=self.admin1,
            assigned=timezone.now(),
            assigned_by=self.admin1
        )

        self.action2 = models.IncidentAction(
            incident=self.incident2,
            action_type=models.ActionType.objects.get(pk=2),
            description=faker.text(),
            responsible=self.admin2,
            assigned=timezone.now(),
            assigned_by=self.admin2
        )

        self.action1.save()
        self.action2.save()

    def test_get_actions(self):
        i1a1 = self.incident1.get_actions().order_by('pk')
        self.assertListEqual(list(i1a1), [self.action1])

    def test_display(self):
        a = models.ActionType()
        self.assertEqual("", a.strength_display())
        a.strength = models.WEAKER
        self.assertEqual("Weaker", a.strength_display())

    def test_repr(self):
        a = models.ActionType(name="AType", strength=models.WEAKER)
        self.assertEqual("AType (Weaker)", a.__str__())


class TestSharing(TestCase):

    fixtures = [
        'domains.json',
        'severitydefinitions.json',
        'techniques.json',
        'actionescalation.json',
        'actionpriority.json',
        'actiontypes.json',
        'causes.json',
        'locations.json',
        'standarddescriptions.json',
        'operationaltype.json',
        'sharingaudience.json'
    ]

    def setUp(self):
        self.password = "pwd"

        self.admin1 = User.objects.create_superuser('admin1', 'myemail1@test.com', self.password)
        self.admin2 = User.objects.create_superuser('admin2', 'myemail2@test.com', self.password)

        self.incident1 = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
            valid=True,
        )

        self.incident2 = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.OPERATIONAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin2,
            actual=models.ACTUAL,
            valid=True,
        )

        self.incident1.save()
        self.incident2.save()

        self.share1 = models.IncidentSharing(
            incident=self.incident1,
            sharing_audience=models.SharingAudience.objects.get(pk=1),
            responsible=self.admin1,
            assigned=timezone.now(),
            assigned_by=self.admin1
        )

        self.share2 = models.IncidentSharing(
            incident=self.incident2,
            sharing_audience=models.SharingAudience.objects.get(pk=2),
            responsible=self.admin2,
            assigned=timezone.now(),
            assigned_by=self.admin2
        )

        self.share1.save()
        self.share2.save()

    def test_get_sharing(self):
        i1s1 = self.incident1.get_sharing().order_by('pk')
        self.assertListEqual(list(i1s1), [self.share1])


class TestInvestigation(TestCase):

    fixtures = [
        'domains.json',
        'severitydefinitions.json',
        'techniques.json',
        'actionescalation.json',
        'actionpriority.json',
        'actiontypes.json',
        'causes.json',
        'locations.json',
        'standarddescriptions.json',
        'operationaltype.json',
        'sharingaudience.json'
    ]

    def setUp(self):
        self.password = "pwd"

        self.admin1 = User.objects.create_superuser('admin1', 'myemail1@test.com', self.password)
        self.admin2 = User.objects.create_superuser('admin2', 'myemail2@test.com', self.password)

        self.incident1 = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            intent=models.INTENT_CHOICES[-1][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[-1][0],
            actual=models.POTENTIAL,
            valid=True,
        )

        self.incident2 = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.OPERATIONAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            intent=models.INTENT_CHOICES[-1][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[-1][0],
            submitted_by=self.admin2,
            actual=models.ACTUAL,
            valid=False,
        )

        self.incident1.save()
        self.incident2.save()

        i = self.incident1.investigation
        i.investigator = self.admin1
        i.cause = random.choice(models.Cause.objects.all())
        i.detection_domain = random.choice(models.Domain.objects.all())
        i.origin_domain = random.choice(models.Domain.objects.all())
        i.harm = random.choice(models.DOH_CHOICES)[0]
        i.save()

    def test_manager_incomplete(self):
        self.assertListEqual([self.incident1.investigation], list(models.Investigation.objects.incomplete()))

    def test_manager_user_all(self):
        self.incident1.investigation.investigator=self.admin1
        self.incident1.investigation.save()
        self.incident2.investigation.investigator=self.admin1
        self.incident2.investigation.save()
        self.assertListEqual([self.incident1.investigation, self.incident2.investigation], list(models.Investigation.objects.user_all(self.admin1)))

    def test_manager_user_incomplete(self):
        self.incident1.investigation.investigator=self.admin1
        self.incident1.investigation.save()
        self.incident2.investigation.investigator=self.admin1
        self.incident2.investigation.save()
        self.assertListEqual([self.incident1.investigation], list(models.Investigation.objects.user_incomplete(self.admin1)))

    def test_complete_fields_set(self):
        self.assertTrue(self.incident1.investigation.complete_fields_set())

    def test_missing_fields_actual(self):
        self.incident1.actual = models.ACTUAL
        self.incident1.save()
        self.assertListEqual(self.incident1.investigation.missing_fields(), ["psls_id", "incident__standard_description"])

    def test_missing_fields_both(self):
        self.incident1.num_affected = None
        self.incident1.investigation.harm = None
        self.assertListEqual(self.incident1.investigation.missing_fields(), ["harm", "incident__num_affected"])

    def test_invalid_required_complete(self):
        self.assertTrue(self.incident2.investigation.required_complete())

    def test_required_complete_potential(self):
        self.assertTrue(self.incident1.investigation.required_complete())

    def test_required_not_complete_actual(self):
        self.incident1.actual = models.ACTUAL
        self.assertFalse(self.incident1.investigation.required_complete())

    def test_required_complete_actual(self):
        self.incident1.actual = models.ACTUAL
        self.incident1.investigation.psls_id = "123"
        self.incident1.standard_description = random.choice(models.StandardDescription.objects.all())
        self.assertTrue(self.incident1.investigation.required_complete())

