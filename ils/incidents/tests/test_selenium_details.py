
import time

from .utils import SailsTestCase
from incidents import models

class TestDetails(SailsTestCase):

    def setUp(self):
        self.createTwoIncidents()

    def tearDown(self):
        self.driver.quit()

    def test_field_displays(self):
        d = self.driver
        w = self.wait
        self.load_main()

        self.incident1.incident_type = models.SECURITY
        self.incident1.save()

        d.find_element_by_id("sign-in-dropdown").click()
        w.until(self.find_visible_by_id('id_username_dropdown'), 'id_username_dropdown is not visible')
        d.find_element_by_id('id_username_dropdown').send_keys(self.admin1.username)
        d.find_element_by_id('id_password_dropdown').send_keys(self.password)
        d.find_element_by_id('sign-in-from-dropdown').click()
        w.until(self.find_visible_by_id('incidents-lists'))

        d.get(self.live_server_url + '/incidents/1/?continue=1&basic=1')

        w.until(self.find_visible_by_id('investigation-form'))
        self.assertFalse(d.find_element_by_id('hide-id_operational_type').is_displayed())
        self.assertFalse(d.find_element_by_id('hide-id_detection_domain').is_displayed())
        self.assertFalse(d.find_element_by_id('hide-id_origin_domain').is_displayed())
        self.assertFalse(d.find_element_by_id('hide-id_psls_id').is_displayed())
        self.assertFalse(d.find_element_by_id('hide-id_standard_description').is_displayed())

        w.until(self.find_visible_by_id('select2-id_incident_type-container'))
        d.find_element_by_id('select2-id_incident_type-container').click()
        w.until(self.find_visible_by_id('select2-id_incident_type-results'))
        d.find_element_by_xpath('//li[@class="select2-results__option"][text()="Operational"]').click()

        w.until(self.find_visible_by_id('hide-id_operational_type'))

        d.find_element_by_id('select2-id_incident_type-container').click()
        w.until(self.find_visible_by_id('select2-id_incident_type-results'))
        d.find_element_by_xpath('//li[@class="select2-results__option"][text()="Clinical"]').click()

        w.until(self.find_visible_by_id('hide-id_detection_domain'))
        w.until(self.find_visible_by_id('hide-id_origin_domain'))
        w.until(self.element_hidden_id('hide-id_operational_type'))

        d.find_element_by_id('select2-id_actual-container').click()
        w.until(self.find_visible_by_id('select2-id_actual-results'))
        d.find_element_by_xpath('//li[@class="select2-results__option"][text()="Actual"]').click()

        w.until(self.find_visible_by_id('hide-id_psls_id'))
        w.until(self.find_visible_by_id('hide-id_standard_description'))

        d.find_element_by_id('select2-id_incident_type-container').click()
        w.until(self.find_visible_by_id('select2-id_incident_type-results'))
        d.find_element_by_xpath('//li[@class="select2-results__option"][text()="Environmental"]').click()

        w.until(self.element_hidden_id('hide-id_operational_type'))
        w.until(self.element_hidden_id('hide-id_detection_domain'))
        w.until(self.element_hidden_id('hide-id_origin_domain'))
        w.until(self.element_hidden_id('hide-id_psls_id'))
        w.until(self.element_hidden_id('hide-id_standard_description'))


