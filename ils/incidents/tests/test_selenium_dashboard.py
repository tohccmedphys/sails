
from .utils import SailsTestCase


class TestDashboard(SailsTestCase):

    def setUp(self):
        self.createTwoInvestigations()
        self.createTwoActions()
        self.createTwoSharing()

        self.investigation1.investigator = self.admin1
        self.investigation2.investigator = self.admin2
        self.investigation1.save()
        self.investigation2.save()

    def tearDown(self):
        self.driver.quit()

    def test_dashboard(self):
        d = self.driver
        w = self.wait
        self.load_main()
        # w.until(self.find_visible_by_id('sign-in-dropdown'))

        d.find_element_by_id("sign-in-dropdown").click()
        w.until(self.find_visible_by_id('id_username_dropdown'), 'id_username_dropdown is not visible')
        d.find_element_by_id('id_username_dropdown').send_keys(self.admin1.username)
        d.find_element_by_id('id_password_dropdown').send_keys(self.password)
        d.find_element_by_id('sign-in-from-dropdown').click()

        d.find_element_by_id('dashboard-link').click()
        w.until(self.find_visible_by_xpath('//body'))

        inv_table = d.find_element_by_id('incident-table')
        self.assertTrue(inv_table.find_element_by_xpath('//a[text()="#1"]'))

        act_table = d.find_element_by_id('action-table')
        self.assertTrue(act_table.find_element_by_xpath('//a[text()="#1"]'))

        sha_table = d.find_element_by_id('sharing-table')
        self.assertTrue(sha_table.find_element_by_xpath('//a[text()="#1"]'))

