
import time

from django.test.testcases import LiveServerTestCase
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as e_c

from .utils import SailsTestCase


class TestLoginScreen(SailsTestCase):

    def setUp(self):
        self.createTwoAdmins()

    def tearDown(self):
        self.driver.quit()

    def test_successful(self):
        self.load_main()
        d = self.driver
        self.wait.until(e_c.presence_of_element_located((By.ID, "select2-id_actual-container")))
        d.find_element_by_id('id_patient_id').send_keys('12345678')
        d.find_element_by_id('select2-id_actual-container').click()
        d.find_element_by_xpath('//li[@class="select2-results__option"][text()="Potential"]').click()
        d.find_element_by_id('select2-id_incident_type-container').click()
        d.find_element_by_xpath('//li[@class="select2-results__option"][text()="Security"]').click()
        d.find_element_by_id('select2-id_severity-container').click()
        d.find_element_by_xpath('//li[@class="select2-results__option"][text()="Major"]').click()
        d.find_element_by_id('select2-id_location-container').click()
        d.find_elements_by_xpath('//li[@class="select2-results__option"]')[1].click()
        d.find_element_by_id('id_description').send_keys('King Gizzard and the Lizard Wizard')
        d.find_element_by_id('id_submitted_by').send_keys(self.admin1.username)
        d.find_element_by_id('id_auth_password').send_keys(self.password)
        d.find_element_by_xpath('//button[text()="Submit"]').click()

        self.wait.until(self.find_visible_by_xpath('//div[@class="alert sized-div  alert-success"]'))

    def test_actual_clinical_alert(self):
        self.load_main()
        d = self.driver
        self.wait.until(e_c.presence_of_element_located((By.ID, "select2-id_actual-container")))
        d.find_element_by_id('select2-id_actual-container').click()
        d.find_element_by_xpath('//li[@class="select2-results__option"][text()="Actual"]').click()
        d.find_element_by_id('select2-id_incident_type-container').click()
        d.find_element_by_xpath('//li[@class="select2-results__option"][text()="Clinical"]').click()

        self.wait.until(self.find_visible_by_xpath('//div[@class="sized-div alert alert-info clinical-actual"]'))

    def test_severity_popover(self):
        self.load_main()
        d = self.driver
        self.wait.until(e_c.presence_of_element_located((By.ID, "select2-id_actual-container")))
        d.find_element_by_id('select2-id_severity-container').click()
        self.wait.until(self.find_visible_by_xpath('//div[@class="popover-content"][text()="Severity definitions are dependent on Incident type. Please select incident type before selecting Severity"]'))

        d.find_element_by_id('select2-id_incident_type-container').click()
        d.find_element_by_xpath('//li[@class="select2-results__option"][text()="Clinical"]').click()

        self.wait.until(self.find_visible_by_xpath('//div[@class="popover-content"]'))
        self.assertNotEqual(str(d.find_element_by_xpath('//div[@class="popover-content"]').text), 'Severity definitions are dependent on Incident type. Please select incident type before selecting Severity')
