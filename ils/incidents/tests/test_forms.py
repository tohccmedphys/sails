import django.forms
from django.test import TestCase
from .. import models, forms

from django.contrib.auth.models import AnonymousUser
from django.contrib.auth import get_user_model

import django.utils.timezone as timezone
from faker import Faker
import random

faker = Faker()
User = get_user_model()


class TestReportForm(TestCase):

    def setUp(self):

        self.password = 'password'
        self.admin = User.objects.create_superuser('admin', 'myemail@test.com', self.password)

    def test_login_remove(self):

        f = forms.IncidentReportForm(data={}, user=self.admin)
        self.assertTrue("auth_password" in f.fields)
        self.assertTrue("submitted_by" in f.fields)

    def test_login_present_for_anon(self):

        f = forms.IncidentReportForm(data={}, user=AnonymousUser())
        self.assertTrue("auth_password" in f.fields)
        self.assertTrue("submitted_by" in f.fields)

    def test_login_for_anon(self):
        data = {"auth_password":self.password, "submitted_by":self.admin.username}

        f = forms.IncidentReportForm(data=data, user=AnonymousUser())
        f.is_valid()
        f.clean()
        self.assertEqual(f.user, self.admin)

    def test_login_failed(self):
        f = forms.IncidentReportForm(data={}, user=AnonymousUser())
        f.is_valid()
        self.assertTrue("submitted_by" in f._errors)
        self.assertTrue("submitted_by" not in f.cleaned_data)


class TestInvestigationForm(TestCase):

    fixtures = [
        'domains.json',
        #'severities.json',
        'techniques.json',
        'actionescalation.json',
        'actionpriority.json',
        'actiontypes.json',
        'causes.json',
        'locations.json',
        'standarddescriptions.json',
    ]

    def setUp(self):

        self.password = 'password'
        self.admin = User.objects.create_superuser('admin', 'myemail@test.com', self.password)

        self.valid = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.INCIDENT_TYPE_CHOICES[0][0],
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            intent = models.INTENT_CHOICES[-1][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[-1][0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin,
            actual=models.POTENTIAL,
        )

        self.valid.save()

        i = self.valid.investigation
        i.investigator=self.admin
        i.cause = random.choice(models.Cause.objects.all())
        i.detection_domain = random.choice(models.Domain.objects.all())
        i.origin_domain = random.choice(models.Domain.objects.all())
        i.harm = random.choice(models.DOH_CHOICES)[0]
        i.save()

    def test_invalid_readonly(self):

        self.valid.valid = False
        form = forms.Investigation(instance=self.valid.investigation)
        for f in form.fields.values():
            self.assertTrue('readonly' in f.widget.attrs)


class TestIncidentActionForm(TestCase):

    def test_attrs_set(self):
        f = forms.IncidentActionForm()
        for field in f.fields.values():
            if isinstance(field.widget, django.forms.Textarea):
                self.assertEqual(3, field.widget.attrs['rows'])


