import datetime

from adminsortable2.admin import SortableAdminMixin
from import_export.admin import ExportMixin
from django.contrib import admin
from django.contrib.admin import FieldListFilter
from django.core.cache import cache
from django.db.models import DateField, DateTimeField
from django_mptt_admin.admin import DjangoMpttAdmin
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from incidents import models, resources


class MPTTModelAdmin(DjangoMpttAdmin):
    list_display = ("name", "parent", "lft", "rght", "level", "tree_id",)


class NameSlugAdminMixin(object):
    prepopulated_fields = {"slug": ("name",)}


class NameSlugAdmin(NameSlugAdminMixin, SortableAdminMixin, admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        super(NameSlugAdmin, self).save_model(request, obj, form, change)
        cache.clear()

    def delete_model(self, request, obj):
        super(NameSlugAdmin, self).delete_model(request, obj)
        cache.clear()


def short_description(inc):
    des = inc.description[:50]
    if len(inc.description) > 50:
        des += "..."
    return des


class CustomDateFieldListFilter(FieldListFilter):

    def __init__(self, field, request, params, model, model_admin, field_path):
        self.field_generic = '%s__' % field_path
        self.date_params = {k: v for k, v in params.items()
                            if k.startswith(self.field_generic)}

        now = timezone.now()
        # When time zone support is enabled, convert "now" to the user's time
        # zone so Django's definition of "Today" matches what the user expects.
        if timezone.is_aware(now):
            now = timezone.localtime(now)

        if isinstance(field, DateTimeField):
            today = now.replace(hour=0, minute=0, second=0, microsecond=0)
        else:  # field is a models.DateField
            today = now.date()
        tomorrow = today + datetime.timedelta(days=1)
        if today.month == 12:
            next_month = today.replace(year=today.year + 1, month=1, day=1)
            last_month = today.replace(month=today.month - 1, day=1)
        elif today.month == 1:
            next_month = today.replace(month=today.month + 1, day=1)
            last_month = today.replace(year=today.year - 1, month=12, day=1)
        else:
            next_month = today.replace(month=today.month + 1, day=1)
            last_month = today.replace(month=today.month - 1, day=1)

        next_year = today.replace(year=today.year + 1, month=1, day=1)
        last_year = today.replace(year=today.year - 1, month=1, day=1)

        self.lookup_kwarg_since = '%s__gte' % field_path
        self.lookup_kwarg_until = '%s__lt' % field_path
        self.links = (
            (_('Any date'), {}),
            (_('Today'), {
                self.lookup_kwarg_since: str(today),
                self.lookup_kwarg_until: str(tomorrow),
            }),
            (_('Past 7 days'), {
                self.lookup_kwarg_since: str(today - datetime.timedelta(days=7)),
                self.lookup_kwarg_until: str(tomorrow),
            }),
            (_('This month'), {
                self.lookup_kwarg_since: str(today.replace(day=1)),
                self.lookup_kwarg_until: str(next_month),
            }),
            (_('Last month'), {
                self.lookup_kwarg_since: str(last_month),
                self.lookup_kwarg_until: str(today.replace(day=1)),
            }),
            (_('This year'), {
                self.lookup_kwarg_since: str(today.replace(month=1, day=1)),
                self.lookup_kwarg_until: str(next_year),
            }),
            (_('Last Year'), {
                self.lookup_kwarg_since: str(last_year),
                self.lookup_kwarg_until: str(today.replace(month=1, day=1)),
            }),
        )
        super(CustomDateFieldListFilter, self).__init__(
            field, request, params, model, model_admin, field_path)

    def expected_parameters(self):
        return [self.lookup_kwarg_since, self.lookup_kwarg_until]

    def choices(self, cl):
        for title, param_dict in self.links:
            yield {
                'selected': self.date_params == param_dict,
                'query_string': cl.get_query_string(param_dict, [self.field_generic]),
                'display': title,
            }


FieldListFilter.register(
    lambda f: isinstance(f, DateField), CustomDateFieldListFilter)


class IncidentExportAdmin(ExportMixin, admin.ModelAdmin):
    resource_class = resources.IncidentResource
    list_filter = (
        ("incident_date", CustomDateFieldListFilter),
        "valid",
        "incident_type",
        "actual",
        "technique",
        "severity",
        "intent",
        "location",
    )
    list_display = (
        "__str__",
        "incident_date",
        "actual",
        "severity",
        short_description,
    )


class InvestigationExportAdmin(ExportMixin, admin.ModelAdmin):
    resource_class = resources.IncidentResource

    search_fields = ("psls_id",)

    list_filter = (
        "complete",
        "operational_type",
        "harm",
        "theme",
        ("assigned", CustomDateFieldListFilter),
        "investigator",
    )

    list_display = (
        "incident",
        # "origin_domain",
        # "detection_domain",
        "cause",
        "investigator",
        "complete",
    )

    def has_add_permission(self, request):
        return False


class LearningActionAdmin(ExportMixin, admin.ModelAdmin):
    resource_class = resources.IncidentActionResource

    list_display = ("incident", "action_type", "responsible", "priority", "escalation",)
    list_filter = (("assigned", CustomDateFieldListFilter), "priority", "escalation",)


class SharingAdmin(ExportMixin, admin.ModelAdmin):
    resource_class = resources.IncidentSharingResource

    list_display = ("incident", "responsible", "sharing_audience",)
    list_filter = (("assigned", CustomDateFieldListFilter), "sharing_audience",)


admin.site.register(
    [
        models.LocationChoice,
        models.StandardDescription,
        models.ActionEscalation,
        models.ActionPriority,
        models.Technique,
        models.OperationalType,
        models.ActionType,
        models.SharingAudience
    ],
    NameSlugAdmin
)

admin.site.register([models.IncidentAction], LearningActionAdmin)
admin.site.register([models.SeverityDefinition], admin.ModelAdmin)
admin.site.register([models.Incident], IncidentExportAdmin)
admin.site.register([models.Investigation], InvestigationExportAdmin)
admin.site.register([models.Cause, models.Domain], MPTTModelAdmin)
admin.site.register([models.IncidentSharing], SharingAdmin)
admin.site.register([models.Theme], admin.ModelAdmin)
