
from django.contrib.auth import get_user_model
from django.conf import settings
from django.db import models

User = settings.AUTH_USER_MODEL


class Subscription(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    incident = models.ForeignKey('incidents.Incident', related_name="subscriptions", on_delete=models.CASCADE)

