from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.contrib import messages
from django.core.exceptions import PermissionDenied, MultipleObjectsReturned
from django.urls import reverse
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.views.generic import DeleteView, View

from notifications import models


class Subscribe(View, LoginRequiredMixin):

    template_name_suffix = "_create"
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        models.Subscription.objects.get_or_create(user=self.request.user, incident_id=pk)
        messages.success(request, "You are now subscribed to incident %s" % pk)
        return HttpResponseRedirect(reverse("update_incident", kwargs={"pk": pk}))

    def handle_no_permission(self):
        return reverse("login")


class Unsubscribe(DeleteView, LoginRequiredMixin):

    model = models.Subscription

    def get_object(self, *args, **kwargs):

        pk = self.kwargs["pk"]
        try:
            obj = models.Subscription.objects.get(incident_id=pk, user=self.request.user)
        except models.Subscription.DoesNotExist:
            raise Http404("You are not subscribed to this incident!")
        except MultipleObjectsReturned:

            obj = models.Subscription.objects.latest(incident_id=pk, user=self.request.user)

            models.Subscription.objects.filter(incident_id=pk, user=self.request.user).exclude(
                id=obj.id
            ).delete()

        return obj

    def get_success_url(self):

        if "cancel" not in self.request.POST:
            messages.success(self.request, "Successfully unsubscribed from Incident #%d" % self.object.incident.pk)

        if self.request.POST.get("next", None) == "incident":
            return reverse("update_incident", kwargs={"pk": self.kwargs['pk']})
        elif self.request.user.has_module_perms("incidents"):
            return reverse("dashboard")

        return reverse("report")

    def post(self, request, *args, **kwargs):

        if "cancel" in request.POST:
            return HttpResponseRedirect(self.get_success_url())

        return super(Unsubscribe, self).post(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(Unsubscribe, self).get_context_data(**kwargs)
        context["next"] = self.request.GET.get("next", None)
        return context

    def handle_no_permission(self):
        return reverse("login")


@login_required
def sub_unsub(request):

    incident_id = request.POST.get('object_id')

    if incident_id:
        sub_qs = models.Subscription.objects.filter(user=request.user, incident_id=incident_id)
        if sub_qs.exists():
            sub_qs.delete()
            subbed = False
            message = 'You are now unsubscribed from incident {}'.format(incident_id)
        else:
            models.Subscription.objects.get_or_create(user=request.user, incident_id=incident_id)
            message = 'You are now subscribed to incident {}'.format(incident_id)
            subbed = True

        data = {
            'message': message,
            'subbed': subbed
        }
        return JsonResponse(data)
    return JsonResponse({'message': 'No incident found'}, status=400)
