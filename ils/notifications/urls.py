from django.urls import re_path, path
from notifications import views

urlpatterns = [
    re_path(r'^unsubscribe/(?P<pk>\d+)/$', views.Unsubscribe.as_view(), name="unsubscribe"),
    re_path(r'^subscribe/(?P<pk>\d+)/$', views.Subscribe.as_view(), name="subscribe"),
    path('sub_unsub/', views.sub_unsub, name='sub_unsub')
]

