
require.config({
    urlArgs: (function () {
        if (SiteConfig.debug === 'True') {
            return 'v=' + Math.random();
        }
        return 'v=' + SiteConfig.version;
    }()),
    baseUrl: SiteConfig.baseStaticUrl,
    paths: {
        adminlte: SiteConfig.baseStaticUrl + 'adminlte/js/adminlte' + SiteConfig.min,
        adminlte_config: SiteConfig.baseStaticUrl + 'adminlte/js/adminlte.config',
        daterangepicker: SiteConfig.baseStaticUrl + 'daterangepicker/js/daterangepicker',
        // datepicker: SiteConfig.baseStaticUrl + 'flatpickr/js/flatpickr',
        flatpickr: SiteConfig.baseStaticUrl + 'flatpickr/js/flatpickr',
        jquery: SiteConfig.baseStaticUrl + 'jquery/js/jquery' + SiteConfig.min,
        'jquery.sortable': SiteConfig.baseStaticUrl + 'sortable/js/jquery-sortable-min',
        knockout: SiteConfig.baseStaticUrl + 'js/knockout',
        bootstrap: SiteConfig.baseStaticUrl + 'bootstrap/js/bootstrap.bundle' + SiteConfig.min,
        'bootstrap-multiselect': SiteConfig.baseStaticUrl + 'multiselect/js/bootstrap-multiselect',
        // 'bootstrap.datepicker': SiteConfig.baseStaticUrl + 'datepicker/js/bootstrap-datepicker',
        // 'bootstrap.editable': SiteConfig.baseStaticUrl + 'bootstrap-editable/js/bootstrap-editable',
        'bootstrap.colorpicker': SiteConfig.baseStaticUrl + 'colorpicker/js/bootstrap-colorpicker',
        // underscore: SiteConfig.baseStaticUrl + 'js/underscore' + SiteConfig.min,
        lodash: SiteConfig.baseStaticUrl + 'lodash/js/lodash',
        json2: SiteConfig.baseStaticUrl + 'json2/js/json2',
        // django: SiteConfig.baseStaticUrl + 'js/djangojs/django',
        moment: SiteConfig.baseStaticUrl + 'moment/js/moment',
        ajaxcomments: SiteConfig.baseStaticUrl + 'js/ajaxcomments',
        selectize: SiteConfig.baseStaticUrl + 'selectize/js/selectize',
        datatables: SiteConfig.baseStaticUrl + 'listable/js/jquery.dataTables' + SiteConfig.min,
        'datatables.bootstrap': SiteConfig.baseStaticUrl + 'listable/js/jquery.dataTables.bootstrap',
        'datatables.columnFilter': SiteConfig.baseStaticUrl + 'listable/js/jquery.dataTables.columnFilter',
        'datatables.searchPlugins': SiteConfig.baseStaticUrl + 'listable/js/jquery.dataTables.searchPlugins',
        'datatables.sort': SiteConfig.baseStaticUrl + 'listable/js/jquery.dataTables.sort',
        listable: SiteConfig.baseStaticUrl + 'listable/js/listable',
        multiselect: SiteConfig.baseStaticUrl + 'multiselect/js/bootstrap.multiselect',

        fontawesome: SiteConfig.baseStaticUrl + 'fontawesome/js/fontawesome' + SiteConfig.min,
        popper: SiteConfig.baseStaticUrl + 'popper/js/popper',
        select2: SiteConfig.baseStaticUrl + 'select2/js/select2.full' + SiteConfig.min,
        toastr: SiteConfig.baseStaticUrl + 'toastr/js/toastr' + SiteConfig.min,

        flot: SiteConfig.baseStaticUrl + 'flot/jquery.flot' + SiteConfig.min,
        'flot.categories': SiteConfig.baseStaticUrl + 'flot/jquery.flot.categories',
        'flot.stack': SiteConfig.baseStaticUrl + 'flot/jquery.flot.stack',
        'flot.tickrotor': SiteConfig.baseStaticUrl + 'flot/jquery.flot.tickrotor',

        project: SiteConfig.baseStaticUrl + 'js/project',
        statistics: SiteConfig.baseStaticUrl + 'incidents/js/statistics',
        incomplete_statistics: SiteConfig.baseStaticUrl + 'incidents/js/incomplete_statistics',
        incidents_dt: SiteConfig.baseStaticUrl + 'incidents/js/incidents_dt',
        dashboard: SiteConfig.baseStaticUrl + 'incidents/js/dashboard',
        details: SiteConfig.baseStaticUrl + 'incidents/js/details',
        report: SiteConfig.baseStaticUrl + 'incidents/js/report',
        config: SiteConfig.baseStaticUrl + 'incidents/js/config'
    },
    shim: {
        adminlte: {
            deps: ['jquery', 'bootstrap', /*'slimscroll', 'fontawesome',*/ 'adminlte_config']
        },
        bootstrap: {
            deps: ['jquery', 'popper']
        },
        'bootstrap-multiselect': {
            deps: ['bootstrap']
        },
        datatables: {
            deps: ['jquery'],
            exports: 'dataTable'
        },
        'datatables.bootstrap': {
            deps: ['datatables']
        },
        'datatables.columnFilter': {
            deps: ['datatables']
        },
        'datatables.searchPlugins': {
            deps: ['datatables']
        },
        'datatables.sort': {
            deps: ['datatables']
        },
        datepicker: {
            deps: ['jquery', 'bootstrap']
        },
        daterangepicker: {
            exports: 'DateRangePicker',
            deps: ['jquery', 'moment']
        },
        // full_calendar: {
        //     deps: ['full_calendar_daygrid', 'full_calendar_timegrid', 'full_calendar_interaction']
        // },
        jquery: {
            exports: '$'
        },
        listable: {
            deps: ['jquery', 'moment', 'datatables', 'datatables.columnFilter', 'datatables.searchPlugins', 'datatables.sort', 'datatables.bootstrap', 'multiselect', 'daterangepicker']
        },
        lodash: {
            deps: ['jquery'],
            exports: '_'
        },
        multiselect: {
            deps: ['jquery', 'bootstrap']
        },
        popper: {
            exports: 'Popper'
        },
        slimscroll: {
            deps: ['jquery']
        },
        select2: {
            deps: ['jquery']
        },
        toastr: {
            deps: ['adminlte']
        },
        flatpickr: {
            deps: ['jquery']
        },

        flot: {
            deps: ['jquery']
        },
        'flot.categories': {
            deps: ['flot']
        },
        'flot.stack': {
            deps: ['flot']
        },
        'flot.tickrotor': {
            deps: ['flot']
        },

        project: {
            deps: ['jquery']
        },
        incidents_dt: {
            deps: ['jquery', 'django']
        },
        dashboard: {
            deps: ['jquery', 'bootstrap', 'select2', 'listable', 'flot', 'flot.categories', 'flot.stack']
        },
        // statistics: {
        //     deps: ['jquery', 'bootstrap', 'select2', 'json2', 'listable', 'lodash', 'moment', 'flatpickr', 'bootstrap.editable', 'flot', 'flot.categories', 'flot.stack', 'flot.tickrotor']
        // },
        // incomplete_stats: {
        //     deps: ['jquery', 'bootstrap', 'moment', 'flatpickr', 'listable']
        // },
        details: {
            deps: ['jquery', 'bootstrap', 'select2', 'selectize']
        },
        report: {
            deps: ['bootstrap']
        },
        config: {
            deps: ['jquery', 'bootstrap', 'knockout', 'select2', 'jquery.sortable', 'bootstrap.colorpicker']
        }
    }
});

require(['jquery', 'bootstrap', /**/'project', 'adminlte']);