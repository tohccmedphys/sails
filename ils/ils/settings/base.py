"""Common settings and globals."""


import os
from os.path import abspath, basename, dirname, join, normpath
from sys import path


VERSION = "4.1.0"
BUG_REPORT_URL = "https://bitbucket.org/tohccmedphys/sails/issues/new"


# Absolute filesystem path to the Django project directory:
DJANGO_ROOT = dirname(dirname(abspath(__file__)))

# Absolute filesystem path to the top-level project folder:
SITE_ROOT = dirname(DJANGO_ROOT)

# Site name:
SITE_NAME = basename(DJANGO_ROOT)
INTERNAL_IPS = []

# Add our project to our pythonpath, this way we don't need to type our project
# name in our dotted import paths:
path.append(DJANGO_ROOT)


# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = False

FORCE_SCRIPT_NAME = ""
# See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
TEMPLATE_DEBUG = DEBUG


FLAG_ALL_ON_REPORT = True

AUTH_USER_MODEL = 'accounts.ILSUser'


# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ('Your Name', 'your_email@example.com'),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS


# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(SITE_ROOT, 'db.sqlite3'),
    }
}


MAX_CACHE_TIMEOUT = 24*60*60*365  # 1 year

CACHE_LOCATION = os.path.join(SITE_ROOT, "cache", "cache_data")
if not os.path.isdir(CACHE_LOCATION):
    os.mkdir(CACHE_LOCATION)

DOMAIN_CACHE_KEYS = ["sub_domains", "domain_ancestors"]
CAUSE_CACHE_KEYS = ["sub_causes", "cause_ancestors"]

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': CACHE_LOCATION,
        'TIMEOUT': MAX_CACHE_TIMEOUT,
    }
}


# See: https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = 'America/Toronto'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

FORMAT_MODULE_PATH = "ils.formats"


# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = normpath(join(SITE_ROOT, 'media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'


# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = normpath(join(SITE_ROOT, 'assets'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    normpath(join(SITE_ROOT, 'static')),
)

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key only used for development and testing.
SECRET_KEY = r"1epdnj%0lp70&z4-+rwh08d5xf9pqa93x#l7ta6*&+p&o$s52q"


# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    normpath(join(SITE_ROOT, 'fixtures')),
)


# See: https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
# TEMPLATE_CONTEXT_PROCESSORS = (
#     'django.contrib.auth.context_processors.auth',
#     'django.template.context_processors.debug',
#     'django.template.context_processors.i18n',
#     'django.template.context_processors.media',
#     'django.template.context_processors.static',
#     'django.template.context_processors.tz',
#     'django.contrib.messages.context_processors.messages',
#     'django.template.context_processors.request',
#     'absolute.context_processors.absolute',
#     'ils.context_processors.info',
# )
#
# # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
# TEMPLATE_LOADERS = (
#     'django.template.loaders.filesystem.Loader',
#     'django.template.loaders.app_directories.Loader',
# )
#
# # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
# TEMPLATE_DIRS = (
#     normpath(join(SITE_ROOT, 'templates')),
# )

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(SITE_ROOT, 'templates')],
        # 'DIRS': ['templates'],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                # 'absolute.context_processors.absolute',
                'ils.context_processors.info',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ]
        },
    },
]


# See: https://docs.djangoproject.com/en/dev/ref/settings/#middleware-classes
MIDDLEWARE = [
    # Default Django middleware.
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
]


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)


# See: https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = '%s.urls' % SITE_NAME


DJANGO_APPS = [
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Useful template tags:
    # 'django.contrib.humanize',

    # Admin panel and documentation:
    'django.contrib.admin',
    # 'django.contrib.admindocs',
]

THIRD_PARTY_APPS = [
    # Database migration helpers:
    # 'south',
    'django_comments',
    'mptt',
    'django_mptt_admin',
    #'rest_framework',
    # 'djangojs',
    #'stronghold',
    'adminsortable2',
    # 'paintstore',
    'colorfield',
    'import_export',
    # 'form_utils',
    #'permissions',
    #'workflows',
    'listable'
]

# Apps specific for this project go here.
LOCAL_APPS = [
    'ils',
    'cache',
    'accounts',
    'incidents',
    'notifications',
]

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS


REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAdminUser',),
    'PAGINATE_BY': 100,
}

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

NOTIFICATIONS_EMAIL = "sails@toh.on.ca"
NOTIFICATIONS_FAIL_SILENTLY = True
NOTIFICATIONS_ON = False

LISTABLE_DOM = '<"row"<"span6"ir><"span6"p>><"row"<"span12"rt>><"row"<"span12"lp>>'
LISTABLE_PAGINATION_TYPE = 'bootstrap2'

STRONGHOLD_PUBLIC_URLS = (
    r'^/djangojs/.+$',
    r'^/accounts/(login|logout)/$',
    r'^%s.+$' % STATIC_URL,
    r'^%s.+$' % MEDIA_URL,
)

STRONGHOLD_PUBLIC_NAMED_URLS = (
    "report",
    "home",
    "login",
)

LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'
LOGOUT_REDIRECT_URL = LOGIN_URL
LOGIN_REDIRECT_URL = '/incidents/report/'


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

PYTHON_DATE_FORMAT = '%d-%m-%Y'


# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'wsgi.application'


FRONTEND_DATE_DISPLAY_FORMAT = 'j M Y'
# FRONTEND_DATE_DISPLAY_FORMAT = 'Do MMM, YYYY'
FRONTEND_DATE_DATA_FORMAT = 'd-m-Y'
FRONTEND_TIME_DATA_FORMAT = 'h:m'
FRONTEND_DATE_DATA_FORMAT_MOMENT = 'DD-MM-YYYY'
FRONTEND_TIME_DATA_FORMAT_MOMENT = 'H:m'
FRONTEND_DATE_DISPLAY_FORMAT_MOMENT = 'MMM D YYYY'
FRONTEND_DATETIME_DATA_FORMAT_MOMENT = 'DD-MM-YYYY H:m'

CSRF_COOKIE_NAME = 'csrftoken'

URL_FOR_EMAILS = 'URL NOT SET'