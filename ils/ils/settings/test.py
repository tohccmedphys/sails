

from .base import *
from selenium import webdriver

# ######### TEST SETTINGS ######################
# TEST_RUNNER = 'discover_runner.DiscoverRunner'
#TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
TEST_DISCOVER_TOP_LEVEL = SITE_ROOT
TEST_DISCOVER_ROOT = SITE_ROOT
TEST_DISCOVER_PATTERN = "test_*.py"
# NOSE_ARGS = [
#     '--with-coverage',  # activate coverage report
#     # '--with-doctest',  # activate doctest: find and run docstests
#     '--verbosity=1',  # verbose output
#     # '--with-xunit',  # enable XUnit plugin
#     # '--xunit-file=xunittest.xml',  # the XUnit report file
#     '--cover-xml',  # produle XML coverage info
#     '--cover-xml-file=coverage.xml',  # the coverage info file
#     '--cover-package=incidents,notifications,accounts,ils,listable',
#     '--nocapture',
#     '--nologcapture',
# ]
########## IN-MEMORY TEST DATABASE
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ":memory:",
        "USER": "",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    },
}

NOTIFICATIONS_ON = False
DEBUG = True

SELENIUM_DRIVER_PATH = 'E:/code/installs/chromedriver.exe'
SELENIUM_DRIVER = webdriver.Chrome
os.environ['DJANGO_LIVE_TEST_SERVER_ADDRESS'] = 'localhost:8000'
