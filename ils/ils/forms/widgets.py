from django.forms import widgets
from django.db.models import Q
from django.utils.html import escape
from django.forms.utils import flatatt
from django.template.loader import get_template

from django.template import Context


class MPTTSelect(widgets.Select):

    # def render(self, *args, **kwargs):
    #     super().render(*args, **kwargs)
    def render(self, name, value, attrs=None, choices=()):
        if not value:
            value = ''
        else:
            # when forms are submiited the pk values are strings
            # when pulled from db they are ints :/
            # so coerce to always be ints
            value = int(value)

        qs = self.choices.queryset.all()#filter(parent=None)
        final_attrs = flatatt(self.build_attrs(attrs, name=name))
        context = {"final_attrs": final_attrs, "choices": qs, "selected": value}
        return get_template("forms/mptt_select.html").render(context)

