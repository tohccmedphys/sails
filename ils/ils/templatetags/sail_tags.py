
import re

from django import template
from django.contrib.contenttypes.models import ContentType


register = template.Library()


@register.filter(name='addcss')
def addcss(field, css):
    return field.as_widget(attrs={"class": css})


@register.filter(name='addplaceholder')
def addplaceholder(field, placeholder=None):
    if placeholder is None:
        if hasattr(field, 'verbose_name'):
            return field.as_widget(attrs={'placeholder': field.verbose_name})
        v_name = field.name.replace('_', ' ').title()
        return field.as_widget(attrs={'placeholder': v_name})
    else:
        return field.as_widget(attrs={'placeholder': placeholder})


@register.filter(name='addcss_addplaceholder')
def addcss_addplaceholder(field, css):
    if hasattr(field, 'verbose_name'):
        return field.as_widget(attrs={'placeholder': field.verbose_name, 'class': css})
    v_name = re.sub(r'\d', '', field.name.replace('_', ' ').title())
    return field.as_widget(attrs={'placeholder': v_name, 'class': css})


@register.filter(name='vue_management_form')
def vue_management_form(management_form, prefix):

    for field in management_form.fields:
        management_form.fields[field].widget.attrs.update({
            'v-model': '{}{}'.format(prefix, field),
        })
    return management_form


@register.filter(name='hide')
def hide(field):
    try:
        if field.name in field.form.hide_fields:
            return True
    except AttributeError:
        if field.hide:
            return True

    return False


@register.filter(name='get_start_time_field')
def get_start_time_field(form, day):

    field_name = '{}_time_start'.format(day[0])

    if field_name in form.fields:
        return form.fields[field_name].get_bound_field(form, field_name)

    return None


@register.filter(name='get_end_time_field')
def get_end_time_field(form, day):

    field_name = '{}_time_end'.format(day[0])

    if field_name in form.fields:
        return form.fields[field_name].get_bound_field(form, field_name)

    return None


@register.filter(name='get_day_available_field')
def get_day_available_field(form, day):

    field_name = '{}_is_available'.format(day[0])

    if field_name in form.fields:
        return form.fields[field_name].get_bound_field(form, field_name)

    return None
