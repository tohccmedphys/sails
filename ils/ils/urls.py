
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path, re_path, reverse_lazy
from django.views.generic import RedirectView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$', RedirectView.as_view(url=reverse_lazy("report")), name="home"),
    path('incidents/', include('incidents.urls')),
    path('notifications/', include('notifications.urls')),
    path('comments/', include('fluent_comments.urls')),
    path('', include('django.contrib.auth.urls')),
]


if settings.DEBUG:
    from django.conf.urls import include
    import debug_toolbar
    urlpatterns.append(re_path(r'^__debug__/', include(debug_toolbar.urls)))

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)